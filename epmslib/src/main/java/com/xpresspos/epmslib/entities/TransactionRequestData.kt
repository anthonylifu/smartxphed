package com.xpresspos.epmslib.entities

import android.os.Parcelable
import com.xpresspos.epmslib.utils.IsoAccountType
import kotlinx.android.parcel.Parcelize

/**
 * Payment request data.
 * otherAmount property is required for PURCHASE WITH CASHBACK transactions
 * For SALES COMPLETION, REFUND and REVERSAL transactions, originalDataElements property is required
 *
 * @property transactionType
 * @property amount
 * @property otherAmount
 * @property accountType
 * @property echoData
 * @property originalDataElements
 */
@Parcelize
data class TransactionRequestData(val transactionType: TransactionType = TransactionType.PURCHASE,
                                  var amount: Long,
                                  val otherAmount: Long = 0,
                                  val accountType: IsoAccountType = IsoAccountType.DEFAULT_UNSPECIFIED,
                                  var echoData: String? = null,
                                  val originalDataElements: OriginalDataElements? = null): Parcelable {


}

/**
 *
 *
 * @property originalTransactionType
 * @property originalAmount
 * @property originalAuthorizationCode
 * @property originalTransmissionTime
 * @property originalSTAN
 * @property originalRRN
 * @property originalAcquiringInstCode
 * @property originalForwardingInstCode
 */
@Parcelize
data class OriginalDataElements( val originalTransactionType: TransactionType,
                                 val originalAmount: Long = 0L,
                                 val originalAuthorizationCode: String? = null,
                                 val originalTransmissionTime: String = "",
                                 val originalSTAN: String = "",
                                 val originalRRN: String = "",
                                 val originalAcquiringInstCode: String = "",
                                 val originalForwardingInstCode: String? = null): Parcelable



fun TransactionResponse.toOriginalDataElements() = OriginalDataElements(
        originalTransactionType = transactionType,
        originalAmount =  amount,
        originalAuthorizationCode = authCode,
        originalTransmissionTime = transmissionDateTime,
        originalSTAN = STAN,
        originalRRN = RRN,
        originalAcquiringInstCode = acquiringInstCode,
        originalForwardingInstCode = originalForwardingInstCode
)