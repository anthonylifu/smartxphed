package com.xpresspayments.verifonex990

import android.app.Application
import android.content.Context
import android.content.Intent
import android.os.Build
import android.util.Log
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import com.xpresspayments.commons.Device
import com.xpresspayments.commons.DeviceConfigurator
import com.xpresspayments.commons.EmvProcessor
import com.xpresspayments.commons.SmartAppInterface
import com.xpresspayments.commons.printer.Printer
import com.xpresspayments.verifonex990.verifone.VFIService
import com.xpresspayments.verifonex990.verifone.emv.VerifoneCardProcessor
import com.xpresspayments.verifonex990.verifone.emv.VerifoneConfigManager
import com.xpresspayments.verifonex990.verifone.extensions.logTrace
import com.xpresspayments.verifonex990.verifone.printer.VerifonePrinter
import com.xpresspos.epmslib.entities.HostConfig


private lateinit var deviceService: VFIService
class VerifoneApp: SmartAppInterface {

   override val terminalSerial: String
        get() = deviceService.deviceInfo?.serialNo ?:  /*if (Build.VERSION.SDK_INT < Build.VERSION_CODES.O)*/ Build.SERIAL ?: "0123456789" //else Build.getSerial()

   override  fun initializeDevice(context: Context) {
        deviceService = VFIService(context.applicationContext as Application)
        val intent = Intent().apply {
            action = "com.vfi.smartpos.device_service"
            setPackage("com.vfi.smartpos.deviceservice")
        }

        if (!context.bindService(intent, deviceService, Context.BIND_AUTO_CREATE)) {
            Log.i("TAG", "deviceService connect fail!")
            Toast.makeText(context, R.string.vfi_service_connection_failed, Toast.LENGTH_LONG).show()
            throw Error(context.getString(R.string.vfi_service_connected))
        } else {
            logTrace(context.getString(R.string.vfi_service_connected))
//            Toast.makeText(context, R.string.vfi_service_connected, Toast.LENGTH_SHORT).show()
        }

    }


    override val deviceConfigurator: DeviceConfigurator
        get() = VerifoneConfigManager

    override fun getEmvProcessor(
        activity: AppCompatActivity,
        hostConfig: HostConfig
    ): EmvProcessor = VerifoneCardProcessor(activity, hostConfig)

    override fun getPrinter(context: Context): Printer {
        return VerifonePrinter(context)
    }

    override fun destruct(context: Context) {
        context.unbindService(deviceService)
    }

    override val device: Device
        get() = Device.VerifoneX990
}


val Context.vfiService: VFIService
get() =  deviceService