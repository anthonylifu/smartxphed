package com.xpresspayments.verifonex990.verifone

import android.app.Application
import android.content.ComponentName
import android.content.ServiceConnection
import android.os.IBinder
import android.widget.Toast
import com.vfi.smartpos.deviceservice.aidl.*
import com.xpresspayments.verifonex990.R

class VFIService(val app: Application) : ServiceConnection {

    private var iDevice: IDeviceService? = null

    override fun onServiceConnected(name: ComponentName?, service: IBinder?) {
        iDevice = IDeviceService.Stub.asInterface(service)
    }

    override fun onServiceDisconnected(name: ComponentName?) {
        iDevice = null
        Toast.makeText(app, R.string.vfi_service_disconnected, Toast.LENGTH_SHORT).show()
    }


    val beeper: IBeeper?
    get() = iDevice?.beeper

    val pinPad: IPinpad?
    get() = iDevice?.getPinpad(1)


    val emv: IEMV?
    get() = iDevice?.emv

    val printer: IPrinter?
    get() = iDevice?.printer

    val frontScanner: IScanner?
    get() = iDevice?.getScanner(1)

    val rearScanner: IScanner?
    get() = iDevice?.getScanner(0)

    val led: ILed?
    get() = iDevice?.led

    val serialPort: ISerialPort?
    get() = iDevice?.getSerialPort("usb-rs232")

    val usbSerialPort: IUsbSerialPort?
    get() = iDevice?.usbSerialPort

    val externalSerialPort: IExternalSerialPort?
    get() = iDevice?.externalSerialPort

    val deviceInfo: IDeviceInfo?
    get () = iDevice?.deviceInfo
}

//const val MASTER_KEY_ID = 1
//const val MAC_KEY_ID = 9
//const val PIN_KEY_ID = 10


const val NIBSS_MASTER_KEY_ID = 2
const val NIBSS_PIN_KEY_ID = 19
