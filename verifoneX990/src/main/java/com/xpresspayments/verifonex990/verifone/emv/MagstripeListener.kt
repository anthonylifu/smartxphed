package com.xpresspayments.verifonex990.verifone.emv

import android.os.Bundle
import android.util.Log
import com.vfi.smartpos.deviceservice.aidl.PinInputListener
import com.vfi.smartpos.deviceservice.constdefine.ConstCheckCardListener
import com.xpresspayments.commons.ProcessState
import com.xpresspayments.verifonex990.verifone.extensions.logTrace
import com.xpresspayments.verifonex990.verifone.NIBSS_PIN_KEY_ID
import com.xpresspayments.verifonex990.verifone.extensions.errorBeep
import com.xpresspayments.verifonex990.verifone.extensions.okBeep
import com.xpresspayments.verifonex990.verifone.extensions.startPlatformPinEntry
import com.xpresspayments.verifonex990.vfiService
import com.xpresspos.epmslib.entities.CardData
import com.xpresspos.epmslib.entities.getCardHolderFromTrack1
import com.xpresspos.epmslib.extensions.hexString

class MagstripeListener(private val processor: VerifoneCardProcessor) {
    private val beeper = processor.beeper

    lateinit var cardData: CardData
    private var cardHolderName = ""


    fun processTransaction(tracks: Bundle) {
        processor.setState(
            ProcessState(
                ProcessState.States.PROCESSING,
                "Processing"
            )
        )
        beeper.okBeep()

        val pan = tracks.getString(ConstCheckCardListener.onCardSwiped.track.KEY_PAN_String)
        logTrace("PAN: $pan")

        val track1 = tracks.getString(ConstCheckCardListener.onCardSwiped.track.KEY_TRACK1_String)
        logTrace("Track1: $track1")

        val track2 =
            tracks.getString(ConstCheckCardListener.onCardSwiped.track.KEY_TRACK2_String)?.replace(
                '=',
                'D'
            ) ?: kotlin.run {
                beeper.errorBeep()
                processor.setError(RuntimeException("Swipe error"))
                return
            }

        if (track2.isBlank()) {
            processor.setError(RuntimeException("Swipe error"))
            beeper.errorBeep()
            return
        }
        logTrace("Track2: $track2")

        val track3 = tracks.getString(ConstCheckCardListener.onCardSwiped.track.KEY_TRACK3_String)
        logTrace("Track3: $track3")

        val serviceCode =
            tracks.getString(ConstCheckCardListener.onCardSwiped.track.KEY_SERVICE_CODE_String)
        logTrace("Service code: $serviceCode")

        cardData = CardData(track2, "", "", "801") // todo put back 901
        logTrace("CardData: $cardData")

        cardHolderName = getCardHolderFromTrack1(track1 ?: "").trim()
        logTrace("Cardholder: $cardHolderName")

        processor.activity.startPlatformPinEntry(
            true,
            1,
            pan ?: "",
            pinInputListener,
            NIBSS_PIN_KEY_ID
        )
    }

    private fun onRequestOnline(cardData: CardData, cardHolderName: String) {
        beeper.startBeep(50)
        processor.setState(
            ProcessState(
                ProcessState.States.PROCESSING,
                "Processing online"
            )
        )

        logTrace("OnRequestOnline Pinblock: ${cardData.pinBlock}")
        val result = kotlin.runCatching {
            processor.processor.processTransaction(
                processor.activity,
                processor.requestData,
                cardData
            )
        }

        if (result.isFailure) {
            Log.e(TAG, "Host request error")
            val throwable = result.exceptionOrNull()
                ?: Exception("Error connecting to host"/*processor.activity.getString(R.string.error_connecting_to_host)*/)
            throwable.printStackTrace()
            processor.setError(throwable)
        }

        if (result.isSuccess) {
            val hostResponse = result.getOrNull()
            println(hostResponse)

            hostResponse?.let {
                it.cardHolder = cardHolderName
                it.cardLabel = if (cardData.track2Data.isNotBlank()) "Magstripe" else "CNP"
                processor.setResponse(it)
            }
        }

    }


    private val pinInputListener = object : PinInputListener.Stub() {
        override fun onInput(len: Int, key: Int) {}

        override fun onConfirm(data: ByteArray?, isNonePin: Boolean) {
            beeper.okBeep()
            logTrace("OnConfirm")

            if (!isNonePin) {
                cardData.pinBlock = data?.hexString()
                logTrace("Pinblock: ${cardData.pinBlock}")
            }

            onRequestOnline(cardData, cardHolderName)
        }

        override fun onCancel() {
            beeper.errorBeep()
            logTrace("Pin entry cancelled")
            processor.setError(RuntimeException("Pin entry cancelled"))
        }

        override fun onError(errorCode: Int) {
            beeper.errorBeep()
            val message =
                "Error occurred: $errorCode, ${processor.activity.vfiService.pinPad?.lastError}"
            logTrace(message)
            processor.setError(RuntimeException(message))
        }
    }
}