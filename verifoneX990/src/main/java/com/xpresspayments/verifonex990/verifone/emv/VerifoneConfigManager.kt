package com.xpresspayments.verifonex990.verifone.emv

import android.content.Context
import com.vfi.smartpos.deviceservice.aidl.PinpadKeyType
import com.vfi.smartpos.deviceservice.constdefine.ConstIPBOC
import com.xpresspayments.verifonex990.verifone.extensions.*
import com.xpresspayments.verifonex990.vfiService
import com.xpresspayments.commons.DeviceConfigurator
import com.xpresspayments.verifonex990.verifone.*
import com.xpresspos.epmslib.entities.*
import com.xpresspos.epmslib.extensions.hexByteArray
import com.xpresspos.epmslib.extensions.hexString
import java.lang.RuntimeException

object VerifoneConfigManager: DeviceConfigurator {

    override fun processKeys(context: Context, keyHolder: KeyHolder): Boolean {
        val pinPad = context.vfiService.pinPad!!

        if (!pinPad.loadMainKey(
                NIBSS_MASTER_KEY_ID, keyHolder.masterKey.hexByteArray().also {
                logTrace("Encrypted Masterkey: ${it.hexString()}")
            },
                CryptoManager.getKCV(keyHolder.masterKey).hexByteArray().also {
                    logTrace("Masterkey KCV: ${it.hexString()}")
                })) {
            throw RuntimeException("Master key injection failed")
        }

        if (!pinPad.loadWorkKey(
                PinpadKeyType.PINKEY, NIBSS_MASTER_KEY_ID, NIBSS_PIN_KEY_ID,
                keyHolder.pinKey.hexByteArray().also {
                    logTrace("Enc Pinkey: ${it.hexString()}")
                },
                CryptoManager.getKCV(keyHolder.clearPinKey).hexByteArray().also {
                    logTrace("Pinkey KCV: ${it.hexString()}")
                }
            )
        ) {
            throw RuntimeException("Pinkey injection failed")
        }

        return true
    }

    override fun processConfigData(context: Context, terminalID: String, configData: ConfigData): Boolean {
        context.vfiService.emv?.let {

        }

        return true
    }

    override fun processCAs(context: Context, caList: List<NibssCA>): Boolean {
        return true
    }

    override fun processAIDs(context: Context, aidList: List<NibssAID>): Boolean {
        return true
    }

    fun loadDefaultAIDs(context: Context, shouldClear: Boolean = true): Boolean {
        context.vfiService.emv?.let { emv ->
            if (shouldClear) {
                if (!emv.clearAllAIDs()) {
                    logTrace("AID clear failed")
                }
            }

            aidArray.forEach {
                val contactResult =  emv.updateAID(ConstIPBOC.updateAID.operation.append, ConstIPBOC.updateAID.aidType.smart_card, it.tlvString)
                logTrace("Updated Contact AID - ${it.comment} - $contactResult")

                val ctlsResult =  emv.updateAID(ConstIPBOC.updateAID.operation.append, ConstIPBOC.updateAID.aidType.contactless, it.tlvString)
                logTrace("Updated CTLS AID - ${it.comment} - $ctlsResult")
            }
        }

        return true
    }

    fun loadDefaultRIDs(context: Context, shouldClear: Boolean = true): Boolean {

        context.vfiService.emv?.let { emv ->
            if (shouldClear) {
                if (!emv.clearAllRIDs()) {
                    logTrace("RID clear failed")
                }
            }

            capKeyArray.forEach {
                val result = emv.updateRID(ConstIPBOC.updateAID.operation.append, it.tlvString)
                logTrace("Updated RID- ${it.comment} - $result")
            }
        }

        return true
    }

}