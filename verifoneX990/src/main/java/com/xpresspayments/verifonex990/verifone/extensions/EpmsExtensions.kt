package com.xpresspayments.verifonex990.verifone.extensions

import com.vfi.smartpos.deviceservice.constdefine.ConstIPBOC
import com.xpresspayments.commons.device.oem.EMVParamApplication
import com.xpresspayments.commons.device.oem.EMVParamKey
import com.xpresspos.epmslib.entities.NibssAID
import com.xpresspos.epmslib.entities.NibssCA


fun NibssAID.toEmvParamApp(type: Int = ConstIPBOC.updateAID.aidType.smart_card): EMVParamApplication =
    EMVParamApplication().let {
        it.flagAppendRemoveClear = ConstIPBOC.updateRID.operation.append
        it.aidType = type
        it.comment = applicationName

        it.append(EMVParamApplication.TAG_AID_9F06, AID)
        it.append(EMVParamApplication.TAG_DefaultDDOL_DF14, DDOL)
        it.append(EMVParamApplication.TAG_TAC_Default_DF11, tacDefault)
        it.append(EMVParamApplication.TAG_TAC_Online_DF12, tacOnline)
        it.append(EMVParamApplication.TAG_TAC_Denial_DF13, tacDenial)

        it.append(EMVParamApplication.TAG_ASI_DF01, "00")

        it.append(
            EMVParamApplication.TAG_FloorLimit_9F1B,
            if (tflDomestic.isNotEmpty()) tflDomestic else "00000001"
        )

        it.append(
            EMVParamApplication.TAG_Threshold_DF15,
            if (offlineThresholdDomestic.isNotEmpty()) offlineThresholdDomestic else "100"
        )

        it.append(
            EMVParamApplication.TAG_VerNum_9F09,
            if (!applicationVersion.isEmpty()) applicationVersion else "00"
        )

        it.append(
            EMVParamApplication.TAG_MaxTargetPercentage_DF16,
            if (!maxTargetDomestic.isEmpty()) maxTargetDomestic else "50"
        )


        it.append(
            EMVParamApplication.TAG_TargetPercentage_DF17,
            if (!targetPercentageDomestic.isEmpty()) targetPercentageDomestic else "20"
        )


        if (TDOL.isNotEmpty()) {
            it.append(EMVParamApplication.TAG_DefaultTDOL_97_Optional, TDOL)
        }

        it.append(0xDF18, "01")
        it.append(EMVParamApplication.TAG_CurrencyCodeTerm_5F2A, "0566")
        it.append(EMVParamApplication.TAG_CountryCodeTerm_9F1A, "0566")

//        it.append(EMVParamApplication.TAG_ECTransLimit_9F7B, "000000100000")
        it.append(EMVParamApplication.TAG_CTLSFloorLimit_DF19, "000000100000")
        it.append(EMVParamApplication.TAG_CTLSTransLimit_DF20, "000999999999")
        it.append(EMVParamApplication.TAG_CTLSCVMLimit_DF21, "000000100000")

        it
    }

fun NibssCA.toEmvParamKey(): EMVParamKey {
    val nibssCA = this

    return EMVParamKey().apply {
        comment = nibssCA.keyName
        flagAppendRemoveClear = ConstIPBOC.updateRID.operation.append
        append(EMVParamKey.TAG_RID_9F06, nibssCA.RID)
        append(EMVParamKey.TAG_Index_9F22, nibssCA.keyIndex)
        append(EMVParamKey.TAG_ExpiryDate_DF05, "3230323931323331")
        append(EMVParamKey.TAG_KEY_DF02, nibssCA.keyAlgorithm)
        append(EMVParamKey.TAG_Exponent_DF04, nibssCA.exponent)
        append(EMVParamKey.TAG_Hash_DF03, nibssCA.hash)
        append(EMVParamKey.TAG_Algorithm_DF07, nibssCA.hashAlgorithm)
    }
}
