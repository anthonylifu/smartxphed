package com.xpresspayments.verifonex990.verifone.printer

import android.content.Context
import android.graphics.Bitmap
import androidx.core.os.bundleOf
import com.vfi.smartpos.deviceservice.aidl.PrinterListener
import com.xpresspayments.verifonex990.vfiService
import com.vfi.smartpos.deviceservice.constdefine.ConstIPrinter
import com.xpresspayments.commons.printer.*
import kotlinx.coroutines.runBlocking
import java.io.ByteArrayOutputStream
import java.io.IOException
import kotlin.coroutines.resume
import kotlin.coroutines.suspendCoroutine


const val ERROR_NONE = 0x00
const val ERROR_PAPERENDED = (0xF0)
const val ERROR_HARDERR = (0xF2)
const val ERROR_OVERHEAT = (0xF3)
const val ERROR_BUFOVERFLOW = (0xF5)
const val ERROR_LOWVOL = (0xE1)
const val ERROR_PAPERENDING = (0xF4)
const val ERROR_MOTORERR = (0xFB)
const val ERROR_PAPERJAM = (0xEE)
const val ERROR_BUSY = (0xF7)
const val ERROR_WORKON = (0xE6)


class VerifonePrinter(context: Context): Printer {

    val printer = context.vfiService.printer

    init {
        printer?.cleanCache()
    }

    override fun addText(text: String, format: TextFormat) {
        printer?.addText(format.bundle, text)
    }

    override fun addDoubleText(left: String, right: String, format: TextFormat) {
        printer?.addTextInLine(format.doubleTextBundle, left, "", right, ConstIPrinter.addTextInLine.mode.Devide_flexible)
    }

    override fun addImage(bitmap: Bitmap, width: Int, height: Int, offset: Int) {
       printer?.addImage(
           bundleOf(
               Pair("height", height),
               Pair("offset", offset),
               Pair("width", width)
           ),
           bitmap.toByteArray
       )
    }

    override fun addBarcode(text: String, expectedHeight: Int, align: Align) {
        printer?.addQrCode(bundleOf(
            Pair(ConstIPrinter.addBarCode.format.KEY_Alignment_int , align.code),
            Pair(ConstIPrinter.addBarCode.format.KEY_Height_int, expectedHeight)
        ),
            text
        )
    }

    override fun addQrCode(text: String, expectedHeight: Int, offset: Int) {
       printer?.addQrCode(bundleOf(
           Pair(ConstIPrinter.addQrCode.format.KEY_Offset_int, offset),
           Pair(ConstIPrinter.addQrCode.format.KEY_Height_String, expectedHeight)
       ),
           text
       )
    }

    override fun print(): Printer.Status = runBlocking<Printer.Status> {
        suspendCoroutine{
            printer?.startPrint(object : PrinterListener.Stub() {
                override fun onFinish() {
                    printer.cleanCache()
                    it.resume(Printer.Status.OK)
                }

                override fun onError(error: Int) {
                    it.resume(error.toPrinterStatus)
                }
            })
        }
    }

    override fun getStatus(): Printer.Status {
       return  printer?.status?.toPrinterStatus ?: Printer.Status.ERROR
    }

    override fun feedLine(count: Int) {
        printer?.feedLine(count)
    }

    override fun addLine() {
        addText("----------------------------")
    }
}

val Int.toPrinterStatus
    get() = when(this) {
       ERROR_NONE -> Printer.Status.OK
        ERROR_BUFOVERFLOW -> Printer.Status.BUFFER_OVERFLOW
        ERROR_OVERHEAT -> Printer.Status.OVER_HEAT
        ERROR_BUSY, ERROR_WORKON -> Printer.Status.BUSY
        ERROR_PAPERENDED, ERROR_PAPERENDING -> Printer.Status.OUT_OF_PAPER
        ERROR_LOWVOL -> Printer.Status.LOW_VOLTAGE
        ERROR_PAPERJAM -> Printer.Status.PAPER_JAM
        else -> Printer.Status.ERROR

    }

val FontSize.code
    get() = when(this) {
        FontSize.TINY -> ConstIPrinter.addText.format.VALUE_FontSize_SMALL_16_16
        FontSize.SMALL -> ConstIPrinter.addText.format.VALUE_FontSize_SMALL_16_16
        FontSize.NORMAL -> ConstIPrinter.addText.format.VALUE_FontSize_NORMAL_24_24
        FontSize.LARGE -> ConstIPrinter.addText.format.VALUE_FontSize_LARGE_32_32
        FontSize.EXTRA_LARGE -> ConstIPrinter.addText.format.VALUE_FontSize_LARGE_DH_32_64_IN_BOLD
        FontSize.BIG -> ConstIPrinter.addText.format.VALUE_FontSize_HUGE_48
    }

val Align.code
    get() = when(this) {
        Align.LEFT -> ConstIPrinter.addText.format.VALUE_Alignment_LEFT
        Align.CENTER -> ConstIPrinter.addText.format.VALUE_Alignment_CENTER
        Align.RIGHT -> ConstIPrinter.addText.format.VALUE_Alignment_RIGHT
    }

val FontStyle.value
    get() = when (this) {
        FontStyle.BOLD -> true
        else -> false
    }

val TextFormat.bundle
    get() = bundleOf(
        Pair(ConstIPrinter.BUNDLE_PRINT_FONT, fontSize.code),
        Pair(ConstIPrinter.BUNDLE_PRINT_ALIGN, align.code),
        Pair(ConstIPrinter.addText.format.KEY_StyleBold_boolean, fontStyle.value),
        Pair(ConstIPrinter.addText.format.KEY_newline_boolean, ConstIPrinter.addText.format.VALUE_newline_AppendCRLF)
    )

val TextFormat.doubleTextBundle
    get() = bundleOf(
        Pair(ConstIPrinter.BUNDLE_PRINT_FONT, fontSize.code),
        Pair(ConstIPrinter.BUNDLE_PRINT_ALIGN, align.code),
        Pair(ConstIPrinter.addTextInLine.format.KEY_StyleBold_boolean, fontStyle.value))

val Bitmap.toByteArray: ByteArray
    get() {
        val out =  ByteArrayOutputStream()
        this.compress(Bitmap.CompressFormat.PNG, 100, out);
        try {
            out.flush();
            out.close();
        } catch (e: IOException) {
            e.printStackTrace();
        }
        return out.toByteArray();
    }

