package com.xpresspayments.verifonex990.verifone.emv

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import com.vfi.smartpos.deviceservice.aidl.CheckCardListener
import com.vfi.smartpos.deviceservice.aidl.IBeeper
import com.vfi.smartpos.deviceservice.constdefine.ConstIPBOC
import com.xpresspayments.commons.EmvProcessor
import com.xpresspayments.commons.ProcessState
import com.xpresspayments.commons.Reader
import com.xpresspayments.commons.displayMessageId
import com.xpresspayments.verifonex990.R
import com.xpresspayments.verifonex990.verifone.extensions.logTrace
import com.xpresspayments.verifonex990.vfiService
import com.xpresspayments.verifonex990.verifone.cardOption
import com.xpresspayments.verifonex990.verifone.extensions.errorBeep
import com.xpresspayments.verifonex990.verifone.extensions.okBeep
import com.xpresspos.epmslib.entities.HostConfig
import com.xpresspos.epmslib.entities.TransactionRequestData
import kotlin.RuntimeException

const val TAG = "X990"
const val DEFAULT_CHECK_CARD_TIMEOUT = 60

class VerifoneCardProcessor(activity: AppCompatActivity,
                            hostConfig: HostConfig
): EmvProcessor(activity, hostConfig) {

    internal val emv by lazy {
        activity.vfiService.emv!!
    }

    internal val beeper: IBeeper by lazy {
        activity.vfiService.beeper!!
    }

    internal lateinit var requestData: TransactionRequestData


    private val magListener by lazy {
        MagstripeListener(this)
    }

    private val emvListener by lazy {
        EmvListener(this)
    }


    var cardType = 0
        private  set


    override fun startTransaction(requestData: TransactionRequestData, reader: Reader) {
        this.requestData = requestData
        beginTransaction(reader, DEFAULT_CHECK_CARD_TIMEOUT)
    }



    internal fun fallbackTransaction () {
        val reader = if (cardType == ConstIPBOC.startEMV.intent.VALUE_cardType_contactless) {
            Reader.CT_MSR
        } else {
            Reader.MSR
        }

        beginTransaction(reader)
    }

    private fun beginTransaction(reader: Reader, timeOut: Int = DEFAULT_CHECK_CARD_TIMEOUT) {
        logTrace("Readers: $reader")

        runCatching {
            emv.abortEMV()
        }


        setState(
            ProcessState(
                ProcessState.States.CARD_ENTRY,
                activity.getString(reader.displayMessageId())
            )
        )
        emv.checkCard(reader.cardOption, timeOut, checkCardListener)
    }

    private fun startEmvTransaction(cardType: Int) {
        emv.stopCheckCard()
        beeper.okBeep()

        this.cardType = cardType
        val option = Bundle().apply {
            putInt(ConstIPBOC.startEMV.intent.KEY_cardType_int, cardType)
            putLong(ConstIPBOC.startEMV.intent.KEY_authAmount_long, requestData.amount)

            putString("otherAmount", requestData.otherAmount.toString())

            putString(ConstIPBOC.startEMV.intent.KEY_merchantName_String, hostConfig.configData.merchantNameLocation)
            putString(ConstIPBOC.startEMV.intent.KEY_merchantId_String, hostConfig.configData.cardAcceptorIdCode)
            putString(ConstIPBOC.startEMV.intent.KEY_terminalId_String, hostConfig.terminalId)

            putBoolean(
                ConstIPBOC.startEMV.intent.KEY_isSupportQ_boolean,
                ConstIPBOC.startEMV.intent.VALUE_unsupported
            )
            putBoolean(
                ConstIPBOC.startEMV.intent.KEY_isSupportSM_boolean,
                ConstIPBOC.startEMV.intent.VALUE_unsupported
            )
            putBoolean(
                ConstIPBOC.startEMV.intent.KEY_isQPBOCForceOnline_boolean,
                ConstIPBOC.startEMV.intent.VALUE_unforced
            )
            putBoolean(
                "isForceOnline",
                ConstIPBOC.startEMV.intent.VALUE_unforced
            )

            putByte(ConstIPBOC.startEMV.intent.KEY_transProcessCode_byte, requestData.transactionType.code.substring(0..1).toByte())
            putBoolean("isSupportPBOCFirst", false)

//            val currencyCode =  hostConfig.configData.currencyCode.padLeft(4, '0')
//            logTrace("Currency Code: $currencyCode")
//
//            putString("transCurrCode", currencyCode)
        }

        setState(
            ProcessState(
                ProcessState.States.PROCESSING,
                activity.getString(R.string.processing)
            )
        )

        emv.startEMV(ConstIPBOC.startEMV.processType.full_process, option, emvListener)
    }

    override fun abortTransaction() {
        logTrace("AbortTransaction")
        emv.stopCheckCard()
        emv.abortEMV()
        setError(RuntimeException("Transaction cancelled"))
    }

    private val checkCardListener = object : CheckCardListener.Stub() {
        override fun onCardSwiped(tracks: Bundle?) {
            //MSR
            logTrace("onCardSwiped")

            magListener.processTransaction(tracks ?: throw RuntimeException("Swipe error"))
        }

        override fun onCardPowerUp() {
            logTrace("OnCardPowerUp")
            //ICC/Contact
            startEmvTransaction(ConstIPBOC.startEMV.intent.VALUE_cardType_smart_card)
        }

        override fun onCardActivate() {
            logTrace("onCardActivate")
            //RF/Contactless
            startEmvTransaction(ConstIPBOC.startEMV.intent.VALUE_cardType_contactless)
        }


        override fun onTimeout() {
            logTrace("Timeout")
            setError(RuntimeException("Timeout"))

        }

        override fun onError(error: Int, message: String?) {
            val msg = "Error: $error :: $message"
            logTrace(msg)
            if (error in arrayOf(3/*powerup error*/)) {
                beeper.errorBeep()
                fallbackTransaction()
            } else {
                setError(RuntimeException("$message - $error"))
            }

        }
    }
}