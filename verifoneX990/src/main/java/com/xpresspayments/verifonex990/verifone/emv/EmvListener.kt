package com.xpresspayments.verifonex990.verifone.emv

import android.os.Bundle
import android.util.Log
import com.google.android.material.dialog.MaterialAlertDialogBuilder
import com.vfi.smartpos.deviceservice.aidl.EMVHandler
import com.vfi.smartpos.deviceservice.aidl.OnlineResultHandler
import com.vfi.smartpos.deviceservice.aidl.PinInputListener
import com.vfi.smartpos.deviceservice.constdefine.ConstIPBOC
import com.vfi.smartpos.deviceservice.constdefine.ConstOnlineResultHandler
import com.vfi.smartpos.deviceservice.constdefine.ConstPBOCHandler
import com.xpresspayments.commons.EmvTag
import com.xpresspayments.commons.ProcessState
import com.xpresspayments.verifonex990.R
import com.xpresspayments.verifonex990.verifone.extensions.*
import com.xpresspayments.verifonex990.verifone.NIBSS_PIN_KEY_ID
import com.xpresspos.epmslib.entities.CardData
import com.xpresspos.epmslib.entities.TransactionResponse
import com.xpresspos.epmslib.entities.isApproved
import com.xpresspos.epmslib.extensions.hexString
import com.xpresspos.epmslib.extensions.maskPan
import com.xpresspos.epmslib.extensions.padLeft
import com.xpresspos.epmslib.tlv.TLVList
import com.xpresspos.epmslib.utils.IsoTimeManager
import kotlin.concurrent.thread
const val CTLS_TC = 204
class EmvListener(private val processor: VerifoneCardProcessor) : EMVHandler.Stub() {

    private val beeper = processor.beeper
    private val emv = processor.emv
    private val context = processor.activity

    private lateinit var cardData: CardData
    private var hostResponse: TransactionResponse? = null
    private var pinBlock: String? = null

    override fun onRequestAmount() {
        logTrace("onRequestAmount")
    }

    override fun onSelectApplication(appList: MutableList<Bundle>) {
        logTrace("onSelectApplication")
        beeper.okBeep()
        val apps: List<String> = appList.map {
            "${it.getString("aidName")} - ${it.getString("aid")}"
        }

        context.runOnUiThread {
            MaterialAlertDialogBuilder(context)
                .setTitle(R.string.title_select_app)
                .setItems(apps.toTypedArray<CharSequence>()) { _, which ->
                    emv.importAppSelection(1 + which)
                }.setOnCancelListener {
                    emv.importAppSelection(0)
                }.show()
        }
    }

    override fun onConfirmCardInfo(info: Bundle) {
        logTrace("OnConfirmCard")

        val result = "onConfirmCardInfo callback, \nPAN:" + info.getString("PAN") +
        "\nTRACK2:" + info.getString("TRACK2") +
                "\nCARD_SN:" + info.getString("CARD_SN")+
                "\nSERVICE_CODE:" + info.getString("SERVICE_CODE") +
                "\nEXPIRED_DATE:" + info.getString("EXPIRED_DATE");

        logTrace("result: $result")
       // processor.setState(ProcessState(ProcessState.States.WAITING, ""))

        val track2 = info.getString("TRACK2")?.replace('=', 'D') ?: ""
        val panSequenceNumber = info.getString("CARD_SN") ?: ""
        cardData = CardData(track2, "", panSequenceNumber)
        logTrace("CardData: $cardData")



       // processor.requestData.isDCC = false// !isGhanaBin(cardData.pan.substring(0..5))
        emv.importCardConfirmResult(ConstIPBOC.importCardConfirmResult.pass.allowed)
    }


    override fun onConfirmCertInfo(certType: String, certInfo: String) {
        logTrace("onConfirmCertInfo")
        beeper.okBeep()
        MaterialAlertDialogBuilder(context)
            .setTitle(R.string.title_confirm_certificate)
            .setMessage("Certificate Type: $certType\nCertificate Info: $certInfo")
            .setPositiveButton(android.R.string.ok) { _, _ ->
                emv.importCertConfirmResult(1)
            }.setNegativeButton(android.R.string.cancel) { _, _ ->
                emv.importCertConfirmResult(0)
            }.show()
    }

    override fun onRequestInputPIN(isOnlinePin: Boolean, retryTimes: Int) {
        logTrace("onRequestInputPIN")
        beeper.okBeep()
        val pan = cardData.pan
        logTrace("Card PAN: $pan")
        context.startPlatformPinEntry(isOnlinePin, retryTimes, pan, pinInputListener,  NIBSS_PIN_KEY_ID )
    }


    override fun onRequestOnlineProcess(aaResult: Bundle?) {
        logTrace("onRequestOnlineProcess")
        beeper.okBeep()
        processor.setState(
            ProcessState(
                ProcessState.States.PROCESSING,
                "Processing online"
            )
        )

        thread {
            logTrace("onRequestOnlineProcess")
            logTrace("Going online")

            val arqcResult = (aaResult?.getInt("RESULT") ?: 0).also { println("actual arqc result: $it") }
            logTrace("ARQC Result: $arqcResult")
            logTrace("CTLS CVN Result: ${aaResult?.getInt("CTLS_CVMR")}")
            logTrace("SIGNATURE: ${aaResult?.getBoolean("SIGNATURE")}")

            var iccData =  if (arqcResult == 302 /*PAYPASS_MAG_ARQC*/) "" else emv.getAppTLVList(CardData.NIBSS_TAGS)

            if (!iccData.isNullOrBlank()) {
                if (processor.cardType == ConstIPBOC.startEMV.intent.VALUE_cardType_contactless) {
                    logTrace("pad tag 9F6E")
                    val tag9F6E = emv.getAppTLVList(arrayOf("9F6E"))
                    logTrace("Tag 9F6E: $tag9F6E")
                    iccData += if (tag9F6E?.trim().isNullOrBlank()) "9F6E0420700000" else tag9F6E
                }
            }


            val posEntryMode = if (processor.cardType == ConstIPBOC.startEMV.intent.VALUE_cardType_contactless) {
                if (arqcResult == 302) "910" else "071"
            } else "051"

            cardData = CardData(cardData.track2Data, iccData.toUpperCase(), cardData.panSequenceNumber, posEntryMode).apply {
                pinBlock = this@EmvListener.pinBlock
                logTrace("Set Pinblock: ${cardData.pinBlock}")
            }

            val result = kotlin.runCatching {
                processor.processor.processTransaction(
                    processor.activity,
                    processor.requestData,
                    cardData
                )

            }

            val onlineResult = Bundle()
            if (result.isFailure) {
                logTrace("Online request failed")
                result.exceptionOrNull()?.let {
                    Log.e(TAG, "Host request error")
                    it.printStackTrace()
                    onlineResult.apply {
                        putBoolean(ConstIPBOC.inputOnlineResult.onlineResult.KEY_isOnline_boolean, false)
                        putString(ConstIPBOC.inputOnlineResult.onlineResult.KEY_field55_String, null)
                        putString(ConstIPBOC.inputOnlineResult.onlineResult.KEY_respCode_String, "20")
                        putString(ConstIPBOC.inputOnlineResult.onlineResult.KEY_authCode_String, null)
                    }
                }
            } else {
                logTrace("Online request successful");
                hostResponse = result.getOrThrow()
                println(hostResponse)
                hostResponse?.let {
                    onlineResult.apply {
                        putBoolean(ConstIPBOC.inputOnlineResult.onlineResult.KEY_isOnline_boolean, true)

                        if (!it.responseDE55.isNullOrBlank()) {
                            logTrace("Response ICC: ${it.responseDE55}")
                            putString(ConstIPBOC.inputOnlineResult.onlineResult.KEY_field55_String, it.responseDE55)
                        }

                        putString(
                            ConstIPBOC.inputOnlineResult.onlineResult.KEY_respCode_String,
                                it.responseCode
                        )
                        putString(ConstIPBOC.inputOnlineResult.onlineResult.KEY_authCode_String, it.authCode)
                    }
                }
            }


            emv.importOnlineResult(onlineResult, object : OnlineResultHandler.Stub() {
                override fun onProccessResult(result: Int, data: Bundle?) {
                    val tcData = data?.getString(
                        ConstOnlineResultHandler.onProccessResult.data.KEY_TC_DATA_String,
                        "not defined"
                    )
                    val str = "RESULT:" + result +
                            "\nTC_DATA:" + tcData +
                            "\nSCRIPT_DATA:" + data?.getString(
                        ConstOnlineResultHandler.onProccessResult.data.KEY_SCRIPT_DATA_String,
                        "not defined"
                    ) +
                            "\nREVERSAL_DATA:" + data?.getString(
                        ConstOnlineResultHandler.onProccessResult.data.KEY_REVERSAL_DATA_String,
                        "not defined"
                    )
                    logTrace("Response Str: $str")

                    val offlineApproved = result == ConstOnlineResultHandler.onProccessResult.result.TC
                    if (hostResponse!!.isApproved && !offlineApproved) {
                        processor.setState(
                            ProcessState(
                                ProcessState.States.PROCESSING,
                                "Offline Decline... Rolling back"
                            )
                        )
                        hostResponse = processor.processor.rollback(processor.activity)
                    } else if ( hostResponse?.responseCode == "65" && processor.cardType == ConstIPBOC.startEMV.intent.VALUE_cardType_contactless) {
                        processor.fallbackTransaction()
                        return
                    }

                    tcData?.let(::updateCardInfo)
                    hostResponse!!.let(processor::setResponse)
                }
            })

        }
    }

    private fun updateCardInfo(iccData: String) {
        logTrace("updateCardInfo")
        hostResponse?.let {
            val tlv = TLVList.fromBinary(iccData)
            it.appCryptogram =
                tlv.getTLVVL(
                    EmvTag.APPLICATION_CRYPTOGRAM_9F26.toString(
                        16
                    ).toUpperCase().also { logTrace("Tag: $it") })
            it.TVR =
                tlv.getTLVVL(
                    EmvTag.TERMINAL_VERIFICATION_RESULT_95.toString(
                        16
                    ).also { logTrace("Tag: $it") })

            it.AID = tlv.getTLVVL(
                EmvTag.DEDICATED_FILE_NAME_84.toString(16).also {
                    logTrace(
                        "Tag: $it"
                    )
                })
            it.TSI =
                emv.getCardData(EmvTag.TSI_9B.toString(16).toUpperCase())
                    .hexString().toUpperCase()
            it.cardHolder = String(
                emv.getCardData(EmvTag.CARDHOLDER_NAME_5F20.toString(16).toUpperCase()) ?: emv.getCardData(EmvTag.CARDHOLDER_NAME_5F20.toString(16).toUpperCase()) ?: byteArrayOf()
            ).trim()
            it.cardLabel = String(
                emv.getCardData(
                    EmvTag.APPLICATION_LABEL.toString(16).toUpperCase()
                ) ?: emv.getCardData(EmvTag.APPLICATION_LABEL.toString(16).toUpperCase()) ?: byteArrayOf()
            )
        }
    }


    override fun onTransactionResult(result: Int, data: Bundle?) {
        logTrace("onTransactionResult")
        logTrace("Result: $result")

        val message = data?.getString(ConstPBOCHandler.onTransactionResult.data.KEY_ERROR_String) ?: getTransactionResultString(result)
        logTrace("Message: $message")

        val TCString = data?.getString(ConstPBOCHandler.onTransactionResult.data.KEY_TC_DATA_String)
        logTrace("TCString: $TCString")

        val reversalDataString = data?.getString(ConstPBOCHandler.onTransactionResult.data.KEY_REVERSAL_DATA_String)
        logTrace("ReversalDataString: $reversalDataString")

        if (result in arrayOf(ConstPBOCHandler.onTransactionResult.result.AARESULT_TC, CTLS_TC, 304 )) {
            beeper.okBeep()
            if (hostResponse == null) {
                val timeMgr = IsoTimeManager()
                with (processor) {
                    hostResponse = TransactionResponse(
                        "","",
                        transactionTimeInMillis = timeMgr.now.time,
                        localDate_13 = timeMgr.shortDate,
                        localTime_12 = timeMgr.time,
                        transmissionDateTime = timeMgr.longDate,
                        transactionType = requestData.transactionType,
                        accountType = requestData.accountType,
                        amount = requestData.amount,
                        otherAmount = requestData.otherAmount,
                        maskedPan = cardData.pan.maskPan(),
                        acquiringInstCode = cardData.acquiringInstitutionIdCode,
                        responseCode = "00",
                        RRN = String.format("%012d", timeMgr.now.time),
                        STAN = timeMgr.time
                        )
                }
            } else {
                hostResponse?.responseCode = "00"
            }

            TCString?.let(::updateCardInfo)

            hostResponse?.let {
                println(it)
                processor.setResponse(it)
            }

        } else {
            beeper.errorBeep()
            if (result in arrayOf( 8/*No CO-OWNED APP*/, 12, 14/*APP BLOCKED*/, 25 /*EMV_CARD_BLOCKED*/, 205/*EMV FALLBACK*/)) {
                thread {
                    processor.fallbackTransaction()
                }
                return
            }


            processor.setError(RuntimeException(message))
        }

    }


    private val pinInputListener = object : PinInputListener.Stub() {

        override fun onInput(len: Int, key: Int) {
            logTrace("PinPad onInput, len:$len, key:$key")
        }

        override fun onConfirm(data: ByteArray?, isNonePin: Boolean) {
            logTrace("PinPad onConfirm")
            emv.importPin(1, data)
            if (!isNonePin && data?.size == 8) {
                pinBlock = data.hexString()
                logTrace("Pinblock: $pinBlock")
            }

        }

        override fun onCancel() {
            beeper.errorBeep()
            emv.importPin(0, null)
        }

        override fun onError(errorCode: Int) {
            beeper.errorBeep()
            logTrace("Pin error code: $errorCode")
            emv.importPin(0, null)
        }
    }
}