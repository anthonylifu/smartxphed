package com.xpresspayments.xpresssmartposphed.ui

import android.graphics.drawable.Drawable

open class ImageTitleItem(val image: Drawable,
                          val title: String)