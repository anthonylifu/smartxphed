package com.xpresspayments.xpresssmartposphed.ui

import android.view.View
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.xpresspayments.xpresssmartposphed.R
import com.xpresspayments.xpresssmartposphed.extensions.formatSingleExponentCurrency
import com.xpresspos.epmslib.entities.TransactionResponse
import com.xpresspos.epmslib.entities.TransactionType
import com.xpresspos.epmslib.utils.IsoAccountType
import java.text.DateFormat
import java.util.*

data class TransactionConfirmationData(val transactionType: TransactionType,
                                       val RRN: String,
                                       val amount: Long,
                                       val otherAmount: Long,
                                       val date: String,
                                       val accountType: IsoAccountType
)

fun TransactionResponse.toConfirmationData() = TransactionConfirmationData(
        transactionType = this.transactionType,
        RRN = this.RRN,
        amount = this.amount,
        otherAmount = this.otherAmount,
        date = DateFormat.getDateTimeInstance(DateFormat.MEDIUM, DateFormat.SHORT).format(Date(this.transactionTimeInMillis)),
        accountType =  this.accountType
)

class TransactionConfirmationViewHolder (view: View): RecyclerView.ViewHolder(view) {
    private val tranTypeView = view.findViewById<TextView>(R.id.transactionTypeText)
    private val amountTextView = view.findViewById<TextView>(R.id.amountText)
    private val otherAmountTextView: TextView = view.findViewById(R.id.otherAmountText)
    private val totalAmountTextView: TextView = view.findViewById(R.id.totalAmountText)
    private val rrnTextView: TextView = view.findViewById(R.id.rrnText)
    private val dateTextView: TextView = view.findViewById(R.id.dateText)
    private val accountTypeTextView: TextView = view.findViewById(R.id.accountTypeText)

    fun bind(data: TransactionConfirmationData) {
        tranTypeView.text = data.transactionType.toString()
        amountTextView.text = data.amount.formatSingleExponentCurrency()
        otherAmountTextView.text = data.otherAmount.formatSingleExponentCurrency()


        totalAmountTextView.text = (data.amount + data.otherAmount).formatSingleExponentCurrency()

        if (data.RRN.isNotEmpty()) {
            rrnTextView.text = data.RRN
        } else {
            rrnTextView.visibility = View.GONE
        }

        dateTextView.text = data.date
        accountTypeTextView.text =  data.accountType.toString()
    }
}