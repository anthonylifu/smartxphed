package com.xpresspayments.xpresssmartposphed.ui

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.core.content.ContextCompat
import androidx.paging.PagedListAdapter
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.RecyclerView
import com.xpresspayments.xpresssmartposphed.R
import com.xpresspayments.xpresssmartposphed.extensions.formatSingleExponentCurrency
import com.xpresspos.epmslib.entities.TransactionResponse
import com.xpresspos.epmslib.entities.isApproved
import java.text.SimpleDateFormat
import java.util.*

class CardHistoryAdapter(val context: Context,
                         private val onClick: ((response: TransactionResponse) -> Unit)? = null):
    PagedListAdapter<TransactionResponse, CardHistoryViewHolder>(HistoryDiffUtil) {


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): CardHistoryViewHolder {
        val view = LayoutInflater.from(context).inflate(R.layout.transaction_list_item_view, parent, false)
        return CardHistoryViewHolder(view)
    }

    override fun onBindViewHolder(holder: CardHistoryViewHolder, position: Int) {
        getItem(position)?.let {item ->
            holder.onBind(context, item)
            holder.itemView.setOnClickListener {
                onClick?.invoke(item)
            }
        }
    }

    companion object {
        val HistoryDiffUtil =  object : DiffUtil.ItemCallback<TransactionResponse>() {
            override fun areItemsTheSame(oldItem: TransactionResponse,
                                         newItem: TransactionResponse) = oldItem.STAN == newItem.STAN
                    && oldItem.RRN == newItem.RRN && oldItem.transactionType == newItem.transactionType

            override fun areContentsTheSame(oldItem: TransactionResponse, newItem: TransactionResponse) = areItemsTheSame(oldItem, newItem)
        }
    }

}


class CardHistoryViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

    private val statusIndicatorView = itemView.findViewById<ImageView>(R.id.statusIndicatorImage)
    private val dateView = itemView.findViewById<TextView>(R.id.dateText)
    private val cardPanView = itemView.findViewById<TextView>(R.id.cardPanText)
    private val rrnView = itemView.findViewById<TextView>(R.id.rrnText)
    private val totalAmountView = itemView.findViewById<TextView>(R.id.amountText)
    private val tranTypeView = itemView.findViewById<TextView>(R.id.tranTypeText)

    fun onBind(context: Context, item: TransactionResponse) {
        with(item) {
            val date = Date(transactionTimeInMillis)
            dateView.text = SimpleDateFormat("dd/MM/yy HH:mm:ss").format(date)
            statusIndicatorView.setImageDrawable(ContextCompat.getDrawable(context, if (isApproved) R.drawable.ic_approved_circle_black_24dp else R.drawable.ic_cancel_black_24dp))
            cardPanView.text = maskedPan
            rrnView.text = RRN
            totalAmountView.text = (amount + otherAmount).formatSingleExponentCurrency()
            tranTypeView.text = transactionType.toString()

        }

    }

}