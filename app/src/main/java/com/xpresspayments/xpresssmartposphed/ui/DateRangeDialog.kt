package com.xpresspayments.xpresssmartposphed.ui

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.DialogFragment
import com.savvi.rangedatepicker.CalendarPickerView
import com.xpresspayments.xpresssmartposphed.R
import kotlinx.android.synthetic.main.date_range_dialog_view.*
import kotlinx.android.synthetic.main.flat_button_layout.*
import java.text.SimpleDateFormat
import java.util.*


class DateRangeDialog(inline val onSelected: (List<Date>)->Unit): DialogFragment() {

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.date_range_dialog_view, container, false)
    }


    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)

        val maxDate = Calendar.getInstance()
        val minDate = Calendar.getInstance()

        minDate.set(Calendar.YEAR, minDate.get(Calendar.YEAR)-1)
        maxDate.set(Calendar.MONTH, maxDate.get(Calendar.MONTH)+1)

        val now = Date()

        rangePicker.init(minDate.time, maxDate.time, SimpleDateFormat("MMMM, yyyy", Locale.getDefault()))
            .inMode(CalendarPickerView.SelectionMode.RANGE)
            .withSelectedDate(now)


        nextBtnText.text = getString(android.R.string.ok)
        nextBtn.setOnClickListener {
            if (rangePicker.selectedDates.isNotEmpty()) {
                onSelected.invoke(rangePicker.selectedDates)
                dismiss()
            }

        }
    }
}