package com.xpresspayments.xpresssmartposphed.ui

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.DialogFragment
import com.xpresspayments.xpresssmartposphed.R
import kotlinx.android.synthetic.main.fragment_password_update.*

class PasswordUpdateDialog(val title: String, val originalPassword: String, val onComplete: (String) -> Unit): DialogFragment() {


    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_password_update, container, false)
    }


    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        titleView.text = title
        closeButton.setOnClickListener {
            dismiss()
        }

        saveBtn.setOnClickListener {
            if (isValidInput()) {
                onComplete(passwordEdit.text.toString())
                dismiss()
            }
        }
    }

    private fun isValidInput(): Boolean {
        val origPassword =  origPasswordEdit.text?.toString()
        val text =  passwordEdit.text?.toString()
        val confirm = confirmEdit.text?.toString()

        if (originalPassword != origPassword) {
            origInputLayout.error = "Incorrect password"
            return false
        }
        origInputLayout.error = null

        if (text.isNullOrBlank() || text.length < 4) {
            passwordInputLayout.error = "Invalid password"
            return false
        }


        if (text == origPassword) {
            passwordInputLayout.error = "Cannot reuse last password"
            return false
        }
        passwordInputLayout.error = null

        if (confirm != text) {
            confirmInputLayout.error = "Password does not match"
            return false
        }
        confirmInputLayout.error = null

        return true
    }

}