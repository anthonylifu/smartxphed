package com.xpresspayments.xpresssmartposphed.repository

import androidx.room.*
import com.xpresspos.epmslib.entities.ConfigData


@Dao
interface ConfigDataDao {
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun save(data: ConfigData)

    @Query("SELECT * FROM ConfigData LIMIT 1")
    fun get(): ConfigData?

    @Delete
    fun delete(configData: ConfigData)
}