package com.xpresspayments.xpresssmartposphed.repository

import androidx.room.Database
import androidx.room.RoomDatabase
import androidx.room.TypeConverters
import com.xpresspos.epmslib.entities.ConfigData
import com.xpresspos.epmslib.entities.KeyHolder
import com.xpresspos.epmslib.entities.TransactionResponse


@Database(entities = [ConfigData::class, KeyHolder::class, TransactionResponse::class], version = 4)
@TypeConverters(Converters::class)
abstract class AppDatabase: RoomDatabase() {
    abstract fun configDataDao(): ConfigDataDao
    abstract fun keyHolderDao(): KeyHolderDao
    abstract fun transactionResponseDao(): TransactionResponseDao
}