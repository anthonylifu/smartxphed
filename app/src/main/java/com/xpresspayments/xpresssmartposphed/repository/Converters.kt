package com.xpresspayments.xpresssmartposphed.repository

import androidx.room.TypeConverter
import com.xpresspos.epmslib.entities.TransactionType
import com.xpresspos.epmslib.utils.IsoAccountType

class Converters {

    @TypeConverter
    fun transactionTypeToInt(transactionType: TransactionType) = transactionType.ordinal

    @TypeConverter
    fun intToTransactionType(value: Int) = TransactionType.values()[value]


    @TypeConverter
    fun accountTypeToInt(accountType: IsoAccountType) = accountType.ordinal

    @TypeConverter
    fun intToAccountType(value: Int) = IsoAccountType.parseIntAccountType(value)
}