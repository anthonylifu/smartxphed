package com.xpresspayments.xpresssmartposphed.repository

import androidx.room.*
import com.xpresspos.epmslib.entities.KeyHolder

@Dao
interface KeyHolderDao {
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun save(data: KeyHolder)

    @Query("SELECT * FROM KeyHolder LIMIT 1")
    fun get(): KeyHolder?

    @Delete
    fun delete(configData: KeyHolder)
}