package com.xpresspayments.xpresssmartposphed.repository

import android.app.Application
import android.util.Log
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.xpresspayments.xpresssmartposphed.db
import com.xpresspos.epmslib.entities.ConfigData
import com.xpresspos.epmslib.entities.TransactionResponse
import com.xpresspos.epmslib.utils.ReceiptType
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch

class ReceiptDataRepository(val application: Application) {
    private var TAG = ReceiptDataRepository::class.java.simpleName
    private var transactionResponseLiveData = MutableLiveData<TransactionResponse?>()
    private var configDataLiveData = MutableLiveData<ConfigData?>()
    private var successfulPrintLiveData = MutableLiveData<ReceiptType?>()
    fun getTransactionData(rrn: String, terminalId: String){
        var transactionResponse: TransactionResponse?
        GlobalScope.launch(Dispatchers.IO){
            try{
                val transactionResponseDao = application.db.transactionResponseDao()
                transactionResponse = transactionResponseDao.findTransactionByRrn(rrn)
                transactionResponseLiveData.postValue(transactionResponse!!)
            } catch (e : Exception){
                Log.e(TAG, e.message!!)
                transactionResponseLiveData.postValue(null)
            }
        }
    }

    fun getTransactionResponseLiveData() : LiveData<TransactionResponse?> {
        return transactionResponseLiveData
    }

    fun loadConfigData() {
        var configData: ConfigData?
        GlobalScope.launch(Dispatchers.IO){
            try{
                val configDataDao = application.db.configDataDao()
                configData = configDataDao.get()
                configDataLiveData.postValue(configData!!)
            } catch (e : Exception){
                Log.e(TAG, e.message!!)
                configDataLiveData.postValue(null)
            }
        }
    }

    fun getConfigDataObservable() : LiveData<ConfigData?> {
        return configDataLiveData
    }

    fun postSuccessfulPrint(receiptType: ReceiptType) {
        successfulPrintLiveData.postValue(receiptType)
    }

    fun getSuccessfulPrintObservable() : LiveData<ReceiptType?> {
        return successfulPrintLiveData
    }
}