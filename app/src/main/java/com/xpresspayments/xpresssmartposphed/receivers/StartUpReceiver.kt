package com.xpresspayments.xpresssmartposphed.receivers

import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent


class StartUpReceiver: BroadcastReceiver() {

    override fun onReceive(context: Context, intent: Intent?) {
        intent?.let {
            if (it.action == Intent.ACTION_BOOT_COMPLETED) {
                /*val newIntent = Intent(context, MainActivity::class.java)
                newIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK)
                context.startActivity(newIntent)*/
            }
        }
    }
}