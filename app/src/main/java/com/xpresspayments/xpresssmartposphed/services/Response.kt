package com.xpresspayments.xpresssmartposphed.services


import com.google.gson.annotations.SerializedName

data class Response(
    @SerializedName("ResponseCode")
    val responseCode: String,
    @SerializedName("ResponseMessage")
    val responseMessage: String
)