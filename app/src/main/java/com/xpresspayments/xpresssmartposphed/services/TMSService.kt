package com.xpresspayments.xpresssmartposphed.services

import retrofit2.Retrofit
import retrofit2.http.GET
import retrofit2.http.Query
import retrofit2.http.Url


interface TMSService {
    @GET
   suspend fun  downloadConfiguration(@Url url: String, @Query("tid") terminalId: String, @Query("sn") terminalSerial: String): TmsConfig

}

val Retrofit.tmsService: TMSService
    get() = create(TMSService::class.java)