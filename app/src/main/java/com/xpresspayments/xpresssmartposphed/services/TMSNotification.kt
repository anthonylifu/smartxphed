package com.xpresspayments.xpresssmartposphed.services

import com.xpresspos.epmslib.entities.TransactionResponse
import com.xpresspos.epmslib.entities.isApproved
import com.xpresspos.epmslib.entities.responseMessage
import retrofit2.Retrofit
import retrofit2.http.Body
import retrofit2.http.POST
import java.text.SimpleDateFormat


data class NotificationResponse(val responseCode: String,
                                val responseMessage: String)

interface TmsNotificationService{
    @POST("rws/notification")
    suspend fun pushNotification(@Body notificationData: NotificationData): NotificationResponse
}

val Retrofit.tmsNotificationService: TmsNotificationService
     get() =  this.create(TmsNotificationService::class.java)

fun TransactionResponse.toNotificationData(locationString: String): NotificationData {
   val transactiondate = SimpleDateFormat("yyyy-MM-dd").format(transactionTimeInMillis)
   val  transactiontime = SimpleDateFormat("HH:mm:ss").format(transactionTimeInMillis)
    val localDate =  SimpleDateFormat("yy/MM/dd").format(transactionTimeInMillis)

    return NotificationData(
        terminalid = terminalId,
        mti = transactionType.MTI.toString(16),
        amount =  amount.toString(),
        stan =  STAN,
        seqno = STAN,
        pan = maskedPan,
        track2 = "${maskedPan}D${cardExpiry}000000",
        posentrymode = "051",
        posconditioncode = "00",
        aurhorisationresponse = authCode,
        rrn = RRN,
        responcecode = responseCode,
        responsedescription = responseMessage,
        trancode = transactionType.ordinal.toString(),
        merchantId = merchantId,
        trantype = transactionType.toString(),
        tranTypeCode = transactionType.ordinal.toString(),
        tStatus = if (isApproved) "APPROVED" else "DECLINED",
        timelocaltransaction = transactiontime,
        datelocaltransaction = localDate,
        transactiondate = transactiondate,
        transactiontime = transactiontime,
        accounttype = accountType.code.toInt(),
        iccdata = iccData,
        gpsinfo =   locationString,
        track1 = cardHolder,
        refcode = echoData ?: "",
        processingcode =  "${transactionType.code}${accountType.code}00",
        authorizationCode = authCode,
        cellinfo = "",
        narration = "",
        revenueCode = "",
        otherterminalid = "",
        customerName = cardHolder,
        customerOtherInfo = ""
    )
}
