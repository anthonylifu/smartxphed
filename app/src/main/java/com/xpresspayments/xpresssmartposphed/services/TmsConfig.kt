package com.xpresspayments.xpresssmartposphed.services


import com.google.gson.annotations.SerializedName

data class TmsConfig(
    @SerializedName("agentID")
    val agentID: String,

    @SerializedName("agentName")
    val agentName: String,

    @SerializedName("ConsultantCode")
    val consultantCode: String,

    @SerializedName("EnablePayAttitude")
    val enablePayAttitude: Boolean,

    @SerializedName("HostKeys")
    val hostKeys: HostKeys,

    @SerializedName("InstitutionCode")
    val institutionCode: String,

    @SerializedName("IpAddress")
    val ipAddress: String,

    @SerializedName("Location")
    val location: String,

    @SerializedName("logo")
    val logo: String,

    @SerializedName("Menu")
    val menu: List<Int>,

    @SerializedName("MerchantID")
    val merchantID: String,

    @SerializedName("MerchantName")
    val merchantName: String,

    @SerializedName("Password")
    val password: String,

    @SerializedName("PayAttitudeConfig")
    val payAttitudeConfig: Any?,

    @SerializedName("Port")
    val port: String,

    @SerializedName("receiptFooter")
    val receiptFooter: String,

    @SerializedName("receiptHeader")
    val receiptHeader: String,

    @SerializedName("response")
    val response: Response,

    @SerializedName("SimConfig")
    val simConfig: SimConfig,

    @SerializedName("terminalID")
    val terminalID: String,

    @SerializedName("TerminalMode")
    val terminalMode: Int,

    @SerializedName("terminalSerial")
    val terminalSerial: String,

    @SerializedName("TestPlatform")
    val testPlatform: Int,

    @SerializedName("Username")
    val username: String,

    @SerializedName("VasConfig")
    val vasConfig: Any?
)