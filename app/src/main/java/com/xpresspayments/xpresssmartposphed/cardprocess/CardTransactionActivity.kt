package com.xpresspayments.xpresssmartposphed.cardprocess

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import android.util.Log
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.os.bundleOf
import androidx.navigation.findNavController
import com.xpresspayments.commons.EmvProcessor
import com.xpresspayments.commons.ProcessState
import com.xpresspayments.xpresssmartposphed.R
import com.xpresspayments.xpresssmartposphed.pos
import com.xpresspos.epmslib.entities.*
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.disposables.Disposable
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.delay
import kotlinx.coroutines.launch

class CardTransactionActivity : AppCompatActivity(), TransactionAborter {

    private lateinit var requestData: TransactionRequestData
    private lateinit var hostConfig: HostConfig
    private var autoPrint = true
    private var reader = com.xpresspayments.commons.Reader.CT_CTLS


    private val emvProcessor: EmvProcessor by lazy {
        pos.getEmvProcessor(this, hostConfig)
    }

    private val compositeDisposable by lazy {
        CompositeDisposable()
    }

    private val navController by lazy {
        findNavController(R.id.navHost)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_card_transaction)

        /*requestData = TransactionRequestData(TransactionType.PURCHASE, 100,0,IsoAccountType.DEFAULT_UNSPECIFIED,"123456",null)
        intent.putExtra(REQUEST_DATA, requestData)*/

        with(intent.getBundleExtra(BUNDLE)) {
            requestData = this?.get(REQUEST_DATA) as? TransactionRequestData ?: kotlin.run {
                error("TransactionRequestData required")
            }

            hostConfig = this?.get(HOST_CONFIG) as? HostConfig ?: kotlin.run {
                error("HostConfig required")
            }

            reader = get(READERS) as? com.xpresspayments.commons.Reader ?: kotlin.run {
                com.xpresspayments.commons.Reader.CT_CTLS_MSR
            }
        }

        compositeDisposable.add(emvProcessor.onStateChanged
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe {
            when (it.state) {
                ProcessState.States.CARD_ENTRY -> showScreen(
                    R.id.cardEntryFragment,
                    bundleOf(
                        Pair(
                            INSERT_MESSAGE,
                            it.message
                        )
                    )
                )
                ProcessState.States.PROCESSING -> showProcessing(it.message)
            }
        })

        emvProcessor.onResponse
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe(object : io.reactivex.Observer<TransactionResponse> {
                override fun onSubscribe(d: Disposable) {
                    compositeDisposable.add(d)
                }

                override fun onNext(response: TransactionResponse) {
                    GlobalScope.launch {
                        response.let { response ->
                            println(response)

                            if (autoPrint) {
                                //print receipt
                            }
                            showScreen(
                                R.id.resultFragment, bundleOf(
                                    Pair(STATUS, response.isApproved),
                                    Pair(MESSAGE, response.responseMessage)
                                )
                            )

                            delay(5000)

                            val result = Intent().apply {
                                putExtra(TRANSACTION_RESPONSE, response)
                            }

                            setResult(Activity.RESULT_OK, result)
                        }

                        finish()
                    }
                }

                override fun onError(e: Throwable) {
                    GlobalScope.launch {
                        val error = Intent().apply {
                            putExtra(ERROR, e)
                        }

                        showScreen(
                            R.id.resultFragment, bundleOf(
                                Pair(STATUS, false),
                                Pair(MESSAGE, e.localizedMessage)
                            )
                        )

                        delay(5000)

                        setResult(Activity.RESULT_CANCELED, error)
                        finish()
                    }
                }

                override fun onComplete() {
                    Log.d("OnResponse", "Completed")
                }
            })


        GlobalScope.launch {
            emvProcessor.startTransaction(requestData, reader)
        }

    }


    override fun onBackPressed() {
//        super.onBackPressed()  // disable back button
    }

    override fun onDestroy() {
        super.onDestroy()
        compositeDisposable.clear()
    }

    companion object {
        const val REQUEST_DATA = "request_data"
        const val HOST_CONFIG = "host_config"
        const val READERS = "readers"
        const val TRANSACTION_RESPONSE = "transaction_response"
        const val ERROR = "error"
        const val BUNDLE = "cardOption"
    }


    private fun showScreen(destinationId: Int, bundle: Bundle?) {
        com.xpresspayments.commons.runOnMain {
            navController.popBackStack(R.id.cardEntryFragment, false)
            navController.navigate(destinationId, bundle)
        }
    }

    private fun showProcessing(message: String = getString(R.string.processing)) {
        showScreen(R.id.waitingFragment, bundleOf(Pair("message", message)))
    }

    override fun abortTransaction() {
        emvProcessor.abortTransaction()
    }
}


interface TransactionAborter {
    fun abortTransaction()
}