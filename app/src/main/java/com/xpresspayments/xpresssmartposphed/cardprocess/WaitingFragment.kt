package com.xpresspayments.xpresssmartposphed.cardprocess


import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.xpresspayments.xpresssmartposphed.R
import kotlinx.android.synthetic.main.fragment_waiting.*

class WaitingFragment : Fragment() {

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_waiting, container, false)
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)

        arguments?.let { args ->
            val message = args.getString(MESSAGE, getString(R.string.processing))
            messageText.text = message
        }

    }

}
