package com.xpresspayments.xpresssmartposphed.extensions

import com.jakewharton.retrofit2.adapter.kotlin.coroutines.CoroutineCallAdapterFactory
import com.xpresspayments.commons.logTrace
import okhttp3.MediaType.Companion.toMediaType
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor

import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory


fun buildRetrofit(baseUrl: String): Retrofit = Retrofit.Builder()
    .baseUrl(baseUrl)
    .addCallAdapterFactory(CoroutineCallAdapterFactory())
    .addConverterFactory(GsonConverterFactory.create())
    .client(OkHttpClient.Builder().apply {
        addInterceptor(HttpLoggingInterceptor(logger).apply {
            level = HttpLoggingInterceptor.Level.BODY
        })

    }.build())
    .build()

val JSON = "application/json; charset=utf-8".toMediaType();
val XML = "application/xml; charset=utf-8".toMediaType();


val logger =  HttpLoggingInterceptor.Logger {
   // Bugfender.d("Retrofit", it)
    logTrace(it)
}