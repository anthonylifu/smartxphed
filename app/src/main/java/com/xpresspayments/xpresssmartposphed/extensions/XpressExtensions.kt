package com.xpresspayments.xpresssmartposphed.extensions

import android.content.Context
import android.content.SharedPreferences
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import androidx.appcompat.app.AlertDialog
import androidx.preference.PreferenceManager
import com.google.gson.Gson
import com.xpresspayments.commons.printer.Printer
import com.xpresspayments.commons.printer.TextFormat
import com.xpresspayments.commons.runOnMain
import com.xpresspayments.xpresssmartposphed.App
import com.xpresspayments.xpresssmartposphed.R
import com.xpresspayments.xpresssmartposphed.db
import com.xpresspayments.xpresssmartposphed.fragments.viewmodels.ReceiptDataViewModel
import com.xpresspayments.xpresssmartposphed.printer
import com.xpresspayments.xpresssmartposphed.services.TmsConfig
import com.xpresspayments.xpresssmartposphed.utils.CallHomeWorker
import com.xpresspayments.xpresssmartposphed.utils.prePrintFooter
import com.xpresspayments.xpresssmartposphed.utils.prePrintHeader
import com.xpresspos.epmslib.entities.*
import com.xpresspos.epmslib.utils.ReceiptType
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.async
import kotlinx.coroutines.runBlocking
import java.util.*


val Context.printerLogo: Bitmap
    get() = if (fileList().contains(getString(R.string.logo_file_name))) {
        BitmapFactory.decodeStream(
            this.openFileInput(getString(R.string.logo_file_name)),
            null,
            BitmapFactory.Options().apply {
                inPreferredConfig = Bitmap.Config.ALPHA_8
            })!!
    } else {
        BitmapFactory.decodeResource(
            resources,
            R.drawable.receipt_logo,
            BitmapFactory.Options().apply {
                inPreferredConfig = Bitmap.Config.ALPHA_8
            })
    }

val Context.logo: Bitmap
    get() = if (fileList().contains(getString(R.string.logo_file_name))) {
        BitmapFactory.decodeStream(this.openFileInput(getString(R.string.logo_file_name)))
    } else {
        BitmapFactory.decodeResource(resources, R.drawable.receipt_logo)
    }

val Context.prefs: SharedPreferences
    get() = PreferenceManager.getDefaultSharedPreferences(this)

val Context.preauthEnabled: Boolean
    get() = prefs.getBoolean(getString(R.string.enable_preauth_key), false)

val Context.accountSelectionEnabled: Boolean
    get() = prefs.getBoolean(getString(R.string.enable_account_selection_key), false)

val Context.isPrepped: Boolean
    get() = prefs.getBoolean(getString(R.string.terminal_prepped_key), false)

val Context.adminPassword: String
    get() = prefs.getString(getString(R.string.update_admin_password_key), "6798")!!

val Context.supervisorPassword: String
    get() = prefs.getString(getString(R.string.update_supervisor_password_key), "1234")!!

val Context.operatorPassword: String
    get() = prefs.getString(getString(R.string.update_operator_password_key), "1234")!!

val Context.connectionData: ConnectionData
    get() {
        val ipAddress =
            prefs.getString(getString(R.string.host_ip_key), getString(R.string.default_host_ip))!!
        val port = prefs.getString(
            getString(R.string.host_port_key),
            getString(R.string.default_host_port)
        )!!
        val isOpenPort = prefs.getBoolean(getString(R.string.host_connection_key), false)
        val isTestPlatform = prefs.getBoolean(getString(R.string.host_platform_key), false)

        return ConnectionData(
            ipAddress = ipAddress,
            ipPort = port.toIntOrNull() ?: 0,
            isSSL = !isOpenPort,
            certFileResId = if (isTestPlatform) R.raw.nibss_cert_test else R.raw.nibss_cert_live
        )
    }

val Context.hostConfig: HostConfig?
    get() = runBlocking {
        if (!isPrepped) return@runBlocking null

        val terminalId = prefs.getString(getString(R.string.terminal_id_key), null)
            ?: return@runBlocking null

        GlobalScope.async {
            val configData = db.configDataDao().get() ?: return@async null
            val keyHolder = db.keyHolderDao().get() ?: return@async null
            HostConfig(terminalId, connectionData, keyHolder, configData)
        }.await()
    }


val Context.tmsConfig: TmsConfig?
    get() = Gson().fromJson(
        prefs.getString(getString(R.string.tms_config_key), ""),
        TmsConfig::class.java
    )


const val DEFAULT_CALL_HOME_TIME = 3600
fun App.startCallHomeWorker() {
    timer?.let {
        it.cancel()
        it.purge()
    }

    timer = Timer(CallHomeWorker.WORKER_NAME)
    val prefContent =
        prefs.getString(getString(R.string.call_home_time_key), DEFAULT_CALL_HOME_TIME.toString())!!
    println("CALL HOME PREF CONTENT: $prefContent")

    val callHomeTime = DEFAULT_CALL_HOME_TIME.toLong().coerceAtLeast(
        (prefContent.toLongOrNull() ?: DEFAULT_CALL_HOME_TIME.toLong())
    ) * 1000
    println("Call home time in millis: $callHomeTime")


    timer?.scheduleAtFixedRate(CallHomeWorker(this), callHomeTime, callHomeTime)
}


fun Context.doPrint(printer: Printer, callback: ((Printer.Status) -> Unit)? = null) {
    when (val status = printer.print()) {
        Printer.Status.OK -> {
            callback?.invoke(status)
        }
        else -> {
            runOnMain {
                AlertDialog.Builder(this)
                    .setTitle(com.xpresspayments.newland910.R.string.printer_error)
                    .setMessage(status.toString())
                    .setPositiveButton(com.xpresspayments.newland910.R.string.retry) { _, _ ->
                        doPrint(printer, callback)
                    }.setNegativeButton(android.R.string.cancel, null)
                    .show()
            }
            callback?.invoke(status)
        }
    }
}

fun Context.printCustomTransactionResult(
    configData: ConfigData,
    response: TransactionResponse,
    receiptData: Map<String, String>,
    receiptType: ReceiptType,
    receiptDataViewModel: ReceiptDataViewModel,
    isReprint: Boolean
) {
    val printer = this.printer

    with(response) {
        if (null != configData) {
            prePrintHeader(printer, configData, transactionTimeInMillis)
        }

        //Body Preceeding
        printer.addText(
            transactionType.toString(),
            TextFormat(
                com.xpresspayments.commons.printer.FontSize.LARGE,
                com.xpresspayments.commons.printer.Align.CENTER
            )
        )

        printer.addLine()
        printer.addText("+++$receiptType+++")
        printer.addLine()

        if (isReprint) {
            printer.addText("+++REPRINT+++")
            printer.addLine()
        }

        printer.feedLine(1)
        printer.addDoubleText(
            getString(com.xpresspayments.xpresssmartposphed.R.string.pan).toUpperCase(),
            maskedPan
        )
        printer.addDoubleText(
            getString(com.xpresspayments.xpresssmartposphed.R.string.expiry).toUpperCase(),
            cardExpiry
        )
        printer.addDoubleText(
            getString(com.xpresspayments.xpresssmartposphed.R.string.name).toUpperCase(),
            cardHolder
        )
        printer.addDoubleText(
            getString(com.xpresspayments.xpresssmartposphed.R.string.stan).toUpperCase(),
            STAN
        )
        printer.addDoubleText(
            getString(com.xpresspayments.xpresssmartposphed.R.string.rrn).toUpperCase(),
            RRN
        )
        printer.addDoubleText(
            getString(com.xpresspayments.xpresssmartposphed.R.string.auth_id).toUpperCase(),
            authCode
        )
        if (accountSelectionEnabled) {
            printer.addDoubleText(
                getString(com.xpresspayments.xpresssmartposphed.R.string.account_type).toUpperCase(),
                accountType.toString()
            )
        }

        if (null != receiptData) {
            for ((key, value) in receiptData) {
                if (null != key && null != value) {
                    printer.addDoubleText(key.toUpperCase(), value)
                }
            }
        }


        when (response.transactionType) {
            com.xpresspos.epmslib.entities.TransactionType.BALANCE, com.xpresspos.epmslib.entities.TransactionType.LINK_ACCOUNT_INQUIRY -> {
                if (response.additionalAmount_54.isNotBlank()) {
                    parseField54AdditionalAmount(response.additionalAmount_54)
                        .forEach {
                            printer.addDoubleText(
                                it.accountType.toString(),
                                it.amount.formatSingleExponentCurrency(currencySymbolFromCode(it.currencyCode.toInt()))
                            )
                        }
                }
            }
            else -> {
                if (response.transactionType == com.xpresspos.epmslib.entities.TransactionType.PURCHASE_WITH_CASH_BACK) {
                    printer.addDoubleText(
                        getString(com.xpresspayments.xpresssmartposphed.R.string.amount).toUpperCase(),
                        amount.formatSingleExponentCurrency()
                    )
                    printer.addDoubleText(
                        getString(com.xpresspayments.xpresssmartposphed.R.string.other_amount).toUpperCase(),
                        otherAmount.formatSingleExponentCurrency()
                    )

                }

                printer.addText("++++++++++")
                printer.addText(
                    (amount + otherAmount).formatSingleExponentCurrency(), TextFormat(
                        com.xpresspayments.commons.printer.FontSize.LARGE
                    )
                )
                printer.addText("++++++++++")
            }
        }

        printer.addText(
            if (isApproved) getString(com.xpresspayments.xpresssmartposphed.R.string.approved) else getString(
                com.xpresspayments.xpresssmartposphed.R.string.declined
            ), TextFormat(
                fontSize = com.xpresspayments.commons.printer.FontSize.LARGE
            )
        )


        printer.addText(
            responseMessage,
            TextFormat(com.xpresspayments.commons.printer.FontSize.NORMAL)
        )
        printer.addLine()

        printer.addDoubleText(
            getString(com.xpresspayments.xpresssmartposphed.R.string.aid).toUpperCase(),
            AID
        )
        printer.addDoubleText(
            getString(com.xpresspayments.xpresssmartposphed.R.string.card).toUpperCase(),
            cardLabel
        )

        printer.addDoubleText(
            getString(com.xpresspayments.xpresssmartposphed.R.string.ac).toUpperCase(),
            appCryptogram
        )

        printer.addDoubleText(
            getString(com.xpresspayments.xpresssmartposphed.R.string.tvr).toUpperCase(),
            TVR
        )

        printer.addDoubleText(
            getString(com.xpresspayments.xpresssmartposphed.R.string.tsi).toUpperCase(),
            TSI
        )

        prePrintFooter(printer)
    }
    val callback =
        fun(status: Printer.Status): Unit {
            if (status == Printer.Status.OK) {
              receiptDataViewModel.postSuccessfulPrint(receiptType)
            }
        }
    doPrint(printer, callback)
}

fun Context.printTransactionResult(
    configData: ConfigData,
    response: TransactionResponse,
    receiptType: ReceiptType,
    isReprint: Boolean
) {

    val printer = this.printer

    with(response) {
        prePrintHeader(printer, configData, transactionTimeInMillis)

        //Body Preceeding
        printer.addText(
            transactionType.toString(),
            TextFormat(
                com.xpresspayments.commons.printer.FontSize.LARGE,
                com.xpresspayments.commons.printer.Align.CENTER
            )
        )

        printer.addLine()
        printer.addText("+++$receiptType+++")
        printer.addLine()

        if (isReprint) {
            printer.addText("+++REPRINT+++")
            printer.addLine()
        }

        printer.feedLine(1)
        printer.addDoubleText(
            getString(com.xpresspayments.xpresssmartposphed.R.string.pan).toUpperCase(),
            maskedPan
        )
        printer.addDoubleText(
            getString(com.xpresspayments.xpresssmartposphed.R.string.expiry).toUpperCase(),
            cardExpiry
        )
        printer.addDoubleText(
            getString(com.xpresspayments.xpresssmartposphed.R.string.name).toUpperCase(),
            cardHolder
        )
        printer.addDoubleText(
            getString(com.xpresspayments.xpresssmartposphed.R.string.stan).toUpperCase(),
            STAN
        )
        printer.addDoubleText(
            getString(com.xpresspayments.xpresssmartposphed.R.string.rrn).toUpperCase(),
            RRN
        )
        printer.addDoubleText(
            getString(com.xpresspayments.xpresssmartposphed.R.string.auth_id).toUpperCase(),
            authCode
        )
        if (accountSelectionEnabled) {
            printer.addDoubleText(
                getString(com.xpresspayments.xpresssmartposphed.R.string.account_type).toUpperCase(),
                accountType.toString()
            )
        }


        when (response.transactionType) {
            com.xpresspos.epmslib.entities.TransactionType.BALANCE, com.xpresspos.epmslib.entities.TransactionType.LINK_ACCOUNT_INQUIRY -> {
                if (response.additionalAmount_54.isNotBlank()) {
                    parseField54AdditionalAmount(response.additionalAmount_54)
                        .forEach {
                            printer.addDoubleText(
                                it.accountType.toString(),
                                it.amount.formatSingleExponentCurrency(currencySymbolFromCode(it.currencyCode.toInt()))
                            )
                        }
                }
            }
            else -> {
                if (response.transactionType == com.xpresspos.epmslib.entities.TransactionType.PURCHASE_WITH_CASH_BACK) {
                    printer.addDoubleText(
                        getString(com.xpresspayments.xpresssmartposphed.R.string.amount).toUpperCase(),
                        amount.formatSingleExponentCurrency()
                    )
                    printer.addDoubleText(
                        getString(com.xpresspayments.xpresssmartposphed.R.string.other_amount).toUpperCase(),
                        otherAmount.formatSingleExponentCurrency()
                    )

                }

                printer.addText("++++++++++")
                printer.addText(
                    (amount + otherAmount).formatSingleExponentCurrency(), TextFormat(
                        com.xpresspayments.commons.printer.FontSize.LARGE
                    )
                )
                printer.addText("++++++++++")
            }
        }

        printer.addText(
            if (isApproved) getString(com.xpresspayments.xpresssmartposphed.R.string.approved) else getString(
                com.xpresspayments.xpresssmartposphed.R.string.declined
            ), TextFormat(
                fontSize = com.xpresspayments.commons.printer.FontSize.LARGE
            )
        )


        printer.addText(
            responseMessage,
            TextFormat(com.xpresspayments.commons.printer.FontSize.NORMAL)
        )
        printer.addLine()

        printer.addDoubleText(
            getString(com.xpresspayments.xpresssmartposphed.R.string.aid).toUpperCase(),
            AID
        )
        printer.addDoubleText(
            getString(com.xpresspayments.xpresssmartposphed.R.string.card).toUpperCase(),
            cardLabel
        )

        printer.addDoubleText(
            getString(com.xpresspayments.xpresssmartposphed.R.string.ac).toUpperCase(),
            appCryptogram
        )

        printer.addDoubleText(
            getString(com.xpresspayments.xpresssmartposphed.R.string.tvr).toUpperCase(),
            TVR
        )

        printer.addDoubleText(
            getString(com.xpresspayments.xpresssmartposphed.R.string.tsi).toUpperCase(),
            TSI
        )

        prePrintFooter(printer)
    }

    doPrint(printer)
}

