package com.xpresspayments.xpresssmartposphed.extensions

import android.annotation.SuppressLint
import android.app.Activity
import android.content.Context
import android.location.LocationManager
import androidx.appcompat.app.AlertDialog
import androidx.fragment.app.FragmentActivity
import androidx.lifecycle.Observer
import com.xpresspayments.commons.runOnMain
import com.xpresspayments.xpresssmartposphed.R
import com.xpresspayments.xpresssmartposphed.onErrorMessage
import com.xpresspayments.xpresssmartposphed.ui.PasswordDialogFragment


fun FragmentActivity.validateAccess(title: String, validationString: String, onValidate: ()->Unit , onError: (()->Unit)) {
    val dialog = PasswordDialogFragment(title)
    dialog.password.observe(this, Observer {
        if (it == validationString) {
            onValidate()
        } else {
            onError.invoke()
        }
        dialog.dismiss()
    })

    dialog.show(supportFragmentManager, dialog.tag)
}

fun FragmentActivity.validateAdminAccess(onValidate: () -> Unit) {
    validateAccess(getString(R.string.admin_password),  adminPassword, onValidate) {
        onErrorMessage(R.string.error_title, Exception(getString(R.string.invalid_password)))
    }
}

fun FragmentActivity.validateSupervisorAccess(onValidate: () -> Unit) {
    validateAccess(getString(R.string.supervisor_password), supervisorPassword, onValidate) {
        onErrorMessage(R.string.error_title, Exception(getString(R.string.invalid_password)))
    }
}


fun Activity.showInfo(message: String?, title: String?, onDismiss: (() -> Unit)? = null) {
    runOnMain {
        AlertDialog.Builder(this)
            .setTitle(title)
            .setMessage(message)
            .setPositiveButton(android.R.string.ok){_, _ ->
                onDismiss?.invoke()
            }
            .show()
    }
}




val Context.locationString: String @SuppressLint("MissingPermission")
get() {
    var  latLong = ""
    runCatching {
        val location = (getSystemService(Context.LOCATION_SERVICE) as LocationManager).getLastKnownLocation(
            LocationManager.PASSIVE_PROVIDER)
        location?.let {
            it.toString()
            latLong = String.format(" %.6f,%.6f", it.latitude, it.longitude)
        }
    }

    return latLong
}


