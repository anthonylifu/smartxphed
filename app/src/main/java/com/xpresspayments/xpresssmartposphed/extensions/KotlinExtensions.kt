package com.xpresspayments.xpresssmartposphed.extensions

import android.content.Context
import android.util.Log
import java.security.MessageDigest
import java.text.DecimalFormat


const val MAX_PHONE_NUMBER_LENGTH = 11
const val NAIRA = '\u20A6'

fun Any.error(message: String, cause: Throwable? = null) = RuntimeException(message, cause)

fun Context.resError(stringRes: Int) = error(getString(stringRes))

fun Number.formatCurrency(currencyIndicator: String = NAIRA.toString()): String {
    return "$currencyIndicator ${DecimalFormat("#,##0.00").format(this)}"
}

fun Number.formatNonDecimalCurrency(currencyIndicator: String = NAIRA.toString()): String {
    return "$currencyIndicator ${DecimalFormat("#,###").format(this)}"
}

fun Number.formatSingleExponentCurrency(currencyIndicator: String = NAIRA.toString()): String {
    return "$currencyIndicator ${DecimalFormat("#,##0.00").format(this.toDouble()/100.0)}"
}

val ByteArray.hexString: String
    get() {
        var hexString = ""
        for (b in this) {
            hexString += String.format("%02X", b)
        }
        return hexString
    }


val String.hexByteArray: ByteArray
    get() {

        val str = "0123456789ABCDEF"
        val bytes = ByteArray(this.length / 2)
        var i = 0
        var j = 0
        while (i < this.length) {

            val firstQuad = (str.indexOf(this[i]) shl 4).toByte()
            val secondQuad = str.indexOf(this[++i]).toByte()
            bytes[j++] = (firstQuad.toInt() or secondQuad.toInt()).toByte()
            i++
        }
        return bytes
    }

fun String.hash(algorithm: String = "sha-512", salt: String = ""): String {
    val digest = MessageDigest.getInstance(algorithm)
    digest.update(this.toByteArray())
    if (salt.isNotEmpty()) {
        digest.update(salt.toByteArray())
    }

    val final = digest.digest()

    return final.hexString
}

fun String.sanitizeAmountString(currencyIdentifier: String = NAIRA.toString())
        = this.replace(currencyIdentifier, "")
        .replace(",", "")
        .replace(" ", "")
        .trim()

fun String.formatCurrency(currencyIdentifier: String = NAIRA.toString()): String {
    val amount:Number = this.sanitizeAmountString().toDoubleOrNull() ?: 0.0

    return amount.formatCurrency(currencyIdentifier)
}

fun String.formatNonDecimalCurrency(currencyIdentifier: String = NAIRA.toString()): String {
    val amount:Number = this.sanitizeAmountString().toIntOrNull() ?: 0

    return amount.formatNonDecimalCurrency(currencyIdentifier)
}

fun String.formatSingleExponentCurrency(currencyIdentifier: String = NAIRA.toString()): String {
    val amount:Number = this.sanitizeAmountString().toLongOrNull() ?: 0L

    return amount.formatSingleExponentCurrency(currencyIdentifier)
}

fun String.formatInternationalPhoneNumber(prefix: String = "+234") = StringBuilder(this).apply {
    deleteCharAt(0)
    insert(0, prefix)
    Log.d("PHONE", this.toString())
}