package com.xpresspayments.xpresssmartposphed.fragments


import android.content.Intent
import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.view.*
import androidx.core.os.bundleOf
import androidx.fragment.app.Fragment
import com.xpresspayments.commons.navController
import com.xpresspayments.commons.showSnack
import com.xpresspayments.xpresssmartposphed.ActionBarTitleUpdater
import com.xpresspayments.xpresssmartposphed.MainActivity

import com.xpresspayments.xpresssmartposphed.R
import com.xpresspayments.xpresssmartposphed.extensions.*
import com.xpresspayments.xpresssmartposphed.ui.IsoAccountTypeListDialogFragment
import com.xpresspos.epmslib.entities.TransactionType
import com.xpresspos.epmslib.utils.IsoAccountType
import kotlinx.android.synthetic.main.button_layout.*
import kotlinx.android.synthetic.main.fragment_amount_entry.*


open class AmountEntryFragment : Fragment(), View.OnClickListener {

    protected lateinit var transactionType: TransactionType

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_amount_entry, container, false)
    }


    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)

        transactionType =
            arguments?.get(getString(R.string.transaction_type)) as? TransactionType ?: kotlin.run {
                navController.popBackStack()
                return
            }

        val intent = requireActivity().getIntent()

        if (intent!=null && intent.getExtras()!=null) {

            val collectionAmountStr = intent.getStringExtra(CardUtils.TRANSACTION_AMOUNT)!!
            println("Amount String from PHED: $collectionAmountStr")
            val rrn = intent.getStringExtra(CardUtils.TRANSACTION_ID)!!
            val metreNo = intent.getStringExtra(CardUtils.METRE_NO)!!
            amountTextView.text = collectionAmountStr

            (activity as ActionBarTitleUpdater).updateTitle(transactionType.toString())
            amountTextView.addTextChangedListener(amountTextWatcher)
            nextBtn.setOnClickListener(this)
        } else {
            showSnack(getString(R.string.function_not_enabled))
            val intent2 = Intent(requireContext(), MainActivity::class.java)
            requireActivity().startActivity(intent2)
            requireActivity().finish()
        }
    }

    private fun next(transactionType: TransactionType, accountType: IsoAccountType, amount: Long) {

        val destination = if (transactionType.hasAdditionalAmount) {
            R.id.action_amountEntryFragment_to_additionalAmountEntryFragment
        } else {
            R.id.action_amountEntryFragment_to_transactionProcessFragment
        }

        navController.navigate(
            destination, bundleOf(
                Pair(getString(R.string.transaction_type), transactionType),
                Pair(getString(R.string.transaction_amount), amount),
                Pair(getString(R.string.account_type), accountType)
            )
        )
    }

    override fun onClick(v: View?) {
        val amount: Long =
            amountTextView.text.toString().sanitizeAmountString().replace(".", "").toLongOrNull()
                ?: 0L
        if (amount <= 0) {
            showSnack(getString(R.string.invalid_amount))
            return
        }

        if (requireContext().accountSelectionEnabled && !transactionType.hasAdditionalAmount) {
            IsoAccountTypeListDialogFragment {
                next(transactionType, it, amount)
            }.show(requireActivity().supportFragmentManager, "account_select")
        } else {
            next(transactionType, IsoAccountType.DEFAULT_UNSPECIFIED, amount)
        }


    }

    private val amountTextWatcher = object : TextWatcher {
        override fun afterTextChanged(p0: Editable) {}
        override fun beforeTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {}

        override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
            if (count <= 0) {
                amountTextView.removeTextChangedListener(this)
                amountTextView.setText("")
                amountTextView.addTextChangedListener(this)
                return
            }
            val output = s?.let {
                val content = it.toString().sanitizeAmountString().replace(".", "")
                val value = content.toLong()
                if (value == 0L) {
                    ""
                } else {
                    (value / 100.0).formatCurrency()
                }
            } ?: ""

            amountTextView.removeTextChangedListener(this)
            amountTextView.setText(output)
            amountTextView.addTextChangedListener(this)
        }
    }

}
