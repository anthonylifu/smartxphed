package com.xpresspayments.xpresssmartposphed.fragments


import android.os.Bundle
import android.view.*
import androidx.preference.Preference
import androidx.preference.PreferenceFragmentCompat
import com.xpresspayments.commons.findPreference
import com.xpresspayments.commons.printer.TextFormat
import com.xpresspayments.xpresssmartposphed.R
import com.xpresspayments.xpresssmartposphed.db
import com.xpresspayments.xpresssmartposphed.extensions.doPrint
import com.xpresspayments.xpresssmartposphed.extensions.prefs
import com.xpresspayments.xpresssmartposphed.printer
import com.xpresspayments.xpresssmartposphed.utils.prePrintFooter
import com.xpresspayments.xpresssmartposphed.utils.prePrintHeader
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch


class SettingsFragment : PreferenceFragmentCompat() {

    override fun onCreatePreferences(savedInstanceState: Bundle?, rootKey: String?) {
        setPreferencesFromResource(R.xml.app_settings, rootKey)

        findPreference<Preference>(R.string.print_configuration)?.setOnPreferenceClickListener {
            printTerminalConfiguration()
            true
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setHasOptionsMenu(true)
    }

    override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) {
        menu.removeItem(R.id.settingsFragment)
        super.onCreateOptionsMenu(menu, inflater)

    }

    private fun printTerminalConfiguration() {
        context?.let {
            GlobalScope.launch {
                val configData =   it.db.configDataDao().get()
                val printer = it.printer

                it.prePrintHeader(printer, configData, System.currentTimeMillis())
                printer.addText("+++NETWORK CONFIG+++", TextFormat())
                printer.addLine()

                val hostName = it.prefs.getString(getString(R.string.host_name_key), "")!!
                printer.addDoubleText("Host Name", hostName)

                val hostIP = it.prefs.getString(getString(R.string.host_ip_key), "")!!
                printer.addDoubleText("Host IP", hostIP)

                val hostPort = it.prefs.getString(getString(R.string.host_port_key), "")!!
                printer.addDoubleText("Host Port", hostPort)

                val portType = if(it.prefs.getBoolean(getString(R.string.host_connection_key), false)) "Open" else "SSL"
                printer.addDoubleText("Port Type", portType)

                val callHome = it.prefs.getString(getString(R.string.call_home_time_key), "")
                printer.addDoubleText("Call Home", "$callHome seconds")

                printer.feedLine(1)
                it.prePrintFooter(printer)

               it.doPrint(printer)
            }
        }

    }

}
