package com.xpresspayments.xpresssmartposphed.fragments.viewmodels

import android.app.Application
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.LiveData
import com.xpresspayments.xpresssmartposphed.repository.ReceiptDataRepository
import com.xpresspos.epmslib.entities.ConfigData
import com.xpresspos.epmslib.entities.TransactionResponse
import com.xpresspos.epmslib.utils.ReceiptType

class ReceiptDataViewModel(application: Application) : AndroidViewModel(application) {
    fun loadLastTransactionData(rrn: String, terminalId: String) {
        receiptDataRepository.getTransactionData(rrn, terminalId)
    }

    fun getTransactionResponseObservable() : LiveData<TransactionResponse?> {
        return receiptDataRepository.getTransactionResponseLiveData()
    }

    fun loadConfigData() {
        receiptDataRepository.loadConfigData();
    }

    fun getConfigDataObservable() : LiveData<ConfigData?> {
        return receiptDataRepository.getConfigDataObservable()
    }

    fun postSuccessfulPrint(receiptType: ReceiptType) {
        receiptDataRepository.postSuccessfulPrint(receiptType)
    }

    fun getSuccessfulPrintObservable() : LiveData<ReceiptType?> {
        return receiptDataRepository.getSuccessfulPrintObservable()
    }

    private val receiptDataRepository: ReceiptDataRepository = ReceiptDataRepository(application)
}