package com.xpresspayments.xpresssmartposphed.fragments.preferences


import android.os.Bundle
import android.view.*
import androidx.core.content.edit
import androidx.preference.Preference
import androidx.preference.PreferenceFragmentCompat
import com.xpresspayments.commons.findPreference
import com.xpresspayments.commons.showSnack
import com.xpresspayments.xpresssmartposphed.*
import com.xpresspayments.xpresssmartposphed.extensions.*
import com.xpresspayments.xpresssmartposphed.ui.PasswordUpdateDialog


class PasswordConfigFragment : PreferenceFragmentCompat() {

    override fun onCreatePreferences(savedInstanceState: Bundle?, rootKey: String?) {
        setPreferencesFromResource(R.xml.password_config_settings, rootKey)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setHasOptionsMenu(true)
    }

    override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) {
        menu.removeItem(R.id.settingsFragment)
        super.onCreateOptionsMenu(menu, inflater)

        findPreference<Preference>(R.string.update_admin_password_key)?.setOnPreferenceClickListener {
            updateAdminPassword(it.title?.toString() ?: "")
            true
        }

        findPreference<Preference>(R.string.update_supervisor_password_key)?.setOnPreferenceClickListener {
            updateSupervisorPassword(it.title?.toString() ?: "")
            true
        }

        findPreference<Preference>(R.string.update_operator_password_key)?.setOnPreferenceClickListener {
            updateOperatorPassword(it.title?.toString() ?: "")
            true
        }
    }

    private fun updateAdminPassword(title: String) {
        PasswordUpdateDialog(title, requireContext().adminPassword) {
            requireContext().prefs.edit {
                putString(getString(R.string.update_admin_password_key), it)
            }
            showSnack("Password updated")
        }.show(requireActivity().supportFragmentManager, "password_update")
    }

    private fun updateSupervisorPassword(title: String) {
        PasswordUpdateDialog(title, requireContext().supervisorPassword) {
            requireContext().prefs.edit {
                putString(getString(R.string.update_supervisor_password_key), it)
            }
            showSnack("Password updated")
        }.show(requireActivity().supportFragmentManager, "password_update")
    }

    private fun updateOperatorPassword(title: String) {
        PasswordUpdateDialog(title, requireContext().operatorPassword) {
            requireContext().prefs.edit {
                putString(getString(R.string.update_operator_password_key), it)
            }
            showSnack("Password updated")
        }.show(requireActivity().supportFragmentManager, "password_update")
    }
}
