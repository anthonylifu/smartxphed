package com.xpresspayments.xpresssmartposphed.fragments.preferences


import android.content.Context
import android.os.Bundle
import android.view.*
import androidx.preference.ListPreference
import androidx.preference.Preference
import androidx.preference.PreferenceFragmentCompat
import com.google.android.material.snackbar.Snackbar
import com.google.gson.Gson
import com.xpresspayment.morefun10Q.logTrace
import com.xpresspayments.commons.findPreference
import com.xpresspayments.commons.getEditTextPreferenceValue
import com.xpresspayments.xpresssmartposphed.*
import com.xpresspayments.xpresssmartposphed.extensions.buildRetrofit
import com.xpresspayments.xpresssmartposphed.extensions.hexByteArray
import com.xpresspayments.xpresssmartposphed.extensions.prefs
import com.xpresspayments.xpresssmartposphed.services.TmsConfig
import com.xpresspayments.xpresssmartposphed.services.tmsService
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch
import java.lang.RuntimeException


class XMSConfigFragment : PreferenceFragmentCompat() {

    override fun onCreatePreferences(savedInstanceState: Bundle?, rootKey: String?) {
        setPreferencesFromResource(R.xml.xms_config_settings, rootKey)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setHasOptionsMenu(true)
    }

    override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) {
        menu.removeItem(R.id.settingsFragment)
        super.onCreateOptionsMenu(menu, inflater)

        findPreference<Preference>(R.string.download_menu_key)?.setOnPreferenceClickListener {
            downloadConfig()
            true
        }
    }

    private fun downloadConfig() {
        val terminalId: String = requireContext().prefs.getString(getString(R.string.terminal_id_key), "")!!
         if (terminalId.isBlank()){
            Snackbar.make(view ?: return, "Terminal Id not set", Snackbar.LENGTH_SHORT).show()
            return
        }


        val protocol = findPreference<ListPreference>(R.string.xms_protocol_key)?.value
        val ipAddress = getEditTextPreferenceValue(R.string.xms_ip_key, R.string.default_xms_ip)
        val port = getEditTextPreferenceValue(R.string.xms_port_key, R.string.default_xms_port)
        val path =  getEditTextPreferenceValue(R.string.xms_url_key, R.string.default_xms_url)

        val  baseUrl = "$protocol://$ipAddress:$port".also { logTrace("TMS url: $it") }

        val service =   buildRetrofit(baseUrl).tmsService

        requireActivity().showProgress("Tms Configuration", "Downloading configuration...")

        GlobalScope.launch {
            kotlin.runCatching {
                val response = service.downloadConfiguration(path, terminalId, requireContext().terminalSerial)

                if (response.response.responseCode != "00") throw RuntimeException(response.response.responseMessage)

                response
            }.onSuccess {
                saveTmsConfig(it)
            }.onFailure {
               // Bugfender.d("StackTrace",it.stackTraceToString())
                it.printStackTrace()
                activity?.onErrorMessage(R.string.tms_config_download_failed, it)
            }
        }
    }

    private fun saveTmsConfig(config: TmsConfig) {
        requireContext().prefs.edit()
            .putString(getString(R.string.tms_config_key), Gson().toJson(config))
            .putString(getString(R.string.merchant_id_key), config.merchantID)
            .putString(getString(R.string.host_ip_key), config.ipAddress)
            .putString(getString(R.string.host_port_key), config.port)
            .putBoolean(getString(R.string.is_open_connection), false)
            .apply()

        if (config.logo.length > 100) {
            requireContext().openFileOutput(
                getString(R.string.logo_file_name),
                Context.MODE_PRIVATE
            ).use {
                it.write(config.logo.hexByteArray)
            }
        }

        activity?.onSuccessMessage(R.string.tms_configuration_downloaded)
    }

}
