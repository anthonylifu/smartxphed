package com.xpresspayments.xpresssmartposphed.fragments


import android.graphics.drawable.Drawable
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.content.ContextCompat
import androidx.core.os.bundleOf
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.LinearLayoutManager
import com.xpresspayments.commons.navController
import com.xpresspayments.commons.showSnack

import com.xpresspayments.xpresssmartposphed.R
import com.xpresspayments.xpresssmartposphed.extensions.*
import com.xpresspayments.xpresssmartposphed.onErrorMessage
import com.xpresspayments.xpresssmartposphed.ui.ImageTitleAdapter
import com.xpresspayments.xpresssmartposphed.ui.ImageTitleItem
import com.xpresspayments.xpresssmartposphed.ui.PasswordDialogFragment
import com.xpresspos.epmslib.entities.TransactionType
import kotlinx.android.synthetic.main.fragment_debit_card_transaction.*
import java.lang.RuntimeException

class DebitCardTransactionFragment : Fragment() {

    private val tmsConfig by lazy {
        requireContext().tmsConfig
    }

    private val preAuthKey = 7
    private val salesCompletionKey = 9

    private  val transactionMap by lazy {
        mapOf(
            Pair(0,  TransactionImageTitleItem(
                TransactionType.PURCHASE,
                ContextCompat.getDrawable(requireContext(), R.drawable.purchase_sale)!!
            )),
            Pair(3,TransactionImageTitleItem(
                TransactionType.CASH_ADVANCE,
                ContextCompat.getDrawable(requireContext(), R.drawable.cash_advance)!!
            ) ),
            Pair(1, TransactionImageTitleItem(
                TransactionType.PURCHASE_WITH_CASH_BACK,
                ContextCompat.getDrawable(requireContext(), R.drawable.cash_back)!!
            )),
            Pair(5, TransactionImageTitleItem(
                TransactionType.BALANCE,
                ContextCompat.getDrawable(requireContext(), R.drawable.balance)!!
            )),
            Pair(7, TransactionImageTitleItem(
                TransactionType.PRE_AUTHORIZATION,
                ContextCompat.getDrawable(requireContext(), R.drawable.preauthorization)!!
            )),
            Pair(9,  TransactionImageTitleItem(
                TransactionType.PRE_AUTHORIZATION_COMPLETION,
                ContextCompat.getDrawable(requireContext(), R.drawable.card_machine)!!
            )),

            Pair(6, TransactionImageTitleItem(
                TransactionType.REFUND,
                ContextCompat.getDrawable(requireContext(), R.drawable.refund)!!
            )),
            Pair(2,  TransactionImageTitleItem(
                TransactionType.REVERSAL,
                ContextCompat.getDrawable(requireContext(), R.drawable.reversal)!!
            )),
        )
    }

    private val tmsMenu by lazy {
        tmsConfig?.menu ?: listOf()
    }




    private val transactionList = ArrayList<TransactionImageTitleItem>()

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_debit_card_transaction, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        val  menus = tmsConfig?.menu ?: kotlin.run {
            requireActivity().onErrorMessage(R.string.error_title, RuntimeException("Menu unavailable. Please download menu from TMS"))
            return
        }

        if (menus.isEmpty()) {
            requireActivity().onErrorMessage(R.string.error_title, RuntimeException("No transaction menu assigned to this terminal."))
            return
        }

        if(transactionList.isNotEmpty()) {
            transactionList.clear()
        }

//        transactionList.addAll(transactionList_)
        for (menu in menus.filter {  it !in arrayOf(preAuthKey, salesCompletionKey) }) {
            if (transactionMap.containsKey(menu)) {
                transactionMap[menu]?.let {
                    transactionList.add(it)
                }
            }
        }

        transactionMap[preAuthKey]?.let { transactionList.add(it) }
        transactionMap[salesCompletionKey]?.let { transactionList.add(it) }



        list.layoutManager = LinearLayoutManager(requireContext(), LinearLayoutManager.VERTICAL, false)
        list.adapter = ImageTitleAdapter(requireActivity(), transactionList, R.layout.inner_transactions_list_item_layout) {

//            if (/*!BuildConfig.DEBUG &&*/ !printHandler.isReady) {
//                activity?.onErrorMessage(R.string.printer_error,  Exception(getString(R.string.printer_not_ready)))
//                return@ImageTitleAdapter
//            }

            val current = transactionList[it]

            when (current.transactionType) {
                TransactionType.REVERSAL, TransactionType.REFUND,
                TransactionType.PRE_AUTHORIZATION_COMPLETION -> {
                    if (current.transactionType == TransactionType.PRE_AUTHORIZATION_COMPLETION && !requireActivity().preauthEnabled){
                        showSnack(getString(R.string.function_not_enabled))
                        return@ImageTitleAdapter
                    }

                    val dialog = PasswordDialogFragment(getString(R.string.supervisor_password))
                    dialog.password.observe(viewLifecycleOwner, Observer {
                        if (it == requireContext().supervisorPassword) {
                            navController.navigate(R.id.action_debitCardTransactionFragment_to_transactionReferenceFragment, bundleOf(
                                    Pair(getString(R.string.transaction_type), current.transactionType)
                            ))
                            dialog.dismiss()
                        } else {
                            dialog.dismiss()
                            showSnack(getString(R.string.invalid_password))
                        }
                    })

                    dialog.show(requireActivity().supportFragmentManager, "fragment")
                }
                TransactionType.PRE_AUTHORIZATION -> {
                    if (!requireActivity().preauthEnabled){
                        showSnack(getString(R.string.function_not_enabled))
                        return@ImageTitleAdapter
                    }
                    val dialog = PasswordDialogFragment(getString(R.string.supervisor_password))
                    dialog.password.observe(viewLifecycleOwner, {
                        if (it == requireContext().supervisorPassword) {
                            navController.navigate(R.id.action_debitCardTransactionFragment_to_amountEntryFragment3, bundleOf(
                                Pair(getString(R.string.transaction_type), current.transactionType)
                            ))
                            dialog.dismiss()
                        } else {
                            dialog.dismiss()
                            showSnack(getString(R.string.invalid_password))
                        }
                    })


                    dialog.show(requireActivity().supportFragmentManager, "fragment")
                }
                TransactionType.BALANCE, TransactionType.LINK_ACCOUNT_INQUIRY -> {
                    /*if (requireContext().accountSelectionEnabled) {
                        IsoAccountTypeListDialogFragment{
                            navController.navigate(R.id.action_debitCardTransactionFragment_to_transactionProcessFragment, bundleOf(
                                Pair(getString(R.string.transaction_type), current.transactionType),
                                Pair(getString(R.string.account_type), it)
                            ))
                        }.show(requireActivity().supportFragmentManager, "account_select")
                    } else {
                        navController.navigate(R.id.action_debitCardTransactionFragment_to_transactionProcessFragment, bundleOf(
                            Pair(getString(R.string.transaction_type), current.transactionType)
                        ))
                    }
                    return@ImageTitleAdapter*/
                    //TO DO commented on 10-06-2021
                    showSnack(getString(R.string.function_not_enabled))
                    return@ImageTitleAdapter
                }

                else -> navController.navigate(R.id.action_debitCardTransactionFragment_to_amountEntryFragment3, bundleOf(
                        Pair(getString(R.string.transaction_type), current.transactionType)
                ))
            }

        }
    }


    class TransactionImageTitleItem(val transactionType: TransactionType,
                                          icon: Drawable) : ImageTitleItem(icon, transactionType.toString())

}
