package com.xpresspayments.xpresssmartposphed.fragments;

public class CardUtils {

    public static final String TRANSACTION_AMOUNT = "transaction_amount";
    public static final String TRANSACTION_ID = "transaction_id";
    public static final String TRANSACTION_ADDITIONAL_AMOUNT = "additional_amount";
    public static final String RECEIPT_DETAILS = "print_map";
    //public static final String PACKAGE_NAME = "com.iisysgroup.androidlite_phedc";
    //public static final String CLASS_DETAILS = "com.iisysgroup.androidlite_phedc.payments_menu.PurchaseActivity";
    public static final String PACKAGE_NAME = "com.fedco.mbc";
    public static final String CLASS_DETAILS = "com.fedco.mbc.activity.PaymentType";
    public static final int REQUESTCODE = 890;
    public static final  String TRANSACTION_STATUS_REASON  ="transactionStatusReason";
    public static final  String TRANSACTION_STATUS  ="transactionStatus";
    public static final  String MAP  ="map";
    public static final  String AMOUNT  ="amount";


    public static final String ADDITIONAL_AMOUNT ="ADDITIONAL AMOUNT";
    public static final String NAME ="NAME";
    public static final String EXPIRY_DATE ="EXPIRY DATE";
    public static final String MID ="MID";
    public static final String AID ="AID";
    public static final String PAN ="PAN";
    public static final String LABEL ="LABEL";
    public static final String STAN ="STAN";
    public static final String RRN ="RRN";
    public static final String AUTH_ID ="AUTH ID";
    public static final String ACCOUNT_TYPE ="ACCOUNT TYPE";
    public static final String TERMINAL_ID ="TERMINAL_ID";
    public static final String METRE_NO ="METRE_NO";
    public static final String TRANSACTION_RESPONSE = "TRANSACTION_RESPONSE";

    public static final int PRINTINGREQUESTCODE = 891;
    public static final int PRINTING_RESULT_MISSING_CONFIG_DATA = -1;
    public static final int PRINTING_RESULT_MISSING_TRANSACTION = -2;
}
