package com.xpresspayments.xpresssmartposphed.fragments


import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.os.bundleOf
import com.google.android.material.dialog.MaterialAlertDialogBuilder
import com.xpresspayments.commons.navController
import com.xpresspayments.commons.showSnack
import com.xpresspayments.xpresssmartposphed.*
import com.xpresspayments.xpresssmartposphed.extensions.accountSelectionEnabled

import com.xpresspayments.xpresssmartposphed.extensions.resError
import com.xpresspayments.xpresssmartposphed.ui.IsoAccountTypeListDialogFragment
import com.xpresspayments.xpresssmartposphed.ui.TransactionConfirmationViewHolder
import com.xpresspayments.xpresssmartposphed.ui.toConfirmationData
import com.xpresspos.epmslib.entities.OriginalDataElements
import com.xpresspos.epmslib.entities.TransactionResponse
import com.xpresspos.epmslib.entities.TransactionType
import com.xpresspos.epmslib.entities.toOriginalDataElements
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import kotlinx.android.synthetic.main.fragment_transaction_reference.*

class TransactionReferenceFragment : Fragment() {

    private lateinit var transactionType: TransactionType

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_transaction_reference, container, false)
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        transactionType = arguments?.get(getString(R.string.transaction_type)) as? TransactionType
                ?: kotlin.run {
                    navController.popBackStack()
                    return
                }

        (activity as ActionBarTitleUpdater).updateTitle(transactionType.toString())

        nextBtn.setOnClickListener {
            val RRN = referenceText.text.toString()
            if (RRN.length != 12) {
                showSnack(getString(R.string.invalid_rrn))
                return@setOnClickListener
            }

            activity?.showProgress(null, getString(R.string.searching))
            requireContext().db.transactionResponseDao().getOneSingle(RRN)
                    .subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe { data, error ->
                        activity?.dismissProgress()

                        error?.let {
                            activity?.onErrorMessage(R.string.error_title, RuntimeException(getString(R.string.no_transaction_found, RRN)))
                        }

                        data?.let {
                            when {
                                transactionType == TransactionType.PRE_AUTHORIZATION_COMPLETION &&
                                        it.transactionType != TransactionType.PRE_AUTHORIZATION -> {
                                    activity?.onErrorMessage(R.string.error_title, requireContext().resError(R.string.transaction_not_preauth))
                                    return@let
                                }
                                !it.transactionType.isReversible -> {
                                    activity?.onErrorMessage(R.string.error_title, requireContext().resError(R.string.transaction_not_reversible))
                                    return@let
                                }

                                else -> next(transactionType, it)
                            }
                        }
                    }
        }
    }

    private fun next(transactionType: TransactionType, transactionResponse: TransactionResponse) {
        val view = View.inflate(activity, R.layout.transaction_confirmation_layout, null)
        TransactionConfirmationViewHolder(view).apply {
            bind(transactionResponse.toConfirmationData())
        }

        val alertDialog = MaterialAlertDialogBuilder(requireActivity())
//                .setTitle("ORIGINAL DATA")
                .setView(view)
                .setPositiveButton(R.string.proceed) {_, _ ->
                    gotoNext(transactionType, transactionResponse.toOriginalDataElements())
                }.setNegativeButton(android.R.string.cancel, null)
                .create()

        alertDialog.show()
    }

    private fun gotoNext(transactionType: TransactionType,originalDataElements: OriginalDataElements) {
        when (transactionType) {
            TransactionType.REVERSAL -> {
                if (requireContext().accountSelectionEnabled) {
                    IsoAccountTypeListDialogFragment{
                        navController.navigate(R.id.action_transactionReferenceFragment_to_transactionProcessFragment, bundleOf(
                            Pair(getString(R.string.transaction_amount), originalDataElements.originalAmount),
                            Pair(getString(R.string.transaction_type), transactionType),
                            Pair(getString(R.string.original_data_elements_key), originalDataElements),
                            Pair(getString(R.string.account_type), it)
                        ))
                    }.show(requireActivity().supportFragmentManager, "account_select")
                } else {
                    navController.navigate(R.id.action_transactionReferenceFragment_to_transactionProcessFragment, bundleOf(
                        Pair(getString(R.string.transaction_amount), originalDataElements.originalAmount),
                        Pair(getString(R.string.transaction_type), transactionType),
                        Pair(getString(R.string.original_data_elements_key), originalDataElements)
                    ))
                }



            }
            TransactionType.REFUND, TransactionType.PRE_AUTHORIZATION_COMPLETION -> {
                navController.navigate(R.id.action_transactionReferenceFragment_to_replacementAmountEntryFragment, bundleOf(
                        Pair(getString(R.string.transaction_type), transactionType),
                        Pair(getString(R.string.original_data_elements_key), originalDataElements)
                ))
            }
        }

    }


}
