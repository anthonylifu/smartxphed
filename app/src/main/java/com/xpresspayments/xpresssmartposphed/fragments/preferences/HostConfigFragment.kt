package com.xpresspayments.xpresssmartposphed.fragments.preferences


import android.content.SharedPreferences
import android.os.Bundle
import android.util.Log
import android.view.Menu
import android.view.MenuInflater
import androidx.preference.*
import com.xpresspayments.commons.findPreference
import com.xpresspayments.commons.getEditTextPreferenceValue
import com.xpresspayments.verifonex990.verifone.emv.VerifoneConfigManager
import com.xpresspayments.xpresssmartposphed.*
import com.xpresspayments.xpresssmartposphed.R
import com.xpresspayments.xpresssmartposphed.extensions.prefs
import com.xpresspayments.xpresssmartposphed.extensions.startCallHomeWorker
import com.xpresspos.epmslib.entities.ConfigData
import com.xpresspos.epmslib.entities.ConnectionData
import com.xpresspos.epmslib.entities.clearSessionKey
import com.xpresspayments.commons.DeviceConfigurator
import com.xpresspayments.xpresssmartposphed.extensions.tmsConfig
import com.xpresspos.epmslib.processors.TerminalConfigurator
import com.xpresspos.epmslib.utils.TripleDES
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.Job
import kotlinx.coroutines.launch
import kotlin.coroutines.CoroutineContext


class HostConfigFragment : PreferenceFragmentCompat(),
        Preference.OnPreferenceClickListener, CoroutineScope, SharedPreferences.OnSharedPreferenceChangeListener {

    private val job = Job()

    override val coroutineContext: CoroutineContext
        get() = job + Dispatchers.IO


    private val db by lazy {
        requireContext().db
    }

    private val prefs by lazy {
        requireContext().prefs
    }

    private val deviceConfigurator: DeviceConfigurator by lazy {
        requireContext().pos.deviceConfigurator
    }

    private val isTestPlatform: Boolean
        get () = findPreference<SwitchPreferenceCompat>(R.string.host_platform_key)?.isChecked ?: false

    private val connectionData: ConnectionData
        get() {
            val ipAddress = getEditTextPreferenceValue(R.string.host_ip_key, R.string.default_host_ip)
            val port = getEditTextPreferenceValue(R.string.host_port_key, R.string.default_host_port)
            val isOpenPort = findPreference<SwitchPreferenceCompat>(R.string.host_connection_key)?.isChecked ?: false

            return ConnectionData(ipAddress = ipAddress,
                    ipPort = port.toIntOrNull() ?: 0,
                    isSSL = !isOpenPort,
                    certFileResId = if (isTestPlatform) R.raw.nibss_cert_test else R.raw.nibss_cert_live)
        }

    private val configurator
        get() = TerminalConfigurator(connectionData)

    private val terminalSerial by lazy {
        requireContext().terminalSerial
    }

    override fun onCreatePreferences(savedInstanceState: Bundle?, rootKey: String?) {
        setPreferencesFromResource(R.xml.host_config_settings, rootKey)

        findPreference<Preference>(R.string.prep_terminal_key)?.onPreferenceClickListener = this
        findPreference<Preference>(R.string.download_term_parameter_key)?.onPreferenceClickListener = this
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setHasOptionsMenu(true)

        preferenceManager.sharedPreferences.registerOnSharedPreferenceChangeListener(this)
    }

    override fun onStop() {
        super.onStop()
        preferenceManager.sharedPreferences.unregisterOnSharedPreferenceChangeListener(this)
    }

    override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) {
        menu.removeItem(R.id.settingsFragment)
        super.onCreateOptionsMenu(menu, inflater)
    }


    override fun onSharedPreferenceChanged(sharedPreferences: SharedPreferences, key: String) {
        if(key == getString(R.string.call_home_time_key)) {
            Log.d("HOSTCONFIG", "Resetting call home worker")
            (context?.applicationContext as App).startCallHomeWorker()
        }
    }


    override fun onPreferenceClick(preference: Preference): Boolean {
        when (preference.key) {
            getString(R.string.prep_terminal_key) -> {
                launch {
                    runCatching {
                        configureTerminal()
                    }.onFailure {
                        it.printStackTrace()
                        activity?.onErrorMessage(R.string.terminal_configuration_failed, it)
                    }.onSuccess {
                        activity?.onSuccessMessage(R.string.terminal_configured)
                    }
                }
                return true
            }

            getString(R.string.download_term_parameter_key) -> {
                launch {
                    runCatching {
                        downloadParameterOnly()
                    }.onFailure {
                        it.printStackTrace()
                        activity?.onErrorMessage(R.string.parameter_download_failed, it)
                    }.onSuccess {
                        activity?.onSuccessMessage(R.string.terminal_parameter_downloaded)
                    }
                }
                return true
            }

        }

        return false
    }


    private fun configureTerminal() {
        val terminalId: String = prefs.getString(getString(R.string.terminal_id_key), null)
                ?: error(getString(R.string.terminal_id_not_set))

        val tmsConfig =  requireContext().tmsConfig ?: error(getString(R.string.download_tms_config_first))

        activity?.showProgress(null, getString(R.string.prepping_terminal))

        val keyHolder = configurator.downloadNibssKeys(requireContext(), terminalId)

        val decryptedMasterKey =  TripleDES.decrypt(keyHolder.masterKey, tmsConfig.hostKeys.combinedKey) // decrypt the master key with the host key from tms
        keyHolder.masterKey =  decryptedMasterKey

        launch {
            println("saving keys")
            db.keyHolderDao().save(keyHolder)
            deviceConfigurator.processKeys(requireContext(), keyHolder)
        }

        activity?.showProgress(null, getString(R.string.downloading_parameter))
        val configData = configurator.downloadTerminalParameters(requireContext(), terminalId, keyHolder.clearSessionKey, terminalSerial)
//        println(configData)
        launch {
            setConfigPrefs(configData)
            deviceConfigurator.processConfigData(requireContext(), terminalId, configData)
            db.configDataDao().save(configData)
            prefs.edit().putBoolean(getString(R.string.terminal_prepped_key), true).apply()
        }


        if (deviceConfigurator is VerifoneConfigManager) {
                VerifoneConfigManager.loadDefaultRIDs(requireContext(), true)
                VerifoneConfigManager.loadDefaultAIDs(requireContext(), true)
        } else {
            activity?.showProgress(null, getString(R.string.downloading_capks))
            val capkList = configurator.downloadNibssCAPK(requireContext(), terminalId, keyHolder.clearSessionKey, terminalSerial)
            launch {
                deviceConfigurator.processCAs(requireContext(), capkList)
            }

            activity?.showProgress(null, getString(R.string.downloading_aids))
            val aidList = configurator.downloadNibssAID(requireContext(), terminalId, keyHolder.clearSessionKey, terminalSerial)
            launch {
                deviceConfigurator.processAIDs(requireContext(), aidList)
            }
        }

    }

    private fun downloadParameterOnly() {
        activity?.showProgress(null, getString(R.string.downloading_parameter))

        val terminalId: String = PreferenceManager.getDefaultSharedPreferences(context).getString(getString(R.string.terminal_id_key), null)
                ?: error(getString(R.string.terminal_id_not_set))

        val keyHolder = db.keyHolderDao().get() ?: error(getString(R.string.terminal_not_prepped))

        val configData = configurator.downloadTerminalParameters(requireContext(), terminalId, keyHolder.clearSessionKey, terminalSerial)
//        println(configData)
        launch {
            setConfigPrefs(configData)
            deviceConfigurator.processConfigData(requireContext(), terminalId, configData)
            db.configDataDao().save(configData)
            prefs.edit().putBoolean(getString(R.string.terminal_prepped_key), true).apply()
        }

    }

    private fun setConfigPrefs(configData: ConfigData) {
        prefs.edit()
                .putString(getString(R.string.merchant_id_key), configData.cardAcceptorIdCode)
                .putString(getString(R.string.call_home_time_key), ((configData.callHomeTime.toIntOrNull() ?: 0) * 60 * 60).toString())
                .apply()
    }
    

}
