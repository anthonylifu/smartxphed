package com.xpresspayments.xpresssmartposphed.fragments.viewmodels

import android.app.Application
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.paging.LivePagedListBuilder
import androidx.paging.PagedList
import com.xpresspayments.xpresssmartposphed.App
import com.xpresspayments.xpresssmartposphed.R
import com.xpresspayments.xpresssmartposphed.extensions.resError
import com.xpresspos.epmslib.entities.TransactionResponse

const val DATABASE_QUERY_SIZE = 20
class HistoryFragmentViewModel(app: Application): AndroidViewModel(app) {

    val db by lazy {
        getApplication<App>().db
    }

    val all by lazy {
        val dataSourceFactory = db.transactionResponseDao().getAll()
        LivePagedListBuilder(dataSourceFactory, DATABASE_QUERY_SIZE)
                .setBoundaryCallback(boundaryCallback)
                .build()
    }


    private val _onError = MutableLiveData<Throwable>()
    val onError: LiveData<Throwable>
        get() = _onError


    fun findTransaction(searchText: String): LiveData<PagedList<TransactionResponse>> {
        val dataSourceFactory =  db.transactionResponseDao().findTransaction("%$searchText%")

        return LivePagedListBuilder(dataSourceFactory, DATABASE_QUERY_SIZE)
            .build()
    }




    private val boundaryCallback =  object :PagedList.BoundaryCallback<TransactionResponse>() {
        override fun onZeroItemsLoaded() {
            _onError.postValue(app.resError(R.string.empty_transaction_list))
        }
    }
}