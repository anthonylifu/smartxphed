package com.xpresspayments.xpresssmartposphed.fragments


import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.google.android.material.dialog.MaterialAlertDialogBuilder
import com.xpresspayments.commons.navController
import com.xpresspayments.commons.runOnMain
import com.xpresspayments.xpresssmartposphed.BuildConfig

import com.xpresspayments.xpresssmartposphed.R
import com.xpresspayments.xpresssmartposphed.db
import com.xpresspayments.xpresssmartposphed.extensions.*
import com.xpresspos.epmslib.entities.*
import com.xpresspos.epmslib.utils.ReceiptType
import kotlinx.android.synthetic.main.fragment_receipt_view.*
import kotlinx.coroutines.*
import java.text.SimpleDateFormat
import java.util.*

class ReceiptViewFragment : Fragment() {

    lateinit var response: TransactionResponse

    private val terminalId by lazy {
        requireContext().prefs.getString(getString(R.string.terminal_id_key), null)
    }
    private val configData by lazy {
        GlobalScope.async {
            requireContext().db.configDataDao().get()!!
        }
    }

    private val tmsConfig by lazy {
        requireContext().tmsConfig
    }


    private val appVersion by lazy {
        "${getString(R.string.app_name)} ${BuildConfig.VERSION_NAME}"
    }


    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_receipt_view, container, false)
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)

        response = arguments?.getParcelable<TransactionResponse>(getString(R.string.response_key))
                ?: kotlin.run {
                    navController.popBackStack()
                    return
                }

        processReceipt(response)


        printButton.setOnClickListener {
            activity?.validateSupervisorAccess {
                doReceiptPrinting()
            }
        }

    }

    private fun doReceiptPrinting(isReprint: Boolean = true)= runBlocking {
        val configData = this@ReceiptViewFragment.configData.await()

        GlobalScope.launch(Dispatchers.IO) {
            requireContext().printTransactionResult(configData, response, ReceiptType.CUSTOMER_COPY, isReprint)
            if (response.transactionType !in arrayOf(TransactionType.BALANCE, TransactionType.LINK_ACCOUNT_INQUIRY)) {
                runOnMain {
                    runCatching {
                        MaterialAlertDialogBuilder(requireActivity())
                            .setTitle(R.string.receipt_printing)
                            .setMessage(R.string.print_merchant_copy)
                            .setPositiveButton(android.R.string.yes) {_,_ ->
                                GlobalScope.launch {
                                    requireContext().printTransactionResult(configData, response, ReceiptType.MERCHANT_COPY, isReprint)
                                }
                            }
                            .setNegativeButton(android.R.string.cancel, null)
                            .show()
                    }
                }
            }

        }
    }

    private fun processReceipt(response: TransactionResponse) = runBlocking {
        val configData = this@ReceiptViewFragment.configData.await()

        val autoPrint = requireArguments().getBoolean(getString(R.string.auto_print_key))
        val isReprint = requireArguments().getBoolean(getString(R.string.is_reprint_key))
        if (autoPrint) {
            doReceiptPrinting(isReprint)
        }

        printVirtualReceipt(configData, response)
    }


    private fun printVirtualReceipt(configData: ConfigData, response: TransactionResponse) {
        logoImage.setImageBitmap(requireContext().logo)
        receiptHeaderText.text = tmsConfig?.receiptHeader ?: configData.merchantNameLocation
        merchanIdText.text = configData.cardAcceptorIdCode
        merchantAddressText.text = tmsConfig?.location ?: ""

        with(response) {
            val date = Date(transactionTimeInMillis)
            dateText.text = SimpleDateFormat("dd/MM/yy").format(date)
            timeText.text = SimpleDateFormat("HH:mm:ss").format(date)

            terminalIdText.text = terminalId

            transactionTypeText.text = transactionType.toString().toUpperCase()

            panText.text = maskedPan
            expiryText.text = cardExpiry
            cardHolderNameText.text = cardHolder
            stanText.text = STAN
            rrnText.text = RRN
            authIdText.text = authCode
            accountTypeText.text = accountType.toString()
            amountText.text = amount.formatSingleExponentCurrency()
            otherAmountText.text = otherAmount.formatSingleExponentCurrency()

            totalAmountText.text = (amount + otherAmount).formatSingleExponentCurrency()
            responseStatusText.text = if (response.isApproved) getString(R.string.approved) else getString(R.string.declined)
            responseReasonText.text = responseMessage
            receiptFooterText.text = tmsConfig?.receiptFooter ?: ""

            aidText.text = AID
            cardText.text = cardLabel
            appCryptogramText.text = appCryptogram
            tvrText.text = TVR
            tsiText.text = TSI

            appVersionText.text = appVersion
        }

    }




}
