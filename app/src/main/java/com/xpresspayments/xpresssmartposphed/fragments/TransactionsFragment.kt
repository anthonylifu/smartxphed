package com.xpresspayments.xpresssmartposphed.fragments


import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.xpresspayments.commons.navController
import com.xpresspayments.commons.showSnack
import com.xpresspayments.xpresssmartposphed.R
import com.xpresspayments.xpresssmartposphed.extensions.isPrepped
import com.xpresspayments.xpresssmartposphed.extensions.resError
import com.xpresspayments.xpresssmartposphed.onErrorMessage
import com.xpresspayments.xpresssmartposphed.ui.ImageTitleAdapter
import com.xpresspayments.xpresssmartposphed.ui.ImageTitleItem


class TransactionsFragment : Fragment() {

    private val transactionList by lazy {
        arrayListOf(
                ImageTitleItem(
                        ContextCompat.getDrawable(requireContext(), R.drawable.credit_card_payment)!!,
                        getString(R.string.debit_card_payments)
                ),
                ImageTitleItem(
                        ContextCompat.getDrawable(requireContext(), R.drawable.payment)!!,
                        getString(R.string.card_not_present)
                ),
                ImageTitleItem(
                        ContextCompat.getDrawable(requireContext(), R.drawable.payxpress)!!,
                        getString(R.string.payxpress)
                ),
                ImageTitleItem(
                        ContextCompat.getDrawable(requireContext(), R.drawable.ic_wallet)!!,
                        getString(R.string.wallet)
                )
//            ,
//            ImageTitleItem(
//                ContextCompat.getDrawable(requireContext(), R.drawable.fingerprint_scan_2)!!,
//                getString(R.string.title_activity_fingerprint)
//            )
        )
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_transactions, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        val list =  view.findViewById<RecyclerView>(android.R.id.list)
        list.layoutManager = GridLayoutManager(requireContext(), 2, GridLayoutManager.VERTICAL, false)
        list.adapter = ImageTitleAdapter(requireActivity(), transactionList) {
            //            showSnack("Click index $it")
            when (it) {
                0 -> {
                    if (requireContext().isPrepped) {
                        navController.navigate(R.id.action_transactionsFragment_to_debitCardTransactionFragment)
                    } else {
                        activity?.onErrorMessage(R.string.app_name, requireContext().resError(R.string.terminal_not_prepped))
                    }
                }
//                4 -> startActivity(Intent(activity, FingerprintActivity::class.java))
                else -> showSnack("Option unavailable")
            }
        }
    }



}


