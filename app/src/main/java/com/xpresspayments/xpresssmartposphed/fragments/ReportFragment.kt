package com.xpresspayments.xpresssmartposphed.fragments


import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.appcompat.app.AlertDialog
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.LinearLayoutManager
import com.xpresspayments.commons.navController
import com.xpresspayments.commons.printer.Align
import com.xpresspayments.commons.printer.Printer
import com.xpresspayments.commons.printer.TextFormat
import com.xpresspayments.commons.runOnBackground
import com.xpresspayments.commons.runOnMain
import com.xpresspayments.commons.showSnack
import com.xpresspayments.xpresssmartposphed.*
import com.xpresspayments.xpresssmartposphed.extensions.*
import com.xpresspayments.xpresssmartposphed.ui.DateRangeDialog
import com.xpresspayments.xpresssmartposphed.ui.ImageTitleAdapter
import com.xpresspayments.xpresssmartposphed.ui.ImageTitleItem
import com.xpresspayments.xpresssmartposphed.utils.prePrintFooter
import com.xpresspayments.xpresssmartposphed.utils.prePrintHeader
import com.xpresspos.epmslib.entities.TransactionResponse
import com.xpresspos.epmslib.entities.TransactionType
import com.xpresspos.epmslib.entities.clearSessionKey
import com.xpresspos.epmslib.entities.isApproved
import com.xpresspos.epmslib.processors.TerminalConfigurator
import kotlinx.android.synthetic.main.fragment_report.*
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch
import java.text.SimpleDateFormat
import java.util.*


class ReportFragment : Fragment() {

    private val transactionList by lazy {
        arrayListOf(
            ImageTitleItem(
                ContextCompat.getDrawable(requireContext(), R.drawable.ic_payment_terminal)!!,
                getString(R.string.title_card_transaction_report)
            ),
            ImageTitleItem(
                ContextCompat.getDrawable(requireContext(), R.drawable.ic_safe_box)!!,
                getString(R.string.end_of_day)
            ),
            ImageTitleItem(
                ContextCompat.getDrawable(requireContext(), R.drawable.ic_calendar)!!,
                getString(R.string.eod_date_range)
            ),
            ImageTitleItem(
                ContextCompat.getDrawable(requireContext(), R.drawable.ic_closed)!!,
                getString(R.string.close_batch)
            )
        )
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_report, container, false)
    }


    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        list.layoutManager =
            LinearLayoutManager(requireContext(), LinearLayoutManager.VERTICAL, false)
        list.adapter = ImageTitleAdapter(
            requireActivity(),
            transactionList,
            R.layout.inner_transactions_list_item_layout
        ) {

            when (it) {
                0 -> navController.navigate(R.id.action_reportFragment_to_historyFragment)
                1 -> {
                    if (!requireActivity().isPrepped) {
                        requireActivity().onErrorMessage(
                            R.string.error_title,
                            Exception(getString(R.string.terminal_not_prepped))
                        )
                        return@ImageTitleAdapter
                    }

                    activity?.validateSupervisorAccess {
                        progressView.visibility = View.VISIBLE
                        GlobalScope.launch {
                            eodAll()
                        }
                    }
                }
                2 -> {
                    if (!requireActivity().isPrepped) {
                        requireActivity().onErrorMessage(
                            R.string.error_title,
                            Exception(getString(R.string.terminal_not_prepped))
                        )
                        return@ImageTitleAdapter
                    }

                    activity?.validateSupervisorAccess {
                        DateRangeDialog {
                            if (it.isNotEmpty()) {
                                eodRange(it.first(), it.last())
                            }
                        }.show(requireActivity().supportFragmentManager, "DateDialog")
                    }
                }
                3 -> {
                    if (!requireActivity().isPrepped) {
                        requireActivity().onErrorMessage(
                            R.string.error_title,
                            Exception(getString(R.string.terminal_not_prepped))
                        )
                        return@ImageTitleAdapter
                    }

                    activity?.validateSupervisorAccess {

                        AlertDialog.Builder(requireActivity())
                            .setTitle(R.string.close_batch)
                            .setMessage(R.string.close_batch_warning)
                            .setNegativeButton(android.R.string.no, null)
                            .setPositiveButton(android.R.string.yes) { _, _ ->
                                progressView.visibility = View.VISIBLE
                                GlobalScope.launch {
                                    closeBatch()
                                }
                            }.show()
                    }
                }
            }

        }
    }


    private fun eodAll() {
        val transactions = requireActivity().db.transactionResponseDao().getList()
        doEOD(transactions, false)
    }

    private fun closeBatch() {
        val transactions = requireActivity().db.transactionResponseDao().getList()
        doEOD(transactions, true)
    }


    private fun eodRange(start: Date, end: Date) {
        progressView.visibility = View.VISIBLE

        GlobalScope.launch {
            val transactions = requireActivity().db.transactionResponseDao().getList().filter {
                val date = Date(it.transactionTimeInMillis)
                date >= start.also { println("Start: $it") } && date <= Calendar.getInstance()
                    .apply {
                        time = end
                        set(Calendar.HOUR_OF_DAY, 23)
                        set(Calendar.MINUTE, 59)
                        set(Calendar.SECOND, 59)
                    }.time.also {
                    println("End: $it")
                }
            }

            if (transactions.isNotEmpty()) {
                doEOD(transactions, false)
            } else {
                runOnMain {
                    showSnack("No transaction found for selected date(s)")
                    progressView.visibility = View.INVISIBLE
                }
            }
        }
    }


    private fun doEOD(transactions: List<TransactionResponse>, shouldDelete: Boolean) {
        with(requireActivity()) {
            val terminalID = prefs.getString(getString(R.string.terminal_id_key), "")
            val sessionKey = db.keyHolderDao().get()!!.clearSessionKey
            val configData = db.configDataDao().get()!!
            kotlin.runCatching {
                val response = TerminalConfigurator(connectionData)
                    .nibssEOD(this, terminalID!!, sessionKey, terminalSerial)
                println("EOD response: $response")
            }.exceptionOrNull()?.printStackTrace()

            println("got here")
            var printer = this.printer
            prePrintHeader(printer, configData, System.currentTimeMillis())

            printer.addText("+++END OF DAY+++")
            printer.addLine()


            var totalApproved = 0L
            var totalDeclined = 0L

            printer.addText(
                "S/N  TYPE    RRN   DATE  AMOUNT STATUS",
                format = TextFormat(align = Align.LEFT)
            )
            transactions.forEachIndexed { i, it ->
                with(printer) {
                    val dateString =
                        SimpleDateFormat("dd/MM/yy").format(Date(it.transactionTimeInMillis))
                    val amount = it.amount.formatSingleExponentCurrency()

                    addText(
                        "${i + 1}. ${it.transactionType.name}  ${it.RRN}   $dateString   $amount   ${if (it.isApproved) "A" else "D"}",
                        TextFormat(align = Align.LEFT)
                    )

//                    addDoubleText("TYPE", it.transactionType.toString())
//                    addDoubleText("STATUS", if (it.isApproved) "APPROVED" else "DECLINED")
//                    addDoubleText("PAN", it.maskedPan)
//                    addDoubleText("EXPIRY", it.cardExpiry)
//                    addDoubleText("RRN", it.RRN)
//                    addDoubleText("AMOUNT", it.amount.formatSingleExponentCurrency())
//                    if (it.transactionType.hasAdditionalAmount) {
//                        addDoubleText("OTHER AMOUNT", it.otherAmount.formatSingleExponentCurrency())
//                    }
//                    addDoubleText("DATE", SimpleDateFormat("dd/MM/yy").format(Date(it.transactionTimeInMillis)))
//                    addLine()

                    val temp = it.amount + it.otherAmount
                    if (it.isApproved) {

                        when (it.transactionType) {
                            TransactionType.REFUND -> {
                                totalApproved -= temp
                                totalDeclined += temp
                            }
                            TransactionType.REVERSAL -> {
                                totalDeclined += temp
                            }
                            else -> totalApproved += temp
                        }
                    } else {
                        if (it.transactionType !in arrayOf(
                                TransactionType.REFUND,
                                TransactionType.REVERSAL
                            )
                        ) {
                            totalDeclined += temp
                        }
                    }

                    doPrint(printer) {
                        if (it != Printer.Status.OK) {
                            stopProcess()
                            return@doPrint
                        }
                    }
                }
                printer = requireContext().printer
            }

            printer.addDoubleText("COUNT", transactions.size.toString())
            printer.addText("--------------SUMMARY--------------")
            printer.addDoubleText("APPROVED", totalApproved.formatSingleExponentCurrency())
            printer.addDoubleText("DECLINED", totalDeclined.formatSingleExponentCurrency())

            prePrintFooter(printer)

            doPrint(printer)

            if (shouldDelete) {
                runOnBackground {
                    db.transactionResponseDao().delete(transactions)
                }
            }
        }

        stopProcess()

    }

    private fun stopProcess() {
        runOnMain {
            progressView.visibility = View.INVISIBLE
        }
    }
}
