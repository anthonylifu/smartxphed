package com.xpresspayments.xpresssmartposphed.fragments


import android.app.SearchManager
import android.content.Context
import android.os.Bundle
import android.view.*
import androidx.appcompat.widget.SearchView
import androidx.fragment.app.Fragment
import androidx.core.os.bundleOf
import androidx.fragment.app.viewModels
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.LinearLayoutManager
import com.xpresspayments.commons.logTrace
import com.xpresspayments.commons.navController
import com.xpresspayments.commons.runOnMain
import com.xpresspayments.commons.showSnack
import com.xpresspayments.xpresssmartposphed.R
import com.xpresspayments.xpresssmartposphed.db
import com.xpresspayments.xpresssmartposphed.extensions.printTransactionResult
import com.xpresspayments.xpresssmartposphed.extensions.validateSupervisorAccess
import com.xpresspayments.xpresssmartposphed.fragments.viewmodels.HistoryFragmentViewModel
import com.xpresspayments.xpresssmartposphed.ui.CardHistoryAdapter
import com.xpresspayments.xpresssmartposphed.ui.DateRangeDialog
import com.xpresspos.epmslib.entities.TransactionResponse
import com.xpresspos.epmslib.utils.ReceiptType
import kotlinx.android.synthetic.main.fragment_card_report.*
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.async
import kotlinx.coroutines.launch
import kotlinx.coroutines.runBlocking
import java.util.*


class CardReportFragment : Fragment() {
    private lateinit var cardHistoryAdapter: CardHistoryAdapter
    private val viewModel: HistoryFragmentViewModel by viewModels()

    private val configData by lazy {
        GlobalScope.async {
            requireContext().db.configDataDao().get()!!
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setHasOptionsMenu(true)
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_card_report, container, false)
    }

    override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) {
        menu.removeItem(R.id.settingsFragment)
        inflater.inflate(R.menu.history_menu, menu)


        // Associate searchable configuration with the SearchView
        val searchManager = requireContext().getSystemService(Context.SEARCH_SERVICE) as SearchManager

        (menu.findItem(R.id.search).actionView as SearchView).apply {
            setSearchableInfo(searchManager.getSearchableInfo(requireActivity().componentName))
            setOnQueryTextListener(onQueryTextListener)
        }

        (menu.findItem(R.id.action_reprint_all)).setOnMenuItemClickListener {
            activity?.validateSupervisorAccess {
                reprintAll()
            }
            true
        }


        (menu.findItem(R.id.action_reprint_date)).setOnMenuItemClickListener {
            activity?.validateSupervisorAccess {
                DateRangeDialog {
                    if (it.isNotEmpty()) {
                        reprintRange(it.first(), it.last())
                    }
                }.show(requireActivity().supportFragmentManager, "DateDialog")
            }
            true
        }


        super.onCreateOptionsMenu(menu, inflater)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        cardHistoryAdapter = CardHistoryAdapter(requireContext()) {
            displayResponse(it)
        }

        recyclerView.layoutManager = LinearLayoutManager(requireContext())
        recyclerView.adapter = cardHistoryAdapter

        viewModel.all.observe(viewLifecycleOwner, Observer {
            emptyList.visibility = View.GONE
            cardHistoryAdapter.submitList(it)
        })

        viewModel.onError.observe(viewLifecycleOwner, Observer {
            emptyList.visibility = View.VISIBLE
        })
    }


   private fun displayResponse(response: TransactionResponse) {
       logTrace("Display response")
        navController.navigate(R.id.action_historyFragment_to_receiptViewFragment, bundleOf(
                Pair(getString(R.string.response_key), response),
                Pair(getString(R.string.auto_print_key), false)
        ))
    }


    private fun reprintRange(start: Date, end: Date) {
        progressView.visibility = View.VISIBLE

        GlobalScope.launch {
            val transactions = requireActivity().db.transactionResponseDao().getList().filter {
                val date = Date(it.transactionTimeInMillis)
                date >= start.also { println("Start: $it") }  && date <= Calendar.getInstance().apply{
                    time = end
                    set(Calendar.HOUR_OF_DAY, 23)
                    set(Calendar.MINUTE, 59)
                    set(Calendar.SECOND, 59)
                }.time.also {
                    println("End: $it")
                }
            }

            if (transactions.isNotEmpty()) {
                reprint(transactions)
            } else {
                runOnMain {
                    showSnack("No transaction found for selected date(s)")
                    progressView.visibility = View.INVISIBLE
                }
            }
        }
    }

    private fun reprintAll() {
        progressView.visibility = View.VISIBLE
        GlobalScope.launch {
            val transactions = requireActivity().db.transactionResponseDao().getList()

            if (transactions.isNotEmpty()) {
                reprint(transactions)
            } else {
                runOnMain {
                    showSnack("No transaction found")
                    progressView.visibility = View.INVISIBLE
                }
            }
        }
    }


    private fun reprint(transactions: List<TransactionResponse>) = runBlocking {
        val configData =  configData.await()
        transactions.forEach {
            requireContext().printTransactionResult(configData, it, ReceiptType.MERCHANT_COPY, true)
        }

        runOnMain {
            progressView.visibility = View.INVISIBLE
        }
    }


    private fun findTransaction(query: String?) {
        logTrace("findTransaction::$query")

        if (view != null) {//added this since navigation seems to cause a crash
            query?.let(viewModel::findTransaction)?.observe(viewLifecycleOwner) {
                //logTrace(it.toString())
                if (it.isNotEmpty()) {
                    cardHistoryAdapter.submitList(it)
                } else {

                }
            }?: cardHistoryAdapter.submitList(viewModel.all.value)
        }

    }

    private val onQueryTextListener = object : SearchView.OnQueryTextListener {
        override fun onQueryTextSubmit(query: String?): Boolean {
            findTransaction(query)
            return true
        }

        override fun onQueryTextChange(newText: String?): Boolean {
            findTransaction(newText)
            return true
        }
    }


}
