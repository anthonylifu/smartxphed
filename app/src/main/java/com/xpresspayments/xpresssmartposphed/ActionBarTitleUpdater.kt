package com.xpresspayments.xpresssmartposphed

interface ActionBarTitleUpdater {
    fun updateTitle(title: String)
}