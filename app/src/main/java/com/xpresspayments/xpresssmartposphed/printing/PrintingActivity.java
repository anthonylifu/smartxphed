package com.xpresspayments.xpresssmartposphed.printing;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.lifecycle.ViewModelProviders;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;

import com.xpresspayments.xpresssmartposphed.extensions.XpressExtensionsKt;

import com.xpresspayments.xpresssmartposphed.R;
import com.xpresspayments.xpresssmartposphed.fragments.CardUtils;
import com.xpresspayments.xpresssmartposphed.fragments.viewmodels.ReceiptDataViewModel;
import com.xpresspayments.xpresssmartposphed.utils.LogUtils;
import com.xpresspos.epmslib.entities.ConfigData;
import com.xpresspos.epmslib.entities.TransactionResponse;
import com.xpresspos.epmslib.utils.ReceiptType;

import java.util.HashMap;

public class PrintingActivity extends AppCompatActivity {
    private String rrn, terminalId;
    private HashMap<String, String> receiptData;
    private ReceiptDataViewModel receiptDataViewModel;
    private TransactionResponse transactionResponse;
    private static final String TAG = PrintingActivity.class.getSimpleName();
    //added to print merchant receipt - 12-06-2021
    private static ReceiptType receiptType;
    private ConfigData configData;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_printing);
        LogUtils.getInstance(this).startLoggingToFile();
        receiptDataViewModel = ViewModelProviders.of(this).get(ReceiptDataViewModel.class);
        Bundle printBundle = getIntent().getExtras();
        rrn = printBundle.getString(CardUtils.RRN);
        terminalId = printBundle.getString(CardUtils.TERMINAL_ID);
        receiptData = (HashMap<String, String>) printBundle.getSerializable(CardUtils.RECEIPT_DETAILS);
        observeViewModels();
    }

    private void observeViewModels() {
        receiptDataViewModel.getTransactionResponseObservable().observe(this, transactionResponse -> {
            if (null != transactionResponse) {
                this.transactionResponse = transactionResponse;
                receiptDataViewModel.loadConfigData();
            } else {
                //TODO transaction is not found in storage show user error and return to caller activity
                returnResultToCallerActivity(CardUtils.PRINTING_RESULT_MISSING_TRANSACTION);
            }
        });

        receiptDataViewModel.getConfigDataObservable().observe(this, configData -> {
            if (null != configData) {
                this.configData = configData;
                //TODO Now we have the original transaction and config data proceed to print
                XpressExtensionsKt.printCustomTransactionResult(this, configData, transactionResponse, receiptData, receiptType, receiptDataViewModel, false);
            } else {
                //TODO Config Data is not found in storage, show user error and return to caller activity
                returnResultToCallerActivity(CardUtils.PRINTING_RESULT_MISSING_CONFIG_DATA);
            }
        });

        receiptDataViewModel.getSuccessfulPrintObservable().observe(this, printedReceiptType -> {
            if (ReceiptType.CUSTOMER_COPY == printedReceiptType) {
                receiptType = ReceiptType.MERCHANT_COPY;
                new AlertDialog.Builder(this)
                        .setTitle("Success")
                        .setMessage("Press continue to print merchant copy and cancel to dismiss")
                        .setPositiveButton("Continue", ((dialog, which) -> {
                            dialog.dismiss();
                            XpressExtensionsKt.printCustomTransactionResult(this, configData, transactionResponse, receiptData, receiptType, receiptDataViewModel, false);
                        }))
                        .setNegativeButton(android.R.string.cancel, ((dialog, which) -> {
                            dialog.dismiss();
                            returnResultToCallerActivity(Activity.RESULT_CANCELED);
                        }))
                        .show();
            } else {
                returnResultToCallerActivity(Activity.RESULT_OK);
            }
        });
    }

    private void returnResultToCallerActivity(int resultCode) {
        setResult(resultCode);
        finish();
    }

    /*@Override
    public void onBackPressed() {
        Intent setIntent = new Intent();
        setIntent.setComponent(new ComponentName(CardUtils.PACKAGE_NAME, CardUtils.CLASS_DETAILS));
        if (getPackageManager ( ).resolveActivity ( setIntent, 0 ) != null) {
            startActivity(setIntent);
            finish();
        } else {
            Toast.makeText ( this, "You Currently don't have an App to back this action", Toast.LENGTH_SHORT ).show ( );
        }
    }*/

    public void printReceipt(View view) {
        try {
            receiptType = ReceiptType.CUSTOMER_COPY;
            receiptDataViewModel.loadLastTransactionData(rrn, terminalId);
        } catch (Exception e) {
            Log.e(TAG, e.getMessage());
        }

    }


    public void printReceipt2(View view) {
        try {
            receiptType = ReceiptType.MERCHANT_COPY;
            receiptDataViewModel.loadLastTransactionData(rrn, terminalId);
        } catch (Exception e) {
            Log.e(TAG, e.getMessage());
        }
    }
}