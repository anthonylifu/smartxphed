package com.xpresspayments.xpresssmartposphed

import android.app.Activity
import android.app.Application
import android.app.ProgressDialog
import android.content.Context
import androidx.room.Room
import com.xpresspayment.morefun10Q.HorizonApp
import com.xpresspayment.morefun10Q.MorefunApp
import com.xpresspayments.commons.SmartAppInterface
import com.xpresspayments.commons.printer.Printer
import com.xpresspayments.newland910.NewlandApp
import com.xpresspayments.verifonex990.VerifoneApp
import com.xpresspayments.xpresssmartposphed.extensions.isPrepped
import com.xpresspayments.xpresssmartposphed.extensions.startCallHomeWorker
import com.xpresspayments.xpresssmartposphed.repository.AppDatabase
import java.util.*

class App(val smartDevice: SmartAppInterface = when(BuildConfig.device_type){
    1 -> VerifoneApp()
    2 -> MorefunApp()
    3 -> HorizonApp()
    else -> NewlandApp()
}) : Application(),
    SmartAppInterface by smartDevice {

    private var progressDialog: ProgressDialog? = null
    var timer: Timer? = null

    val db by lazy {
        Room.databaseBuilder(this, AppDatabase::class.java, "smartx.db")
            //.fallbackToDestructiveMigration()
            .build()
    }

    override fun onCreate() {
        super.onCreate()

//        Bugfender.init(this, "0fOnp0jdsI41RbRzPlfJQ2acrqnulaeB", true)
//        Bugfender.enableCrashReporting()
//        Bugfender.enableLogcatLogging()

        initializeDevice(this)

        if (isPrepped) {
            startCallHomeWorker()
        }
    }

    fun showProgress(context: Activity, title: String?, message: String) {
        com.xpresspayments.commons.runOnMain {
            progressDialog?.dismiss()
            progressDialog = ProgressDialog.show(context, title, message)
        }
    }


    fun dismissProgress() {
        com.xpresspayments.commons.runOnMain {
            progressDialog?.dismiss()
        }
    }

    override fun onTerminate() {
        super.onTerminate()
        destruct(this)

        timer?.cancel()

    }


}


val Context.db: AppDatabase
    get() = (this.applicationContext as App).db


val Context.pos: SmartAppInterface
    get() = (this.applicationContext as App).smartDevice



val Context.terminalSerial: String
    get() = (this.applicationContext as App).smartDevice.terminalSerial


val Context.printer: Printer
    get() =  (this.applicationContext as App).smartDevice.getPrinter(this)


