package com.xpresspayments.xpresssmartposphed.utils

import android.content.Context
import com.xpresspayments.commons.printer.*
import com.xpresspayments.xpresssmartposphed.BuildConfig
import com.xpresspayments.xpresssmartposphed.R
import com.xpresspayments.xpresssmartposphed.extensions.prefs
import com.xpresspayments.xpresssmartposphed.extensions.printerLogo
import com.xpresspayments.xpresssmartposphed.extensions.tmsConfig
import com.xpresspos.epmslib.entities.ConfigData
import java.util.*


fun Context.prePrintHeader(printer: Printer, configData: ConfigData?, transactionTimeInMillis: Long) {
    //header section

    val terminalId = prefs.getString(getString(R.string.terminal_id_key), "")!!
    val merchantId = prefs.getString(getString(R.string.merchant_id_key), "")!!

    val bitmap = printerLogo
    printer.addImage(bitmap, bitmap.width/2, bitmap.height/2, 90)

    printer.addText(tmsConfig?.receiptHeader ?: configData?.merchantNameLocation ?: "", TextFormat(
        align = Align.CENTER,
        fontSize = FontSize.NORMAL,
        fontStyle = FontStyle.BOLD
    ))

    printer.addText(tmsConfig?.location ?: "", TextFormat(
        align = Align.CENTER,
        fontSize = FontSize.SMALL
    ))

   // printer.feedLine(1)

    val date = java.util.Calendar.getInstance().apply {
        timeInMillis = transactionTimeInMillis
    }.time

    printer.addDoubleText(
        java.text.SimpleDateFormat("dd/MM/yy", Locale.ROOT).format(date),
        java.text.SimpleDateFormat("HH:mm:ss", Locale.ROOT).format(date))


    printer.addDoubleText(
        getString(R.string.terminal_id).toUpperCase(Locale.ROOT),
        terminalId
    )

    printer.addDoubleText(
        getString(R.string.merchant_id).toUpperCase(Locale.ROOT),
        merchantId
    )

    //printer.feedLine(1)
    printer.addLine()
}

fun Context.prePrintFooter(printer: Printer) {
    val receiptFooter = tmsConfig?.receiptFooter ?: "Please call again"
    val appVersion = "${getString(R.string.app_name)} ${BuildConfig.VERSION_NAME}"

    //printer.feedLine(1)
    printer.addText(receiptFooter)
    printer.addLine()
    printer.addText(appVersion, TextFormat(fontSize = FontSize.SMALL))

   // printer.feedLine(1)
    printer.addText(getString(R.string.xpress_url), TextFormat(fontSize = FontSize.SMALL))
    printer.addText(getString(R.string.xpress_phone), TextFormat(fontSize = FontSize.SMALL))

    //printer.feedLine(2)
    printer.addLine()
    printer.feedLine(4)
}