package com.xpresspayments.xpresssmartposphed.utils;

import android.content.Context;
import android.os.Environment;

import java.io.File;
import java.io.IOException;
import java.text.NumberFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

public class LogUtils {
    private Context context;
    private static LogUtils mLogUtils;

    public LogUtils(Context context) {
        this.context = context;
    }

    public static LogUtils getInstance(Context context) {
        if (null == mLogUtils) {
            mLogUtils = new LogUtils(context);
        }

        return mLogUtils;
    }

    public void startLoggingToFile() {
        if (isExternalStorageWritable()) {

            File appDirectory = new File(Environment.getExternalStorageDirectory().getPath() + "/Android");
            File logDirectory = new File(appDirectory + "/xp");
            File logFile = new File(logDirectory, "SmartxPHED" + formatLogDate(new Date()) + ".log");

            // create app folder
            if (!appDirectory.exists()) {
                appDirectory.mkdir();
            }

            // create log folder
            if (!logDirectory.exists()) {
                logDirectory.mkdir();
            }

            // clear the previous logcat and then write the new one to the file
            try {
                //Process process = Runtime.getRuntime().exec("logcat -c");
                Process process = Runtime.getRuntime().exec("logcat -f " + logFile);
            } catch (IOException e) {
                e.printStackTrace();
            }

        } else if (isExternalStorageReadable()) {
            // only readable
        } else {
            // not accessible
        }
    }

    /* Checks if external storage is available for read and write */
    public boolean isExternalStorageWritable() {
        String state = Environment.getExternalStorageState();
        if (Environment.MEDIA_MOUNTED.equals(state)) {
            return true;
        }
        return false;
    }

    /* Checks if external storage is available to at least read */
    public boolean isExternalStorageReadable() {
        String state = Environment.getExternalStorageState();
        if (Environment.MEDIA_MOUNTED.equals(state) ||
                Environment.MEDIA_MOUNTED_READ_ONLY.equals(state)) {
            return true;
        }
        return false;
    }

    public static void main(String[] args) {
        NumberFormat numberFormat = NumberFormat.getCurrencyInstance(Locale.GERMANY);
        double a = 0.0;
        System.out.println("Result: " + numberFormat.format(a));
    }

    String formatLogDate(Date date) {
        if (null != date) {
            SimpleDateFormat simpleDateFormat = new SimpleDateFormat("ddMMMyyyy");
            return simpleDateFormat.format(date);
        } else {
            return "";
        }
    }
}
