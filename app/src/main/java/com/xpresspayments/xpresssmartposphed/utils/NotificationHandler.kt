package com.xpresspayments.xpresssmartposphed.utils

import android.content.Context
import com.xpresspayment.morefun10Q.logTrace
import com.xpresspayments.xpresssmartposphed.BuildConfig
import com.xpresspayments.xpresssmartposphed.db
import com.xpresspayments.xpresssmartposphed.extensions.buildRetrofit
import com.xpresspayments.xpresssmartposphed.extensions.locationString
import com.xpresspayments.xpresssmartposphed.services.tmsNotificationService
import com.xpresspayments.xpresssmartposphed.services.toNotificationData
import com.xpresspos.epmslib.entities.TransactionResponse
import com.xpresspos.epmslib.entities.TransactionType
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch

object NotificationHandler {

    private val baseUrl by lazy {
        if (BuildConfig.DEBUG) {
            "http://80.88.8.245:1211/tms/"
        } else {
            "http://80.88.8.24:1211/tms/"
        }
    }

    private val service by lazy {
         buildRetrofit(baseUrl).tmsNotificationService
    }

    fun sendNotification (context: Context, response: TransactionResponse) {
        GlobalScope.launch {
            kotlin.runCatching {
                //val tmsConfig = context.tmsConfig
                service.pushNotification(response.toNotificationData(context.locationString))
            }.onSuccess {
                logTrace(it.toString())
                response.isNotified =  it.responseCode == "00"
                if(response.transactionType == TransactionType.REVERSAL) {
                    context.db.transactionResponseDao().update(response)
                } else {
                    context.db.transactionResponseDao().save(response)
                }

            } .onFailure {
                response.isNotified = false
                context.db.transactionResponseDao().save(response)
            }
        }
    }

    fun rePushNotification(context: Context) {
        GlobalScope.launch {
            context.db.transactionResponseDao().getByNotificationStatus(false).forEach { response ->
                kotlin.runCatching {
                    service.pushNotification(response.toNotificationData(context.locationString))
                }.onSuccess {
                    logTrace(it.toString())
                    response.isNotified = it.responseCode == "00"
                    context.db.transactionResponseDao().update(response)
                } .onFailure {
                    response.isNotified = false
                    context.db.transactionResponseDao().update(response)
                }
            }

        }
    }
}