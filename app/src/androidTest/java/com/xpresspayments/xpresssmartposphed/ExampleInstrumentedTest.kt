package com.xpresspayments.xpresssmartposphed

import androidx.test.InstrumentationRegistry
import androidx.test.runner.AndroidJUnit4
import com.xpresspos.epmslib.utils.IsoAdapter

import org.junit.Test
import org.junit.runner.RunWith

import org.junit.Assert.*

/**
 * Instrumented test, which will execute on an Android device.
 *
 * See [testing documentation](http://d.android.com/tools/testing).
 */
@RunWith(AndroidJUnit4::class)
class ExampleInstrumentedTest {
    @Test
    fun useAppContext() {
        // Context of the newlandApp under test.
        val appContext = InstrumentationRegistry.getTargetContext()
        assertEquals("com.xpresspayments.xpresssmartpos", appContext.packageName)

        val data = "0210F23C46D12FE08200000000000000002016476173900101003600100000000000255505091314211314211314210509221293110510010012D0000000006476173344761739001010036D22122011184491489190509131421062978002012058P06720580LA00010068SOLIMEX NIGERIA LIMI   KN           GONG5662589F26083E497232FA61D1B89F2701809F100706011203A498029F3704212FFE299F36020018950500000080009A031905099C01009F02060000000025559F03060000000000005F2A020566820278009F1A0205669F34030403029F3303E0D0C89F3501259F1E0834363231373237318407A00000000310109F090200965F340101015510101511344101"

        IsoAdapter.processISOBitStreamWithJ8583(appContext, data)
    }


//    @Test fun testCallHomeWorker() {
//
//        val callHomeWorker = OneTimeWorkRequestBuilder<CallHomeWorker>().build()
//        val job = WorkManager.getInstance().enqueue(callHomeWorker)
//
//        assertEquals(job.state, Operation.IN_PROGRESS)
//    }

}
