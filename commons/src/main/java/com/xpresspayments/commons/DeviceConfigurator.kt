package com.xpresspayments.commons

import android.content.Context
import com.xpresspos.epmslib.entities.ConfigData
import com.xpresspos.epmslib.entities.KeyHolder
import com.xpresspos.epmslib.entities.NibssAID
import com.xpresspos.epmslib.entities.NibssCA

interface DeviceConfigurator {
   fun processKeys(context: Context, keyHolder: KeyHolder): Boolean
   fun processConfigData(context: Context, terminalID: String, configData: ConfigData): Boolean
   fun processCAs(context: Context, caList: List<NibssCA>): Boolean
   fun processAIDs(context: Context,aidList: List<NibssAID>): Boolean
}