package com.xpresspayments.commons.device.oem

typealias AID = EMVParamApplication
typealias CAPK = EMVParamKey

const val PARTIAL_MATCH = "00"
const val FULL_MATCH = "01"