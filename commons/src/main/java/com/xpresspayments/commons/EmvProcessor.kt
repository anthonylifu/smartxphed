package com.xpresspayments.commons

import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.Lifecycle
import androidx.lifecycle.LifecycleObserver
import androidx.lifecycle.OnLifecycleEvent
import com.xpresspos.epmslib.entities.HostConfig
import com.xpresspos.epmslib.entities.TransactionRequestData
import com.xpresspos.epmslib.entities.TransactionResponse
import com.xpresspos.epmslib.processors.TransactionProcessor
import io.reactivex.*

abstract class EmvProcessor(val activity: AppCompatActivity,
                            val hostConfig: HostConfig
): LifecycleObserver, AbstractCoroutineScope() {

    private var isAlive =  false

    private lateinit var resultEmitter: ObservableEmitter<TransactionResponse>
    private lateinit var stateEmitter: FlowableEmitter<ProcessState>

    init {
        activity.lifecycle.addObserver(this)
    }

    val processor by lazy {
        TransactionProcessor(hostConfig)
    }


    val onResponse: Observable<TransactionResponse> by lazy {
        Observable.create<TransactionResponse> {
            resultEmitter = it
        }
    }

    val onStateChanged: Flowable<ProcessState> by lazy {
        Flowable.create<ProcessState> ({
            stateEmitter = it
        }, BackpressureStrategy.LATEST)
    }


    @OnLifecycleEvent(Lifecycle.Event.ON_RESUME)
    private fun onResume() {
        isAlive = true
    }

    fun setError(e: Throwable) {
        if (isAlive) {
            resultEmitter.tryOnError(e)
        }
    }

    fun setResponse(response: TransactionResponse) {
        if (isAlive) {
            resultEmitter.onNext(response)
        }
    }

    fun setState(state: ProcessState) {
        if (isAlive) {
            stateEmitter.onNext(state)
        }
    }

    override fun destroy() {
        super.destroy()
        onDestroy()
    }

    @OnLifecycleEvent(Lifecycle.Event.ON_DESTROY)
    private fun onDestroy() {
        isAlive = false
        activity.lifecycle.removeObserver(this)

        stateEmitter.onComplete()
        resultEmitter.onComplete()
    }


    abstract fun startTransaction(requestData: TransactionRequestData, reader: Reader = com.xpresspayments.commons.Reader.CT_CTLS)
    abstract fun abortTransaction()

}





data class ProcessState (val state: States,
                         val message: String) {
    enum class States {
        CARD_ENTRY, PROCESSING
    }
}
