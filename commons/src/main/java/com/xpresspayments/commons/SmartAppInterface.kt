package com.xpresspayments.commons

import android.content.Context
import androidx.appcompat.app.AppCompatActivity
import com.xpresspayments.commons.printer.Printer
import com.xpresspos.epmslib.entities.HostConfig


interface SmartAppInterface {
    fun initializeDevice(context: Context)
    fun destruct(context: Context)
    fun getPrinter(context: Context): Printer

    val device: Device
    val terminalSerial: String

    val deviceConfigurator: DeviceConfigurator

    fun getEmvProcessor(activity: AppCompatActivity,
                        hostConfig: HostConfig
    ): EmvProcessor
}


enum class Device {
    NewlandN910, VerifoneX990, Morefun10Q, HorizonK11
}