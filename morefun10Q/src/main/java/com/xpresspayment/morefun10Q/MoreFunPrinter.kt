package com.xpresspayment.morefun10Q

import android.graphics.Bitmap
import android.graphics.ColorSpace
import android.os.Bundle
import com.morefun.yapi.device.printer.OnPrintListener
import com.morefun.yapi.device.printer.PrinterConfig
import com.xpresspayment.morefun10Q.utils.CombBitmap
import com.xpresspayment.morefun10Q.utils.GenerateBitmap
import com.xpresspayments.commons.printer.*
import kotlinx.coroutines.runBlocking
import kotlin.coroutines.resume
import kotlin.coroutines.suspendCoroutine

class MoreFunPrinter(app: MorefunApp): Printer {
    private val LINE_LENTH = 30
    private var printerBitmap = CombBitmap()

    private val printer =  app.getDeviceService()!!.printer
   // private val multipAppPrinter =  app.getDeviceService()!!.multipleAppPrinter

    private fun getAlign(align: Align): GenerateBitmap.AlignEnum = when (align) {
        Align.RIGHT -> GenerateBitmap.AlignEnum.RIGHT
        Align.LEFT -> GenerateBitmap.AlignEnum.LEFT
        Align.CENTER -> GenerateBitmap.AlignEnum.CENTER
    }

    private fun getTextSize(size: FontSize): Int {
        return when (size) {
            FontSize.TINY -> 16
            FontSize.SMALL -> 20
            FontSize.NORMAL -> 24
            FontSize.LARGE -> 36
            FontSize.EXTRA_LARGE -> 42
            FontSize.BIG -> 55
        }
    }

    private fun isBold(style: FontStyle) = when (style) {
        FontStyle.BOLD , FontStyle.BOLD_INVERTED -> true
        else -> false
    }

    private fun isInverted(style: FontStyle) = when (style) {
        FontStyle.INVERTED , FontStyle.BOLD_INVERTED -> true
        else -> false
    }


    override fun addText(text: String, format: TextFormat) {
        printerBitmap.addBitmap(GenerateBitmap.str2Bitmap(text, getTextSize(format.fontSize), getAlign(format.align), isBold(format.fontStyle), isInverted(format.fontStyle)))
    }

    override fun addDoubleText(left: String, right: String, format: TextFormat) {
        val maxRightLen = if (format.fontSize !in arrayOf(FontSize.SMALL, FontSize.TINY)) LINE_LENTH - left.length else right.length //temp fix, need handle bigger fonts

        printerBitmap.addBitmap(GenerateBitmap.str2Bitmap(left, right.substring(0, right.length.coerceAtMost(maxRightLen)),
            getTextSize(format.fontSize), isBold(format.fontStyle), isInverted(format.fontStyle)))
    }

    override fun addImage(bitmap: Bitmap, width: Int, height: Int, offset: Int) {
        val newBitmap =  Bitmap.createScaledBitmap(bitmap, width, height, true)
        printerBitmap.addBitmap(newBitmap)
    }

    override fun addBarcode(text: String, width: Int, align: Align) {
        printerBitmap.addBitmap(GenerateBitmap.generateBarCodeBitmap(text, width, width))
    }

    override fun addQrCode(text: String, width: Int, height: Int) {
        printerBitmap.addBitmap(GenerateBitmap.generateQRCodeBitmap(text, width))
    }

    override fun feedLine(count: Int) {
        for ( i in 1..count) {
            printerBitmap.addBitmap(GenerateBitmap.generateGap(getTextSize(FontSize.SMALL)))
        }
    }

    override fun addLine() {
        printerBitmap.addBitmap(GenerateBitmap.generateLine(1))
    }

    override fun getStatus(): Printer.Status = morefunStateToPrinterStatus(printer.status)

    private fun morefunStateToPrinterStatus(state: Int) =  when (state) {
        0 -> Printer.Status.OK
        -1004 -> Printer.Status.BUSY
        -1005 -> Printer.Status.OUT_OF_PAPER
        -1007 -> Printer.Status.OVER_HEAT
        -1008 -> Printer.Status.BUFFER_OVERFLOW
        else -> Printer.Status.ERROR
    }

    override  fun print(): Printer.Status = runBlocking {
        val result =  suspendCoroutine<Printer.Status> {
           printer.initPrinter()

            val printConfig = Bundle().apply {
                putInt(PrinterConfig.COMMON_GRAYLEVEL, 25)
                putBoolean(PrinterConfig.COMMON_IS_CUTPAPER, true)
            }

//            multipAppPrinter.printImage(printerBitmap.combBitmap, object : OnPrintListener.Stub() {
//                override fun onPrintResult(error: Int) {
//                    it.resume(morefunStateToPrinterStatus(error))
//                }
//            }, printConfig)


            printer.setConfig(printConfig)

            printer.appendImage(printerBitmap.combBitmap)
            printer.startPrint(object : OnPrintListener.Stub() {
                override fun onPrintResult(error: Int) {
                    it.resume(morefunStateToPrinterStatus(error))
                }
            })

            printer.cutPaper()
        }

        printerBitmap.removeAll()

        result
    }
}