package com.xpresspayment.morefun10Q

import android.content.Context
import android.util.Log
import com.morefun.yapi.device.pinpad.WorkKeyType
import com.xpresspayments.commons.DeviceConfigurator
import com.xpresspayments.commons.device.oem.*
import com.xpresspos.epmslib.entities.*
import com.xpresspos.epmslib.extensions.hexByteArray
import com.xpresspos.epmslib.extensions.hexString
import com.xpresspos.epmslib.extensions.padLeft

object MoreFunDeviceConfigurator: DeviceConfigurator {

    private const val MASTER_KEY_INDEX = 0
    private const val WORK_KEY_INDEX = 0

    private const val TAG = "MFDeviceConfigurator"

    override fun processKeys(context: Context, keyHolder: KeyHolder): Boolean {
        val masterKey: ByteArray = keyHolder.masterKey.hexByteArray()

        val retCode = DeviceHelper.getPinpad().loadPlainMKey(
            MASTER_KEY_INDEX,
            masterKey,
            masterKey.size,
            true
        )

        Log.d(TAG, "loadClearMasterKey: $retCode")

        val pinKey: ByteArray =  keyHolder.clearPinKey.hexByteArray().also { logTrace("Clear Pin Key: ${it.hexString()}") }

        val retCode2 = DeviceHelper.getPinpad().loadPlainWKey(
            WORK_KEY_INDEX,
            WorkKeyType.PINKEY,
            pinKey,
            pinKey.size
        )

        Log.d(TAG, "loadPinKey: $retCode2")

        return retCode == 0 &&  retCode2 == 0
    }

    override fun processConfigData(
        context: Context,
        terminalID: String,
        configData: ConfigData
    ): Boolean {
        return true
    }

    override fun processCAs(context: Context, caList: List<NibssCA>): Boolean {
        if (caList.isEmpty()) return false

        val emv = DeviceHelper.getEmvHandler()
        emv.clearCAPKParam()


        caList.forEach {
            logTrace(
                "${it.keyIndex}, ${it.RID}, addCAPKParam::result = ${
                    emv.addCAPKParam(it.toCapkParam.tlvString.hexByteArray())
                }"
            )
        }

         return true
    }

    override fun processAIDs(context: Context, aidList: List<NibssAID>): Boolean {
        if (aidList.isEmpty()) return false

        val emv = DeviceHelper.getEmvHandler()
        emv.clearAIDParam()

        aidList.forEach {
            logTrace("${it.AID}, ${it.applicationName}, addAidParam::result = ${
                kotlin.runCatching {
                    emv.addAidParam(it.toEmvAidParam.tlvString.hexByteArray())
                }.onFailure { 
                    it.printStackTrace()
                }
                
            }")
        }

        return  true
    }

}


private val NibssCA.toCapkParam: CAPK
    get() {
      return  CAPK().apply {
            comment = keyName
            flagAppendRemoveClear = 1
            append(EMVParamKey.TAG_Exponent_DF04, exponent)
            append(EMVParamKey.TAG_Index_9F22, keyIndex)
            append(EMVParamKey.TAG_RID_9F06, RID)

          if (RID == "A000000371") {
              when (keyIndex) {
                  "05" -> {
                      append(
                          EMVParamKey.TAG_KEY_DF02,
                          "B036A8CAE0593A480976BFE84F8A67759E52B3D9F4A68CCC37FE720E594E5694CD1AE20E1B120D7A18FA5C70E044D3B12E932C9BBD9FDEA4BE11071EF8CA3AF48FF2B5DDB307FC752C5C73F5F274D4238A92B4FCE66FC93DA18E6C1CC1AA3CFAFCB071B67DAACE96D9314DB494982F5C967F698A05E1A8A69DA931B8E566270F04EAB575F5967104118E4F12ABFF9DEC92379CD955A10675282FE1B60CAD13F9BB80C272A40B6A344EA699FB9EFA6867"
                      )
                      append(EMVParamKey.TAG_Hash_DF03, "676822D335AB0D2C3848418CB546DF7B6A6C32C0")
                  }
                  "06" -> {
                      append(
                          EMVParamKey.TAG_KEY_DF02,
                          "D2DA0134B4DFC93A75EE8960C99896D50A91527B87BA7B16CDB77E5B6FDB750EB70B54026CADDA1D562C77A2C6DA541E94BC415D43E68489B16980F2E887C09E4CF90E2E639B179277BBA0E982CCD1F80521D1457209125B3ABCD309E1B92B5AEDA2EB1CBF933BEAD9CE7365E52B7D17FCB405AA28E5DE6AA3F08E764F745E70859ABCBA41E570A6E4367B3D6FECE723B73ABF3EB53DCDE3816E8A813460447021509D0DFDF2EEEE74CC35485FB55C26836EB3BF9C7DEBEE6C0B77B7BE059233801CF76B321FCA25FB1E63117AE1865E23161EC39D7B1FB84256C2BE72BF8EC771548DB9F00BEF77C509FADA15E2B53FF950D383F96211D3"
                      )
                      append(EMVParamKey.TAG_Hash_DF03, "F5BAB84ECE5F8BD45511E5CA861B80C7E6C51F55")
                  }
              }
          } else {
              append(
                  EMVParamKey.TAG_KEY_DF02,
                  modulus
              )
              append(EMVParamKey.TAG_Hash_DF03, hash)
          }




            append(EMVParamKey.TAG_ExpiryDate_DF05, "20251231".toByteArray().hexString())
        }
    }

private val NibssAID.toEmvAidParam: AID
    get()  {
        return AID().apply {
              append(EMVParamApplication.TAG_AID_9F06, AID)
              append(EMVParamApplication.TAG_VerNum_9F09, applicationVersion.padLeft(4, '0'))
              setComment(applicationName)
              append(EMVParamApplication.TAG_ASI_DF01, FULL_MATCH)
              append(EMVParamApplication.TAG_FloorLimit_9F1B, "00000000")
              append(EMVParamApplication.TAG_Threshold_DF15, "00000000")
              append(EMVParamApplication.TAG_TargetPercentage_DF17, "00")
              append(EMVParamApplication.TAG_MaxTargetPercentage_DF16, "00")
              append(EMVParamApplication.TAG_TAC_Denial_DF13, tacDenial)
              append(EMVParamApplication.TAG_TAC_Online_DF12, tacOnline)
              append(EMVParamApplication.TAG_TAC_Default_DF11, tacDefault)
              append(EMVParamApplication.TAG_DefaultTDOL_97_Optional, if (TDOL.isNotBlank()) TDOL else "9F1A0295059A039C01")
              append(EMVParamApplication.TAG_DefaultDDOL_DF14, if (DDOL.isNotBlank()) DDOL else "9F3704")
              append(EMVParamApplication.TAG_CountryCodeTerm_9F1A, "0566")
              append(EMVParamApplication.TAG_CurrencyCodeTerm_5F2A, "0566")
              append(EMVParamApplication.TAG_AppTerminalType_9F35, "22")
              append(EMVParamApplication.TAG_AppTermCap_9F33, "E0F8C8")
              append(EMVParamApplication.TAG_AppTermAddCap_9F40, "F000F0A001")
              append(EMVParamApplication.TAG_ECTransLimit_9F7B, "000000001388")
              append(EMVParamApplication.TAG_CTLSTransLimit_DF20, "999999999999")
              append(EMVParamApplication.TAG_CTLSFloorLimit_DF19, "000000000000")
              append(EMVParamApplication.TAG_CTLSCVMLimit_DF21, "000000005000")

              flagAppendRemoveClear = 1

              append(0xDF1B, "000000000000")
              append(EMVParamApplication.TAG__DF04, "000000000000")
              append(0xDF1A, "000000000000")

              append(EMVParamApplication.TAG_TTQ_9F66, "27004080")
              append(EMVParamApplication.TAG__DF18, "01")
          }
    }
