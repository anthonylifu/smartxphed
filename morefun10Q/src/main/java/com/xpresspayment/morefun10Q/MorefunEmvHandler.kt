package com.xpresspayment.morefun10Q

import android.os.Bundle
import android.os.RemoteException
import com.google.android.material.dialog.MaterialAlertDialogBuilder
import com.morefun.yapi.ServiceResult
import com.morefun.yapi.device.reader.icc.IccCardType
import com.morefun.yapi.device.reader.icc.IccReaderSlot
import com.morefun.yapi.device.reader.icc.OnSearchIccCardListener
import com.morefun.yapi.emv.*
import com.xpresspayment.morefun10Q.utils.EmvUtil
import com.xpresspayments.commons.EmvTag
import com.xpresspayments.commons.ProcessState
import com.xpresspayments.commons.Reader
import com.xpresspayments.commons.displayMessageId
import com.xpresspos.epmslib.entities.CardData
import com.xpresspos.epmslib.entities.TransactionRequestData
import com.xpresspos.epmslib.entities.TransactionResponse
import com.xpresspos.epmslib.extensions.hexByteArray
import com.xpresspos.epmslib.extensions.hexString
import com.xpresspos.epmslib.tlv.TLVList

class MorefunEmvHandler(
    private val processor: Morefun10CardProcessor,
    private val channel: Int,
    private val requestData: TransactionRequestData
): OnEmvProcessListener.Stub() {
    private val emv
        get() =  DeviceHelper.getEmvHandler()


    private var allowed = true
    private val context = processor.activity

    private var hostResponse: TransactionResponse? = null
    private var pinBlock: String? = null

   init {
       pinBlock = null
       hostResponse = null
   }
    
    @Throws(RemoteException::class)
    override fun onSelApp(appNameList: List<String>, isFirstSelect: Boolean) {
        logTrace("onSelApp")

        context.runOnUiThread {
            MaterialAlertDialogBuilder(context)
                .setTitle("Select App")
                .setItems(appNameList.toTypedArray<CharSequence>()) { _, which ->
                    emv.onSetSelAppResponse(which)
                }.setOnCancelListener {
                    emv.onSetSelAppResponse(-1)
                }.show()
        }
    }

    @Throws(RemoteException::class)
    override fun onConfirmCardNo(cardNo: String) {
        logTrace("onConfirmCardNo:$cardNo")
        DeviceHelper.getEmvHandler().onSetConfirmCardNoResponse(true)
    }

    @Throws(RemoteException::class)
    override fun onCardHolderInputPin(isOnlinePin: Boolean, leftTimes: Int) {
        logTrace("onCardHolderInputPin isOnlinePin:$isOnlinePin")
        val cardNo: String = EmvUtil.readPan()
        if (isOnlinePin) {
            PinHandler.inputOnlinePin(cardNo, requestData.amount + requestData.otherAmount, object : OnInputPinListener {
                override fun onInputPin(pinBlock: ByteArray?) {
                    try {
                        this@MorefunEmvHandler.pinBlock = pinBlock?.hexString()
                        DeviceHelper.getEmvHandler().onSetCardHolderInputPin(pinBlock)
                    } catch (e: RemoteException) {
                    }
                }
            })
        } else {
            PinHandler.inputOfflinePin(cardNo,requestData.amount + requestData.otherAmount, object : OnInputPinListener {
                override fun onInputPin(pinBlock: ByteArray?) {
                    try {
                        this@MorefunEmvHandler.pinBlock = null
                        DeviceHelper.getEmvHandler().onSetCardHolderInputPin(pinBlock)
                    } catch (e: RemoteException) {
                    }
                }
            })
        }
    }

    @Throws(RemoteException::class)
    override fun onPinPress(keyCode: Byte) {
        logTrace("Callback:onPinPress")
    }

    @Throws(RemoteException::class)
    override fun onDisplayOfflinePin(retCode: Int) {
        logTrace(
            "Callback:onDisplayOfflinePin: " + retCode + ",is succeed: " + (retCode == 0)
        )

       val message =  when (retCode) {
            0 -> "Pin OK"
            else  -> "Incorrect Pin"
        }

       processor. setState(
            ProcessState(
                ProcessState.States.PROCESSING,
                message
            )
        )
    }

    @Throws(RemoteException::class)
    override fun inputAmount(type: Int) {
        logTrace("Callback:inputAmount ")
        try {
            DeviceHelper.getEmvHandler().onSetInputAmountResponse((requestData.amount / 100.0).toString())
        } catch (e: RemoteException) {
            e.printStackTrace()
        }
    }

    @Throws(RemoteException::class)
    override fun onCertVerify(certName: String?, certInfo: String?) {
        logTrace("Callback:onCertVerify")
        DeviceHelper.getEmvHandler().onSetCertVerifyResponse(true)
    }

    @Throws(RemoteException::class)
    override fun onOnlineProc(data: Bundle) {
        if (!allowed) {
            return
        }
        processor.setState(
            ProcessState(
                ProcessState.States.PROCESSING,
                "Processing online"
            )
        )

        val tlv = EmvUtil.getTLVDatas(CardData.NIBSS_TAGS)
        val track2Data = EmvUtil.readTrack2().replace('=', 'D')
        val panSequenceNo = data.getByteArray(EmvOnlineRequest.CARDSN)?.hexString().also { logTrace("EmvOnlineRequest::CARDSN = $it") } ?: ""

        val cardData  = CardData(
            track2Data = track2Data,
            nibssIccSubset = tlv,
            panSequenceNumber = panSequenceNo,
            posEntryMode = if (channel == EmvChannelType.FROM_ICC) "051" else "071"
        )

        val tempBlock = data.getByteArray(EmvOnlineRequest.PIN)?.hexString()?.trim()
        if (tempBlock?.length == 16) {
            cardData.pinBlock  = tempBlock
        }

        val online = Bundle()
        kotlin.runCatching {
            processor.processor.processTransaction(processor.activity, requestData, cardData)
        }.onFailure {
            online.putString(EmvOnlineResult.REJCODE, "20")
            online.putByteArray(EmvOnlineResult.RECVARPC_DATA, null)

            DeviceHelper.getEmvHandler().onSetOnlineProcResponse(ServiceResult.Success, online)
        }.onSuccess {
            hostResponse = it

            online.putString(EmvOnlineResult.REJCODE, hostResponse?.responseCode)
            online.putByteArray(
                EmvOnlineResult.RECVARPC_DATA,
                hostResponse?.responseDE55?.hexByteArray()
            )

            DeviceHelper.getEmvHandler().onSetOnlineProcResponse(ServiceResult.Success, online)
        }

    }

    @Throws(RemoteException::class)
    override fun onContactlessOnlinePlaceCardMode(mode: Int) {
        logTrace("Callback:onContactlessOnlinePlaceCardMode")
        if (mode == EmvListenerConstrants.NEED_CHECK_CONTACTLESS_CARD_AGAIN) {
             startSearchContactless(object :
                OnSearchIccCardListener.Stub() {
                @Throws(RemoteException::class)
                override fun onSearchResult(retCode: Int, bundle: Bundle) {
                    processor.stopSearch()
                    try {
                        DeviceHelper.getEmvHandler()
                            .onSetContactlessOnlinePlaceCardModeResponse(ServiceResult.Success == retCode)
                    } catch (e: RemoteException) {
                        e.printStackTrace()
                    }
                }
            })
        } else {
            //show Dialog Prompt the user not to remove the card
            DeviceHelper.getEmvHandler().onSetContactlessOnlinePlaceCardModeResponse(true)
        }
    }

    @Throws(RemoteException::class)
    override fun onFinish(retCode: Int, data: Bundle) {
        allowed =  false

        val tlv =  EmvUtil.getTLVDatas(EmvUtil.arqcTLVTags)
        hostResponse?.let {
            tlv?.let(::updateCardInfo)
            hostResponse!!.let(processor::setResponse)
        } ?: kotlin.run {
            processor.setError(RuntimeException("Emv transaction failed ($retCode)"))
        }
    }

    @Throws(RemoteException::class)
    override fun onSetAIDParameter(aid: String?) {
        logTrace("Callback:onSetAIDParameter")
    }

    @Throws(RemoteException::class)
    override fun onSetCAPubkey(rid: String?, index: Int, algMode: Int) {
        logTrace("Callback:onSetCAPubkey")
    }

    @Throws(RemoteException::class)
    override fun onTRiskManage(pan: String?, panSn: String?) {
        logTrace("Callback:onTRiskManage")
    }

    @Throws(RemoteException::class)
    override fun onSelectLanguage(language: String?) {
        
    }

    @Throws(RemoteException::class)
    override fun onSelectAccountType(accountTypes: List<String?>?) {
       
    }

    @Throws(RemoteException::class)
    override fun onIssuerVoiceReference(pan: String?) {
        
    }


    @Throws(RemoteException::class)
    private fun startSearchContactless(listener: OnSearchIccCardListener.Stub) {
        processor.setState(
            ProcessState(
                ProcessState.States.CARD_ENTRY,
                context.getString(Reader.CTLS.displayMessageId())
            )
        )
       val rfReader = DeviceHelper.getIccCardReader(IccReaderSlot.RFSlOT)
        rfReader.searchCard(
            listener,
            60,
            arrayOf(IccCardType.CPUCARD, IccCardType.AT24CXX, IccCardType.AT88SC102)
        )
    }


    private fun updateCardInfo(iccData: String) {
        logTrace("updateCardInfo")
        hostResponse?.let {
            val tlv = TLVList.fromBinary(iccData)
            it.iccData =  iccData

            it.appCryptogram =
                tlv.getTLVVL(
                    EmvTag.APPLICATION_CRYPTOGRAM_9F26.toString(
                        16
                    ).toUpperCase().also { logTrace("Tag: $it") })
            it.TVR =
                tlv.getTLVVL(
                    EmvTag.TERMINAL_VERIFICATION_RESULT_95.toString(
                        16
                    ).also { logTrace("Tag: $it") })

            it.AID = tlv.getTLVVL(
                EmvTag.DEDICATED_FILE_NAME_84.toString(16).also {
                    logTrace(
                        "Tag: $it"
                    )
                })
            it.TSI =tlv.getTLVVL(
                EmvTag.TSI_9B.toString(16).toUpperCase().also {
                    logTrace(
                        "Tag: $it"
                    )
                })
            it.cardHolder = String(
                tlv.getTLVVL(
                    EmvTag.CARDHOLDER_NAME_5F20.toString(16).toUpperCase().also {
                        logTrace(
                            "Tag: $it, "
                        )
                    }).also { logTrace("Value $it") } ?.hexByteArray()?: byteArrayOf()
            ).trim()

            it.cardLabel = String(
                tlv.getTLVVL(
                    EmvTag.APPLICATION_LABEL.toString(16).also {
                        logTrace(
                            "Tag: $it"
                        )
                    })?.hexByteArray() ?: byteArrayOf()
            )
        }
    }

}