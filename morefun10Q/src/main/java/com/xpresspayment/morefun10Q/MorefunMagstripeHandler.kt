package com.xpresspayment.morefun10Q

import android.os.RemoteException
import android.util.Log
import com.morefun.yapi.device.reader.mag.MagCardInfoEntity
import com.xpresspayments.commons.ProcessState
import com.xpresspos.epmslib.entities.CardData
import com.xpresspos.epmslib.entities.TransactionRequestData
import com.xpresspos.epmslib.entities.getCardHolderFromTrack1
import com.xpresspos.epmslib.extensions.hexString

class MorefunMagstripeHandler(private val processor: Morefun10CardProcessor, private val requestData: TransactionRequestData)  {

    fun processOnline (entity: MagCardInfoEntity) {
        val track2Data =  entity.tk2.replace('=', 'D').also { logTrace("Magstripe Track2: $it") }
        val cardHolderName = entity.cardholderName.also { logTrace("Magstripe Cardhholdername: $it") }
        val cardData = CardData(
            track2Data = track2Data,
            nibssIccSubset = "",
            panSequenceNumber = "",
            posEntryMode = "901"
        )

       // val cardHolderName =  getCardHolderFromTrack1(entity.tk1)

        PinHandler.inputOnlinePin(cardData.pan,requestData.amount + requestData.otherAmount, object : OnInputPinListener {
            override fun onInputPin(pinBlock: ByteArray?) {
                try {
                    cardData.pinBlock = pinBlock?.hexString()
                    onRequestOnline(cardData, cardHolderName)
                } catch (e: RemoteException) {
                }
            }
        })
    }


    private fun onRequestOnline(cardData: CardData, cardHolderName: String) {
        processor.setState(
            ProcessState(
                ProcessState.States.PROCESSING,
                "Processing online"
            )
        )

        logTrace("OnRequestOnline Pinblock: ${cardData.pinBlock}")

        val result = kotlin.runCatching {
            processor.processor.processTransaction(
                processor.activity,
                requestData,
                cardData
            )
        }

        if (result.isFailure) {
           logTrace("Host request error")
            val throwable = result.exceptionOrNull()
                ?: Exception("Error connecting to host"/*processor.activity.getString(R.string.error_connecting_to_host)*/)
            throwable.printStackTrace()
            processor.setError(throwable)
        }

        if (result.isSuccess) {
            val hostResponse = result.getOrNull()
            println(hostResponse)

            hostResponse?.let {
                it.cardHolder = cardHolderName
                it.cardLabel = if (cardData.track2Data.isNotBlank()) "Magstripe" else "CNP"
                processor.setResponse(it)
            }
        }

    }
}