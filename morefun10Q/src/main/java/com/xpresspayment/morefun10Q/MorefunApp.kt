package com.xpresspayment.morefun10Q

import android.content.ComponentName
import android.content.Context
import android.content.Intent
import android.content.ServiceConnection
import android.os.Bundle
import android.os.IBinder
import android.os.RemoteException
import android.util.Log
import androidx.appcompat.app.AppCompatActivity
import com.morefun.yapi.engine.DeviceInfoConstrants
import com.morefun.yapi.engine.DeviceServiceEngine
import com.xpresspayments.commons.Device
import com.xpresspayments.commons.DeviceConfigurator
import com.xpresspayments.commons.SmartAppInterface
import com.xpresspayments.commons.printer.Printer
import com.xpresspos.epmslib.entities.HostConfig


private const val TAG = "MorefunApp"
open class MorefunApp: SmartAppInterface  {

    private val SERVICE_ACTION = "com.morefun.ysdk.service"
    private val SERVICE_PACKAGE = "com.morefun.ysdk"

    private val businessId = "00000000"

    private var deviceServiceEngine: DeviceServiceEngine? = null
    lateinit var context: Context
    private set

    override fun initializeDevice(context: Context) {
        this.context = context
        bindDeviceService()
    }

    override fun destruct(context: Context) {
        context.unbindService(connection)
    }

    override fun getPrinter(context: Context): Printer {
            return MoreFunPrinter(this)
    }

    override val device: Device
        get() =  Device.Morefun10Q

    override val terminalSerial: String
        get() = getDeviceService()?.devInfo?.getString(DeviceInfoConstrants.COMMOM_SN) ?: ""

    open val terminalModel: String
        get() = getDeviceService()?.devInfo?.getString(DeviceInfoConstrants.COMMOM_MODEL) ?: ""


    override val deviceConfigurator: DeviceConfigurator
        get() = MoreFunDeviceConfigurator

    override fun getEmvProcessor(activity: AppCompatActivity, hostConfig: HostConfig) = Morefun10CardProcessor(activity, hostConfig)

    fun getDeviceService(): DeviceServiceEngine? {
        return deviceServiceEngine
    }

    fun bindDeviceService() {
        if (null != deviceServiceEngine) {
            return
        }
        val intent = Intent()
        intent.action = SERVICE_ACTION
        intent.setPackage(SERVICE_PACKAGE)
        context.bindService(intent, connection, Context.BIND_AUTO_CREATE)
    }

    private val connection: ServiceConnection = object : ServiceConnection {
        override fun onServiceDisconnected(name: ComponentName) {
            deviceServiceEngine = null
            Log.e(TAG, "======onServiceDisconnected======")
        }

        override fun onServiceConnected(name: ComponentName, service: IBinder) {
            deviceServiceEngine = DeviceServiceEngine.Stub.asInterface(service)
            logTrace( "======onServiceConnected======")
            try {
                DeviceHelper.reset()
                DeviceHelper.initDevices(this@MorefunApp)
                val ret = DeviceHelper.getDeviceService().login(Bundle(), businessId)
                if (ret == 0) {
                    DeviceHelper.setLoginFlag(true)
                    validateModel()
                } else {
                    logTrace( "======Login failed======")
                }
            } catch (e: RemoteException) {
                e.printStackTrace()
            }
            linkToDeath(service)
        }

        private fun linkToDeath(service: IBinder) {
            try {
                service.linkToDeath({
                    logTrace( "======binderDied======")
                    deviceServiceEngine = null
                    bindDeviceService()
                }, 0)
            } catch (e: RemoteException) {
                e.printStackTrace()
            }
        }
    }

    @Throws(Error::class)
    open fun validateModel() {
        logTrace("Terminal model: $terminalModel")
        if (/*!(terminalModel.contains("10Q", true)|| terminalModel.contains("MF919", true))*/
            terminalModel.contains("K11", true)) throw Error("Incompatible device")
    }

}


fun logTrace(message: String) = com.xpresspayments.commons.logTrace(message)