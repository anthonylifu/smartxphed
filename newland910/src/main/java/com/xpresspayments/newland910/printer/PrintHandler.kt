package com.xpresspayments.newland910.printer

import android.content.Context
import android.graphics.Bitmap
import androidx.appcompat.app.AlertDialog
import com.newland.mtype.module.common.printer.PrintContext
import com.newland.mtype.module.common.printer.Printer
import com.newland.mtype.module.common.printer.PrinterResult
import com.newland.mtype.module.common.printer.PrinterStatus
import com.xpresspayments.newland910.R
import com.xpresspayments.newland910.extensions.printerModule
import com.xpresspayments.commons.runOnMain
import java.util.concurrent.TimeUnit

const val DEFAULT_CONTRAST = 6
const val DEFAULT_LETTER_SPACING = 6

class PrintHandler(val context: Context, fontName: String = "DroidSansFallback.ttf") {
    private val timeout = 60L

    private lateinit var printer: Printer

    private val scriptBuffer = StringBuilder()
    private val imageMap = HashMap<String, Bitmap>()

    init {
        reset(fontName)
    }

    private fun reset(fontName: String = "DroidSansFallback.ttf") {
        printer = context.printerModule
        printer.init()

        imageMap.clear()
        scriptBuffer.clear()
        printer.getFontsPath(context, fontName, true)?.let {
            addRawScript("!font $it\n")
        }
    }

    fun addBitmap(vararg printable: BitmapPrintable) {
        imageMap.putAll(printable.map {
            it.data
        })

        printable.forEach(::addPrintable)
    }

    fun addText(
        printable: TextPrintable, fontSize: Printable.FontSize = Printable.FontSize.NORMAL,
        letterSpacing: Int = DEFAULT_LETTER_SPACING, contrast: Int = DEFAULT_CONTRAST
    ) = addText(arrayOf(printable), fontSize, letterSpacing, contrast)

    fun addText(
        printable: Array<out TextPrintable>, fontSize: Printable.FontSize = Printable.FontSize.NORMAL,
        letterSpacing: Int = DEFAULT_LETTER_SPACING, contrast: Int = DEFAULT_CONTRAST
    ) {
        addRawScript("!asc $fontSize\n!gray $contrast\n")
        addRawScript("!yspace $letterSpacing\n")

        printable.forEach(::addPrintable)
    }

    fun addPrintable(printable: Printable, fontSize: Printable.FontSize, contrast: Int = DEFAULT_CONTRAST) {
        addRawScript("!asc $fontSize\n");
        addRawScript("!gray $contrast\n");
        addPrintable(printable)
    }

    fun addPrintable(printable: Printable) {
        addRawScript(printable.prePrint())
    }

    fun addRawScript(script: String) {
        scriptBuffer.append(script)
    }


    fun printBuffer() {
        print()
    }

    fun clearBuffer() {
        scriptBuffer.clear()
    }

    fun printReceipt(): PrinterResult {
        if (!scriptBuffer.endsWith("!NLPRNOVER\n")) {
            addRawScript("!NLPRNOVER\n")
        }
        return print()
    }

    private fun print(): PrinterResult {
//        Log.d("PRINTER", scriptBuffer.toString())
        val scriptData = scriptBuffer.toString()//.toByteArray(Charsets.UTF_8)
       val  status =  printer.printByScript(PrintContext.defaultContext(), scriptData, imageMap, timeout, TimeUnit.SECONDS)

        clearBuffer()
        return status
    }

    override fun toString() = scriptBuffer.toString()

    fun getStatus() = printer.status

}