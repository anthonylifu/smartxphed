package com.xpresspayments.newland910.printer

import java.lang.StringBuilder

data class DoubleStringPrintable(val leftContent: String, val rightContent: String): TextPrintable {

    override fun prePrint() = StringBuilder().apply {
        if (rightContent.isEmpty()) {
            append("*text ${Printable.Align.LEFT} $leftContent\n")
        } else {
            append("*TEXT ${Printable.Align.LEFT} $leftContent\n")
            append("*text ${Printable.Align.RIGHT} $rightContent\n")
        }
    }.toString()
}