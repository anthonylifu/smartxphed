package com.xpresspayments.newland910.printer

interface Printable {
    fun prePrint(): String


    enum class Align {
        LEFT, CENTER, RIGHT;

        override fun toString() = this.name.toLowerCase()[0].toString()

    }

    enum class FontSize {
        SMALL, NORMAL, LARGE;

        override fun toString() = this.name.toLowerCase().first().toString()
    }


    companion object {
        fun prePrintDottedLine () = object : Printable {
            override fun prePrint() = "*line\n"
        }

        fun prePrintFeedLine(count: Int = 1) = object : Printable {
            override fun prePrint() = "*feedline $count\n"
        }
    }
}

interface TextPrintable : Printable

