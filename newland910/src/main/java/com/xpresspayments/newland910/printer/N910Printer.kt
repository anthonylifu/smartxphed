package com.xpresspayments.newland910.printer

import android.content.Context
import android.graphics.Bitmap
import com.newland.mtype.module.common.printer.PrinterResult
import com.newland.mtype.module.common.printer.PrinterStatus
import com.xpresspayments.commons.printer.*

class N910Printer(context: Context): Printer {

    private val handler = PrintHandler(context)
    override fun addText(text: String, format: TextFormat) {
        handler.addText(StringPrintable(text, format.align.printableAlign), format.fontSize.printableFontSize)
    }

    override fun addDoubleText(left: String, right: String, format: TextFormat) {
       handler.addText(DoubleStringPrintable(left, right), format.fontSize.printableFontSize)
    }

    override fun addImage(bitmap: Bitmap, width: Int, height: Int, offset: Int) {
        handler.addBitmap(BitmapPrintable(System.currentTimeMillis().toString(), bitmap, width, height, Printable.Align.CENTER))
    }

    override fun addBarcode(text: String, expectedHeight: Int, align: Align) {
        handler.addPrintable(BarcodePrintable(text, align.printableAlign, expectedHeight))
    }

    override fun addQrCode(text: String, expectedHeight: Int, offset: Int) {
        handler.addPrintable(QrCodePrintable(text,  Printable.Align.CENTER, expectedHeight))
    }

    override fun feedLine(count: Int) {
        handler.addPrintable(Printable.prePrintFeedLine(count-1))
    }

    override fun addLine() {
        handler.addPrintable(Printable.prePrintDottedLine())
    }

    override fun getStatus(): Printer.Status {
        return handler.getStatus().toPrinterStatus
    }

    override fun print(): Printer.Status {
        return when (handler.printReceipt()) {
            PrinterResult.SUCCESS -> Printer.Status.OK
            PrinterResult.BUSY -> Printer.Status.BUSY
            PrinterResult.OUTOF_PAPER -> Printer.Status.OUT_OF_PAPER
            PrinterResult.HEAT_LIMITED -> Printer.Status.OVER_HEAT
            PrinterResult.PPSERR -> Printer.Status.PAPER_JAM
            else -> Printer.Status.ERROR
        }
    }
}


val Align.printableAlign: Printable.Align
    get() = Printable.Align.values()[ordinal]

val FontSize.printableFontSize: Printable.FontSize
    get() = Printable.FontSize.values()[ordinal]

val PrinterStatus.toPrinterStatus
    get() =  when (this) {
        PrinterStatus.NORMAL -> Printer.Status.OK
        PrinterStatus.BUSY -> Printer.Status.BUSY
        PrinterStatus.OUTOF_PAPER -> Printer.Status.OUT_OF_PAPER
        PrinterStatus.HEAT_LIMITED -> Printer.Status.OVER_HEAT
        PrinterStatus.PPSERR -> Printer.Status.PAPER_JAM
        else -> Printer.Status.ERROR
    }