package com.xpresspayments.newland910.printer

data class StringPrintable(val content: String, val align: Printable.Align = Printable.Align.LEFT): TextPrintable {

    override fun prePrint() =  if (content.isNotEmpty()) "*text $align $content\n" else ""
}