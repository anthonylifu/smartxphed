package com.xpresspayments.newland910.printer

class BarcodePrintable(val content: String, val align: Printable.Align = Printable.Align.LEFT, val height: Int =  64): Printable {

    override fun prePrint() = StringBuilder().apply {
        append("!barcode 2 $height\n")
        append("*barcode $align $content\n")
    }.toString()
}