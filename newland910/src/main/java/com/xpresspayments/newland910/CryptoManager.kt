package com.xpresspayments.newland910

import android.content.Context
import com.newland.mtype.module.common.pin.KekUsingType
import com.newland.mtype.module.common.pin.WorkingKeyType
import com.newland.mtype.util.ISOUtils
import com.xpresspayments.newland910.extensions.pinModule
import com.xpresspos.epmslib.utils.TripleDES
import java.lang.Exception
import java.util.*




object CryptoManager {

   private const val NEWLAND_TRANSPORT_KEY = "31313131313131313131313131313131"
    const val DEFAULT_MASTER_KEY_INDEX = 1
    const val DEFAULT_PIN_KEY_INDEX = 10
    const val DEFAULT_DUKPT_KEY_INDEX = 20


    fun getKCV(key: String) = TripleDES.encrypt("0000000000000000", key)

   fun loadMasterKey(context: Context, clearMasterKey: String, KCV: String? = null, keyIndex: Int = DEFAULT_MASTER_KEY_INDEX): Boolean {
        try {
            val encryptedMasterKey =  TripleDES.encrypt(clearMasterKey,
                NEWLAND_TRANSPORT_KEY
            )
            val theKcv =  KCV ?: getKCV(clearMasterKey)
            val result = context.pinModule.loadMainKey(KekUsingType.ENCRYPT_TMK, keyIndex,
                ISOUtils.hex2byte(encryptedMasterKey), ISOUtils.hex2byte(theKcv), -1)

            return (result != null && Arrays.equals(result, byteArrayOf(0x00, 0x00, 0x00, 0x00, 0x00, 0x00)))
        } catch (e: Exception) {
            e.printStackTrace()
        }

        return false
    }

    fun loadWorkingKey(context: Context, workingKeyType: WorkingKeyType,
                       encryptedWorkingKey: String,
                       kcv: String,
                       masterKeyIndex: Int = DEFAULT_MASTER_KEY_INDEX, keyIndex: Int = DEFAULT_PIN_KEY_INDEX
    ): Boolean {
        try {
            val result = context.pinModule.loadWorkingKey(
                workingKeyType,
                masterKeyIndex,
                keyIndex,
                ISOUtils.hex2byte(encryptedWorkingKey),
                ISOUtils.hex2byte(kcv)
            )
            return (result != null && Arrays.equals(result, byteArrayOf(0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00)))
        }  catch (e: Exception) {
            e.printStackTrace()
        }

        return false
    }

    fun loadPinKey(context: Context, encryptedPinKey: String, kcv: String,
                   masterKeyIndex: Int = DEFAULT_MASTER_KEY_INDEX,
                   keyIndex: Int = DEFAULT_PIN_KEY_INDEX
    ) = loadWorkingKey(
        context,
        WorkingKeyType.PININPUT,
        encryptedPinKey,
        kcv,
        masterKeyIndex,
        keyIndex
    )

}

