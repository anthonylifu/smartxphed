package com.xpresspayments.newland910

import android.app.Application
import android.content.Context
import android.os.Handler
import android.util.Log
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import com.newland.me.ConnUtils
import com.newland.me.DeviceManager
import com.newland.me.K21Driver
import com.newland.mtype.ConnectionCloseEvent
import com.newland.mtype.event.DeviceEventListener
import com.newland.mtypex.nseries3.NS3ConnParams
import com.newland.mtype.ModuleType
import com.xpresspayments.commons.Device
import com.xpresspayments.commons.DeviceConfigurator
import com.xpresspayments.commons.EmvProcessor
import com.xpresspayments.commons.SmartAppInterface
import com.xpresspayments.commons.printer.Printer
import com.xpresspayments.newland910.emv.NewlandCardProcessor
import com.xpresspayments.newland910.emv.NewlandConfigurationHandler
import com.xpresspayments.newland910.printer.N910Printer
import com.xpresspos.epmslib.entities.HostConfig
import java.lang.Exception


private lateinit var deviceManager: DeviceManager

open class NewlandApp: SmartAppInterface {
    init {
        deviceManager = ConnUtils.getDeviceManager()
    }

    override val device: Device
        get() = Device.NewlandN910


    override val terminalSerial: String
        get() = deviceManager.device.deviceInfo.sn


    val handler by lazy {
        Handler()
    }

    override val deviceConfigurator: DeviceConfigurator
        get() = NewlandConfigurationHandler

    override fun getEmvProcessor(
        activity: AppCompatActivity,
        hostConfig: HostConfig
    ): EmvProcessor = NewlandCardProcessor(activity, hostConfig)

    override fun initializeDevice(context: Context) {
        val connParams = NS3ConnParams()
        val deviceDriver = K21Driver()

        deviceManager.init(context, deviceDriver, connParams, object : DeviceEventListener<ConnectionCloseEvent> {
            override fun onEvent(event: ConnectionCloseEvent?, handler: Handler?) {
                event?.let { event ->
                    if (event.isFailed) {
                        handler?.let {
                            it.post {
                                Toast.makeText(context, R.string.initialization_failed, Toast.LENGTH_LONG).show()
                            }
                        } ?: kotlin.run {
                            Log.d(NewlandApp.TAG, "Failed to initialize device manager")
                        }
                    }
                }

            }

            override fun getUIHandler(): Handler = handler
        })

        try {
            deviceManager.connect()
            deviceManager.device.bundle = connParams
            Log.d(NewlandApp.TAG, "Device initialized")
        } catch (e: Exception) {
            e.printStackTrace();
            Toast.makeText(context, R.string.initialization_failed, Toast.LENGTH_LONG).show()
        }


    }

    override fun destruct(context: Context) {
        if (deviceManager.device.isAlive) {
            deviceManager.disconnect()
        }

    }

    override fun getPrinter(context: Context): Printer {
        return N910Printer(context)
    }

    companion object {
        const val TAG = "XpressPOS"
    }
}

//Context extensions
val Context.deviceManager
    get () = com.xpresspayments.newland910.deviceManager



fun <T> DeviceManager.getStandardModule(moduleType: ModuleType): T  = deviceManager.device.getStandardModule(moduleType) as T

fun <T> DeviceManager.getExtendedModule(moduleName: String): T = deviceManager.device.getExModule(moduleName) as T
