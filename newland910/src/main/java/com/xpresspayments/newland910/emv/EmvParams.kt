package com.xpresspayments.newland910.emv

import com.newland.mtype.util.ISOUtils

data class EmvConfigParams(
    var terminalId: String,
    var merchantId: String,
    var merchantNameAndLocation: String,
    var merchantCategoryCode: String = "5999",
    var countryCode: String = "0566",
    var currencyCode: String = "0566",
    var currencyExponent: Int = 1,
    var terminalCapabilities: String = "E0F8C8",
    var terminalAdditionalCapabilities: String = "FF80F0B001",
    var terminalType: Int = 0x22
) {

    val tlvPackage
        get() = ISOUtils.newTlvPackage().apply {
            append(com.xpresspayments.commons.EmvTag.TERMINAL_IDENTIFIER_9F1C, terminalId)
            append(com.xpresspayments.commons.EmvTag.MERCHANT_IDENTIFIER_9F16, merchantId)
            append(com.xpresspayments.commons.EmvTag.MERCHANT_NAME_AND_LOCATION_9F4E, merchantNameAndLocation)
            append(com.xpresspayments.commons.EmvTag.MERCHANT_CATEGORY_CODE_9F15, merchantCategoryCode)
            append(com.xpresspayments.commons.EmvTag.TERMINAL_COUNTRY_CODE_9F1A, countryCode)
            append(com.xpresspayments.commons.EmvTag.TRANSACTION_CURRENCY_CODE_5F2A, currencyCode)
            append(com.xpresspayments.commons.EmvTag.TRANSACTION_CURRENCY_EXPONENT_5F36, currencyExponent.toString())
            append(com.xpresspayments.commons.EmvTag.TERMINAL_CAPABILITIES_9F33, terminalCapabilities)
            append(com.xpresspayments.commons.EmvTag.TERMINAL_ADDITIONAL_CAPABILITIES_9F40, terminalAdditionalCapabilities)
            append(com.xpresspayments.commons.EmvTag.TERMINAL_TYPE_9F35, terminalType.toString(16))
        }

    fun dump() = tlvPackage.pack()

    override fun toString() = ISOUtils.hexString(dump()) ?: ""
}
