package com.xpresspayments.newland910.emv

import android.os.Handler
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.Lifecycle
import androidx.lifecycle.LifecycleObserver
import androidx.lifecycle.OnLifecycleEvent
import com.newland.mtype.DeviceRTException
import com.newland.mtype.ModuleType
import com.newland.mtype.common.*
import com.newland.mtype.module.common.cardreader.CommonCardType
import com.newland.mtype.module.common.cardreader.K21CardReaderEvent
import com.newland.mtype.module.common.cardreader.SearchCardRule
import com.xpresspayments.newland910.extensions.cardReaderModule
import com.xpresspayments.newland910.extensions.emvModule
import com.xpresspayments.newland910.utils.AbstractDeviceEventListener
import com.xpresspayments.commons.AbstractCoroutineScope
import com.xpresspayments.commons.EmvProcessor
import com.xpresspayments.commons.ProcessState
import com.xpresspayments.commons.displayMessageId
import com.xpresspos.epmslib.entities.TransactionRequestData
import com.xpresspayments.newland910.R
import com.xpresspayments.newland910.extensions.allowedNewlandModules
import com.xpresspos.epmslib.entities.HostConfig
import com.xpresspos.epmslib.entities.TransactionResponse
import com.xpresspos.epmslib.processors.TransactionProcessor
import io.reactivex.*
import kotlinx.coroutines.launch
import java.math.BigDecimal
import java.util.concurrent.TimeUnit
import kotlin.RuntimeException
import kotlin.coroutines.*


class NewlandCardProcessor(
    activity: AppCompatActivity,
    hostConfig: HostConfig
) : EmvProcessor(activity, hostConfig) {


    internal var actualReaderUsed: com.xpresspayments.commons.Reader? = null

    internal lateinit var requestData: TransactionRequestData

    private val defaultTimeout = 60L
    private val forceOnline = true
    private val isSupportSM = false

    init {
        activity.emvModule.initEmvModule(activity)
    }

    override fun startTransaction(requestData: TransactionRequestData, reader: com.xpresspayments.commons.Reader) {
        this.requestData = requestData
        launch {
            startCardEntry(reader)
        }

        setState(ProcessState(ProcessState.States.CARD_ENTRY, activity.getString(reader.displayMessageId())))
    }


    private fun startCardEntry(reader: com.xpresspayments.commons.Reader) {
        launch {
            val result = kotlin.runCatching {
                val event = getCardEvent(reader)
                val result = event.openCardReaderResult

                val openReaders = result.responseCardTypes
                if (openReaders.size != 1) {
                    throw IllegalStateException("Inconsistent reader state")
                }

                when (openReaders[0]) {
                    CommonCardType.ICCARD -> startEmvTransactionProcess(com.xpresspayments.commons.Reader.CT)
                    CommonCardType.RFCARD -> startEmvTransactionProcess(com.xpresspayments.commons.Reader.CTLS)

                    CommonCardType.MSCARD -> {
                        if (!result.isMSDDataCorrectly) {
                            throw RuntimeException("Card swipe failed")
                        }
                        startMsrProcessing()
                    }
                }
            }

            if (result.isFailure) {
                val e = result.exceptionOrNull()
                e?.printStackTrace()

//                if (e is DeviceRT)
                setError(e ?: RuntimeException("Error occurred"))
            }
        }

    }


    private fun startEmvTransactionProcess(reader: com.xpresspayments.commons.Reader) {
        setState(ProcessState(ProcessState.States.PROCESSING, activity.getString(R.string.processing)))
        val controller = activity.emvModule.getEmvTransController(EmvListener(this))
        actualReaderUsed = reader


        controller.startEmv(
            BigDecimal.valueOf((requestData.amount) / 100.0),
            BigDecimal.valueOf(requestData.otherAmount / 100.0),
            forceOnline
        )

    }

    private fun startMsrProcessing() {
        setState(ProcessState(ProcessState.States.PROCESSING, activity.getString(R.string.processing)))

        actualReaderUsed = com.xpresspayments.commons.Reader.MSR

        MagListener(this).startMagstripeTransaction()
    }

    private suspend fun getCardEvent(reader: com.xpresspayments.commons.Reader) =
        suspendCoroutine<K21CardReaderEvent> { cont ->
            val allowedModules = reader.allowedNewlandModules()

            activity.cardReaderModule.openCardReader(allowedModules,
                allowedModules.contains(ModuleType.COMMON_SWIPER),
                defaultTimeout, TimeUnit.SECONDS, object : AbstractDeviceEventListener<K21CardReaderEvent>() {
                    override fun onEvent(event: K21CardReaderEvent, handler: Handler?) {
                        if (!event.isSuccess) {
                            if (event.isUserCanceled) {
                                cont.resumeWithException(RuntimeException("Operation cancelled by user"))
                                return
                            }
                            event.exception?.let {
                                if (event.exception is RuntimeException) {// Throw  the Runtime Exception
                                    cont.resumeWithException(event.exception)
                                    return
                                }
                                cont.resumeWithException(
                                    DeviceRTException(
                                        -1,
                                        "Open card reader error!",
                                        event.exception
                                    )
                                )
                                return
                            }

                            cont.resumeWithException(DeviceRTException(ExCode.UNKNOWN, "Unknown exception"))
                            return
                        }

                        cont.resume(event) //Success
                    }
                }, SearchCardRule.RFCARD_FIRST
            )
        }

    override fun abortTransaction() {
        activity.cardReaderModule.cancelCardRead()
        setError(RuntimeException("Transaction cancelled"))
    }
}


