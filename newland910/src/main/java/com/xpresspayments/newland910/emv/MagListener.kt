package com.xpresspayments.newland910.emv

import android.util.Log
import com.newland.mtype.DeviceRTException
import com.newland.mtype.module.common.swiper.SwipResultType
import com.newland.mtype.module.common.swiper.SwiperReadModel
import com.xpresspayments.newland910.NewlandApp
import com.xpresspayments.newland910.NewlandApp.Companion.TAG
import com.xpresspayments.newland910.R
import com.xpresspayments.newland910.extensions.hexString
import com.xpresspayments.newland910.extensions.swiperModule
import com.xpresspayments.newland910.ui.PinEntryDialog
import com.xpresspayments.commons.runOnMain
import com.xpresspos.epmslib.entities.CardData
import com.xpresspos.epmslib.entities.getCardHolderFromTrack1
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlin.coroutines.CoroutineContext

internal class MagListener(private val handler: NewlandCardProcessor): com.xpresspayments.commons.AbstractCoroutineScope() {

    private val context = handler.activity

    override val coroutineContext: CoroutineContext
        get() = super.coroutineContext + Dispatchers.IO



    internal fun startMagstripeTransaction() {
        val swipRslt = context.swiperModule.readPlainResult(
                arrayOf(
                        SwiperReadModel.READ_FIRST_TRACK,
                        SwiperReadModel.READ_SECOND_TRACK,
                        SwiperReadModel.READ_THIRD_TRACK
                )
        )

        swipRslt?.let {
            if(it.rsltType == SwipResultType.SUCCESS) {
                val firstTrack =  String(it.firstTrackData ?: byteArrayOf(0))
                Log.d(NewlandApp.TAG, "Track1: $firstTrack")

                val secondTrack = String(it.secondTrackData).replace('=', 'D')
                Log.d(NewlandApp.TAG, "Track2: $secondTrack")

                val thirdTrack = String(it.thirdTrackData ?: byteArrayOf(0))
                Log.d(NewlandApp.TAG, "Track3: $thirdTrack")

                val cardHolderName = getCardHolderFromTrack1(firstTrack).trim()
                println("CardHolder: $cardHolderName")
                val cardData = CardData(track2Data = secondTrack,
                        nibssIccSubset = "", panSequenceNumber = "", posEntryMode = "901")

                runOnMain {
                    val pinDialog = PinEntryDialog.newInstance(cardData.pan, null, null, false,
                            (handler.requestData.amount + handler.requestData.otherAmount)).apply {
                        pinResult.subscribeOn(Schedulers.computation())
                                .observeOn(AndroidSchedulers.mainThread())
                                .subscribe { pin, throwable ->
                                    throwable?.let {
                                        it.printStackTrace()
                                        handler.setError(it)
                                        destroy()
                                    }

                                    pin?.let {
                                        cardData.pinBlock = it.hexString
                                        launch {
                                            onRequestOnline(cardData, cardHolderName)
                                        }
                                    }
                                }
                    }

                    pinDialog.show(context.supportFragmentManager, "Pin Dialog")
                }
            } else {
                throw DeviceRTException(swipRslt.rsltType.ordinal, it.rsltType.toString())
            }
        } ?: kotlin.run {
            throw RuntimeException("Swipe failed with unknown error")
        }
    }

    private fun onRequestOnline(cardData: CardData, cardHolderName: String) {
        val result = kotlin.runCatching {
            handler.processor.processTransaction(
                    context,
                    handler.requestData,
                    cardData
            )
        }

        if (result.isFailure) {
            Log.e(TAG, "Host request error")
            val throwable = result.exceptionOrNull() ?: Exception(context.getString(R.string.error_connecting_to_host))
            throwable.printStackTrace()
            handler.setError(throwable)
        }

        if (result.isSuccess) {
            val hostResponse = result.getOrNull()
            println(hostResponse)

            hostResponse?.let {
                it.cardHolder = cardHolderName
                it.cardLabel = "Magstripe"
                handler.setResponse(it)
            }
        }

        destroy()
    }


}