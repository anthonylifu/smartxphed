package com.xpresspayments.newland910.emv

import android.content.Context
import android.util.Log
import com.newland.mtype.util.ISOUtils
import com.xpresspayments.newland910.CryptoManager
import com.xpresspayments.newland910.NewlandApp.Companion.TAG
import com.xpresspayments.newland910.extensions.emvModule
import com.xpresspayments.newland910.extensions.toAidConfig
import com.xpresspayments.newland910.extensions.toCAPublicKey
import com.xpresspos.epmslib.entities.*
import com.xpresspayments.commons.DeviceConfigurator

object NewlandConfigurationHandler: DeviceConfigurator {

    override fun processKeys(context: Context, keyHolder: KeyHolder): Boolean {

        if (!CryptoManager.loadMasterKey(context, keyHolder.masterKey)) {
            println("Master key injection failed")
            return false
        }


        val kcv = CryptoManager.getKCV(keyHolder.clearPinKey)
        if (!CryptoManager.loadPinKey(context, keyHolder.pinKey,kcv)) {
            println("Pin key injection failed")
            return false
        }

        return true
    }



    override fun processConfigData(context: Context, terminalID: String, configData: ConfigData): Boolean {
        val emvConfigParams: EmvConfigParams = configData.run {
            EmvConfigParams(
                terminalID,
                cardAcceptorIdCode,
                merchantNameLocation,
                merchantCategoryCode,
                countryCode,
                currencyCode
            )
        }

        if (!EmvConfigurator.setTerminalParameters(context, emvConfigParams).blockingGet()) {
            throw RuntimeException("Terminal parameter loading failed")
        }

        return true
    }

    override fun processCAs(context: Context, caList: List<NibssCA>): Boolean {
        val emvModule = context.emvModule.apply {
            initEmvModule(context)
            clearAllCAPublicKey()
        }
        caList.forEach {
            println(it)
            val capkey =  it.toCAPublicKey()
            Log.d("Test", ISOUtils.hexString(capkey.rid))
            if (!emvModule.addCAPublicKey(capkey.rid, capkey)) {
                return false
            }
        }
        return true
    }


    override fun processAIDs(context: Context,aidList: List<NibssAID>): Boolean {
         val emvModule = context.emvModule.apply {
             initEmvModule(context)
             clearAllAID()
         }

        aidList.forEach {
            println(it)
            val aid = it.toAidConfig()
            Log.d(TAG, "AidConfig()")
            if (!emvModule.addAID(aid)) {
                return false
            }
        }

        return true
    }



}