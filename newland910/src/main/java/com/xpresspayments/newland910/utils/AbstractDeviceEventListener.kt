package com.xpresspayments.newland910.utils

import android.os.Handler
import com.newland.mtype.event.DeviceEvent
import com.newland.mtype.event.DeviceEventListener


abstract class AbstractDeviceEventListener<T: DeviceEvent?> : DeviceEventListener<T> {
    override fun getUIHandler(): Handler? = null



}