package com.xpresspayments.newland910.extensions

import android.content.Context
import com.newland.mtype.ExModuleType
import com.newland.mtype.ModuleType
import com.newland.mtype.module.common.buzzer.Buzzer
import com.newland.mtype.module.common.cardreader.K21CardReader
import com.newland.mtype.module.common.emv.EmvModule
import com.newland.mtype.module.common.light.IndicatorLight
import com.newland.mtype.module.common.pin.K21Pininput
import com.newland.mtype.module.common.printer.Printer
import com.newland.mtype.module.common.scanner.BarcodeScanner
import com.newland.mtype.module.common.scanner.BarcodeScannerManager
import com.newland.mtype.module.common.security.SecurityModule
import com.newland.mtype.module.common.swiper.K21Swiper
import com.xpresspayments.newland910.*
import java.lang.IllegalStateException

//Context extensions


val Context.emvModule
    get() =   deviceManager.getExtendedModule<EmvModule>(ExModuleType.EMVINNERLEVEL2)

val Context.swiperModule
    get() = deviceManager.getStandardModule<K21Swiper>(ModuleType.COMMON_SWIPER)

val Context.pinModule
    get() = deviceManager.getStandardModule<K21Pininput>(ModuleType.COMMON_PININPUT)

val Context.printerModule
    get() =  deviceManager.getStandardModule<Printer>(ModuleType.COMMON_PRINTER)

val Context.cardReaderModule
    get() =  deviceManager.getStandardModule<K21CardReader>(ModuleType.COMMON_CARDREADER)

val Context.buzzerModule
    get() =  deviceManager.getStandardModule<Buzzer>(ModuleType.COMMON_BUZZER)

val Context.lightIndicatorModule
    get() =  deviceManager.getStandardModule<IndicatorLight>(ModuleType.COMMON_INDICATOR_LIGHT)

val Context.barcodeModule: BarcodeScanner
    get() {
       val barcodeScannerManager =  deviceManager.getStandardModule<Buzzer>(ModuleType.COMMON_BUZZER) as BarcodeScannerManager
        return barcodeScannerManager.default
    }

val Context.securityModule: SecurityModule
    get() =  deviceManager.getStandardModule<SecurityModule>(ModuleType.COMMON_SECURITY)

