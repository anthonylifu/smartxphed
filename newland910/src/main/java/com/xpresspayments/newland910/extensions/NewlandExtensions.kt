package com.xpresspayments.newland910.extensions

import com.newland.mtype.ModuleType
import com.newland.mtype.module.common.emv.EmvModule
import com.newland.mtype.module.common.emv.EmvPinInputType
import com.newland.mtype.module.common.emv.EmvTransInfo
import com.newland.mtype.util.ISOUtils
import com.xpresspayments.newland910.R
import com.xpresspayments.commons.Reader
import com.xpresspos.epmslib.tlv.TLV

val EmvTransInfo.errorMessageId
    get() = when (errorcode) {
        -6 -> R.string.msg_trans_error_no_supported_app
        -11 -> R.string.msg_trans_error_offline_data_auth_failed
        -13 -> R.string.msg_trans_error_holder_auth_failed
        -18 -> R.string.msg_trans_error_card_locked
        -1531, -2116 -> R.string.msg_trans_error_card_expired
        -1532, -2115 -> R.string.msg_trans_error_card_not_effected
        -1822 -> R.string.msg_trans_error_ecash_nsf
        -1903 -> R.string.msg_error_ec_load_exceed_limit
        -1904, -1905 -> R.string.msg_trans_error_script_execut_error
        -1901 -> R.string.msg_trans_error_script_transfinite
        -2105 -> R.string.msg_trans_error_pre_money_limit
        -2120, -1441 -> R.string.msg_trans_error_pure_ecash_cant_online
        -2121 -> R.string.msg_trans_error_card_reject
        else -> R.string.msg_trans_failed
    }

val EmvTransInfo.resultMessageId
    get () =  when (executeRslt) {
        0, 1 -> R.string.msg_trans_acc
        2 -> R.string.msg_trans_reject
        3 -> R.string.msg_request_online
        -2105 -> R.string.msg_trans_exceed_limit
        else -> R.string.msg_trans_failed
    }


val String.hexByteArray
    get () = ISOUtils.hex2byte(this)

val ByteArray.hexString
    get() = ISOUtils.hexString(this)

fun EmvModule.ICCValue(tag: Int) = TLV.fromRawData(fetchEmvData(intArrayOf(tag)).hexByteArray, 0).value

val EmvTransInfo.isOfflinePin
    get() = when (emvPinInputType) {
        EmvPinInputType.OFFLINE, EmvPinInputType.OFFLINE_ONLY -> true
        else -> false
    }

fun Reader.allowedNewlandModules() = when (this) {
    Reader.CT -> arrayOf(ModuleType.COMMON_ICCARDREADER)
    Reader.CTLS -> arrayOf(ModuleType.COMMON_RFCARDREADER)
    Reader.MSR -> arrayOf(ModuleType.COMMON_SWIPER)
    Reader.CT_MSR -> arrayOf(ModuleType.COMMON_ICCARDREADER, ModuleType.COMMON_SWIPER)
    Reader.CT_CTLS, Reader.CT_CTLS_MANUAL -> arrayOf(
        ModuleType.COMMON_ICCARDREADER,
        ModuleType.COMMON_RFCARDREADER
    )
    Reader.CT_CTLS_MSR_MANUAL, Reader.CT_CTLS_MSR -> arrayOf(
        ModuleType.COMMON_SWIPER,
        ModuleType.COMMON_ICCARDREADER,
        ModuleType.COMMON_RFCARDREADER
    )
    else -> emptyArray()
}