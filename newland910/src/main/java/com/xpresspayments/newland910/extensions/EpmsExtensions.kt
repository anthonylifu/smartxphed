package com.xpresspayments.newland910.extensions

import com.newland.mtype.module.common.emv.AIDConfig
import com.newland.mtype.module.common.emv.CAPublicKey
import com.newland.mtype.util.ISOUtils
import com.xpresspos.epmslib.entities.NibssAID
import com.xpresspos.epmslib.entities.NibssCA


fun NibssCA.toCAPublicKey(): CAPublicKey  = CAPublicKey(
    keyIndex.toInt(16),
    hashAlgorithm.toInt(16),
    keyAlgorithm.toInt(16),
    modulus.hexByteArray,
    exponent.hexByteArray,
    hash.hexByteArray,
    ""
).apply {
    rid = ISOUtils.hex2byte(RID)
}

fun NibssAID.toAidConfig(): AIDConfig = AIDConfig().let {

    it.aid = AID.hexByteArray
    it.aidLength = it.aid.size
    it.appSelectIndicator = appSelectionPriority.toInt(16)

    if (!applicationVersion.isEmpty()) {
        it.appVersionNumberCard = ISOUtils.hex2byte(applicationVersion)
    }

    it.defaultDDOL = DDOL.hexByteArray
    it.ddolLength = it.defaultDDOL.size

    if (!maxTargetDomestic.isEmpty()) {
        it.maxTargetPercentageForBiasedRandomSelection = maxTargetDomestic.toInt()
    }

    it.tacDefault = tacDefault.hexByteArray
    it.tacDenial = tacDenial.hexByteArray
    it.tacOnLine = tacOnline.hexByteArray

    if (!targetPercentageDomestic.isEmpty()) {
        it.targetPercentageForRandomSelection = targetPercentageDomestic.toInt()
    }

    if (TDOL.isNotEmpty()) {
        it.tdol = TDOL.hexByteArray
        it.tdolLength = it.tdol.size
    }


    it.terminalCountryCode = "0566".hexByteArray
    it.transactionCurrencyCode = "0566"
    it.transactionCurrencyExp = "1"

    it.onLinePinCapability = 1
    it.coreConfigType = 2


it
}