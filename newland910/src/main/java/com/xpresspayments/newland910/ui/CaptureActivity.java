package com.xpresspayments.newland910.ui;

import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.newland.printer.FileUtil;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Process;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;

import com.nextbiometrics.devices.NBDevice;
import com.nextbiometrics.devices.NBDeviceScanFormatInfo;
import com.nextbiometrics.devices.NBDeviceScanResult;
import com.nextbiometrics.devices.NBDeviceScanStatus;
import com.nextbiometrics.devices.event.NBDeviceScanPreviewEvent;
import com.nextbiometrics.devices.event.NBDeviceScanPreviewListener;
import com.xpresspayments.newland910.R;
import com.xpresspayments.newland910.fingerprint.ScanDeviceInfo;

import java.nio.IntBuffer;

import static android.os.Process.THREAD_PRIORITY_FOREGROUND;

public class CaptureActivity extends AppCompatActivity implements View.OnClickListener {

    private TextView log;
    private ImageView fingerImage;
    private Button scanSnapshotBtn;
    private Button              scanBtn;
    private Button              statusBtn;
    private ScanTask            scanTask;

    private NBDevice device;
    private TextView            device_time;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_capture);

        scanSnapshotBtn = (Button) findViewById(R.id.btnScanShot);
        scanBtn = (Button) findViewById(R.id.btnScan);
        statusBtn = (Button) findViewById(R.id.btnStatus);

        fingerImage = (ImageView) findViewById(R.id.imageView);
        log = (TextView) findViewById(R.id.messageTextView);

        device_time = (TextView) findViewById(R.id.messageTextView);
        scanSnapshotBtn.setOnClickListener(this);
        scanBtn.setOnClickListener(this);
        statusBtn.setOnClickListener(this);
    }

    @Override
    protected void onResume() {
        super.onResume();
        setDevice(getDevice());
    }

    @Override
    protected void onStop() {
        super.onStop();
        if (device != null && device.isScanRunning())
            device.cancelScan();
        if (scanTask != null) {
            scanTask.cancel(true);
        }
        setDevice(null);
    }

    @Override
    public void onClick(View v) {
        int id = v.getId();
        if (device != null) {
            if (id == R.id.btnScanShot) {
                startCapture(ScanType.SNAPSHOT);
            } else if (id == R.id.btnScan) {
                startCapture(ScanType.ONE_FINGERPRINT);
            } else if (id == R.id.btnStatus) {
                getStatus();
            }
        }
    }

    private void setDevice(NBDevice device) {
        if (device == null && this.device != null) {
            this.device.dispose();
            this.device = null;
        } else {
            this.device = device;
        }
        this.device = device;
        enableButtons(device != null);
        log.setText(device != null ? getString(R.string.press_scan) : getString(R.string.device_not_connected));
        fingerImage.setImageResource(R.drawable.finger_locked);
    }

    private NBDevice getDevice() {
        Intent intent = this.getIntent();
        Bundle bundle = intent.getExtras();
        ScanDeviceInfo deviceInfo = (ScanDeviceInfo)bundle.getSerializable("value");
        return ScanDeviceInfo.getDevice(deviceInfo);
    }

    private void enableButtons(boolean en) {
        scanBtn.setEnabled(en);
        scanSnapshotBtn.setEnabled(en);
        statusBtn.setEnabled(en);
    }

    private void startCapture(ScanType scanType) {
        scanTask = new ScanTask(scanType);
        scanTask.execute(device.getSupportedScanFormats()[0]);
    }

    private static Bitmap convertToBitmap(NBDeviceScanFormatInfo formatInfo, byte[] image){
        IntBuffer buf = IntBuffer.allocate(image.length);
        for (byte pixel : image) {
            int grey = pixel & 0x0ff;
            buf.put(Color.argb(255, grey, grey, grey));
        }
        return Bitmap.createBitmap(buf.array(), formatInfo.getWidth(), formatInfo.getHeight(), Bitmap.Config.ARGB_8888);
    }

    private void cancelCapture() {
        new AsyncTask<Void, Void, String>() {

            @Override
            protected void onPreExecute() {
                enableButtons(false);
            }

            @Override
            protected String doInBackground(Void... params) {
                Process.setThreadPriority(THREAD_PRIORITY_FOREGROUND);
                try {
                    device.cancelScan();
                    return null;
                }
                catch (RuntimeException e) {
                    e.printStackTrace();
                    return e.getMessage();
                }
            }

            @Override
            protected void onPostExecute(String msg) {
                enableButtons(true);
                if (msg != null && !"".equals(msg)) {
                    log.setText(msg);
                }
            }
        }.execute();
    }

    private void getStatus() {
        new AsyncTask<Void, Void, String>() {

            @Override
            protected void onPreExecute() {
                enableButtons(false);
            }

            @Override
            protected String doInBackground(Void... params) {
                Process.setThreadPriority(THREAD_PRIORITY_FOREGROUND);
                try {
                    return device.getState().toString();
                }
                catch (RuntimeException e) {
                    e.printStackTrace();
                    return e.getMessage();
                }
            }

            @Override
            protected void onPostExecute(String msg) {
                enableButtons(true);
                log.setText(getString(R.string.device_state));
                log.append(" ");
                log.append(msg);
            }
        }.execute();
    }

    private enum ScanType {
        SNAPSHOT,
        ONE_FINGERPRINT
    }

    private class ScanProgress {
        private String message;
        private Bitmap image;

        ScanProgress(NBDeviceScanResult result, int fingerprintDetectValue) {
            this(result.getStatus(), fingerprintDetectValue, result.getFormat(), result.getImage());
        }

        ScanProgress(NBDeviceScanPreviewEvent eventDetails) {
            this(eventDetails.getStatus(), eventDetails.getFingerDetectValue(), eventDetails.getFormat(), eventDetails.getImage());
        }

        ScanProgress(NBDeviceScanStatus status, int fingerprintDetectValue, NBDeviceScanFormatInfo formatInfo, byte[] image) {
            this(String.format("%s %s, %s %d", getString(R.string.scan_status), status, getString(R.string.finger_detect_value), fingerprintDetectValue)
                    , convertToBitmap(formatInfo, image));
        }

        ScanProgress(String message, Bitmap image) {
            this.message = message;
            this.image = image;
        }

        ScanProgress(String message) {
            this(message, null);
        }

        String getMessage() {
            return message;
        }

        Bitmap getImage() {
            return image;
        }
    }

    private class ScanTask extends AsyncTask<NBDeviceScanFormatInfo, ScanProgress, ScanProgress> implements NBDeviceScanPreviewListener {
        private ScanType scanType;

        long start ;
        ScanTask(ScanType scanType) {
            this.scanType = scanType;
        }

        @Override
        protected void onPreExecute() {
            enableButtons(false);
            log.setText(R.string.scan_in_progress);
            fingerImage.setImageResource(R.drawable.finger_locked);
            device_time.setText("");
        }

        @Override
        protected ScanProgress doInBackground(NBDeviceScanFormatInfo... params) {
            Process.setThreadPriority(THREAD_PRIORITY_FOREGROUND);
            start = System.currentTimeMillis();
            try {
                NBDeviceScanFormatInfo format = params[0];
                if (scanType == ScanType.SNAPSHOT){
                    return new ScanProgress(device.scan(format), device.getFingerDetectValue());
                }

                return new ScanProgress(device.scanEx(format, 10000, this), device.getFingerDetectValue());
            }
            catch (Throwable e) {
                e.printStackTrace();
                publishProgress(new ScanProgress(String.format("%s %s", getString(R.string.scan_failed), e.getMessage())));
                return null;
            }
        }

        @Override
        protected void onProgressUpdate(ScanProgress... msg) {
            ScanProgress progress = msg[0];
            long end = System.currentTimeMillis();
            device_time.append("\n"+"scan time：" + (end - start) + "\n");
            start = end;

            updateView(progress);
        }

        @Override
        protected void onPostExecute(ScanProgress fp) {
            if (scanType == ScanType.SNAPSHOT){
                long end = System.currentTimeMillis();
                device_time.setText("\n"+"snapshot time：" + (end - start) + "\n");
            }
            enableButtons(true);
            if (fp != null) {
                updateView(fp);
            }
            else {
                fingerImage.setImageResource(R.drawable.fingerprint_scan_failed);
            }
        }

        @Override
        public void preview(NBDeviceScanPreviewEvent event) {
            publishProgress(new ScanProgress(event));
        }

        private void updateView(ScanProgress progress) {
            fingerImage.setImageBitmap(progress.getImage());
            log.setText(progress.getMessage());
        }
    }
}
