package com.xpresspayments.newland910.ui

import android.app.ProgressDialog
import android.content.DialogInterface
import android.os.Bundle
import android.os.Environment
import android.os.Handler
import android.os.Process
import android.view.Menu
import android.view.MenuItem
import android.view.View
import android.widget.EditText
import android.widget.Toast
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.app.AppCompatActivity
import com.developer.filepicker.model.DialogConfigs
import com.developer.filepicker.model.DialogProperties
import com.developer.filepicker.view.FilePickerDialog
import com.nextbiometrics.biometrics.*
import com.nextbiometrics.biometrics.event.NBBiometricsScanPreviewEvent
import com.nextbiometrics.biometrics.event.NBBiometricsScanPreviewListener
import com.nextbiometrics.devices.*
import com.nextbiometrics.devices.event.NBDeviceScanPreviewEvent
import com.nextbiometrics.devices.event.NBDeviceScanPreviewListener
import com.nextbiometrics.devices.event.NBDevicesDeviceChangedEvent
import com.nextbiometrics.devices.event.NBDevicesDeviceChangedListener
import com.snatik.storage.Storage
import com.xpresspayments.commons.runOnBackground
import com.xpresspayments.commons.runOnMain
import com.xpresspayments.newland910.BuildConfig
import com.xpresspayments.newland910.R
import com.xpresspayments.newland910.fingerprint.*
import kotlinx.android.synthetic.main.activity_fingerprint.*
import kotlinx.android.synthetic.main.content_fingerprint.*
import java.io.*
import java.text.SimpleDateFormat
import java.util.*
import kotlin.concurrent.thread


class FingerprintActivity : AppCompatActivity() {

    private val handler = Handler()
    private var menu: Menu? = null

    private val DEFAULT_SPI_NAME = "/dev/spidev0.0"

    private val basePath =
        Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOWNLOADS).path + "/NBImages/"



    // For synchronization with DialogInterface
    private var dialogResult = false
    private val lock = Object()

    private val previewListener by lazy {
        PreviewListener()
    }

    private val storage by lazy {
        Storage(applicationContext)
    }

    private var isSdkInitialized = false

    private var nbDevice: NBDevice? = null
    private var nbContext: NBBiometricsContext? = null

    private val testTemplateType = NBBiometricsTemplateType.ISO
    private var securityLevel = NBBiometricsSecurityLevel.NORMAL


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_fingerprint)
        setSupportActionBar(toolbar)

        initialize()

        FingerprintRecordMgr.loadRecords(this)

        btnExtract.setOnClickListener {
            nbDevice?.run {
                extractFingerprint(this, testTemplateType)
            }
        }

        btnVerify.setOnClickListener {
            listRecords {
                prepareVerify(it.first())
            }
        }

        btnIdentify.setOnClickListener {
            listRecords( {
                prepareIdentify(it)
            }, true)
        }

        handler.postDelayed({
            if (!isSdkInitialized) {
                showErrorStatus("SDK could not be initialized.")
            }
        }, 15000)
    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        menuInflater.inflate(R.menu.fingerprint_menu, menu)
        this.menu = menu
        return super.onCreateOptionsMenu(menu)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            R.id.actionCapture -> {
                kotlin.runCatching {
                    nbDevice?.run {
                        scanFingerprint(this)
                    }
                }.onFailure {
                    showErrorStatus(it.localizedMessage)
                }
            }

            R.id.actionCancelScan -> {
                nbDevice?.run {
                    previewListener.reset()
                    if (isScanRunning) {
                        cancelScan()
                    }
                }
            }

            R.id.actionDeviceInfo -> {
                showDeviceInfo()
            }

            R.id.actionSecurityLevel -> {
                setSecurityLevel()
            }

            R.id.actionRecords -> {
                listRecords {  }
            }

            R.id.actionQuit -> {
                finish()
            }

            R.id.actionConvert -> {
//                loadFingerprintFile {
//                    nbDevice?.run {
//                        toeConverter(this, it)
//                    }
//                }

                listRecords({
                    convertTemplate(it)
                }, true)
            }
        }
        return super.onOptionsItemSelected(item)
    }


    override fun onPause() {
        super.onPause()
        FingerprintRecordMgr.saveRecords(this)
    }

    override fun onBackPressed() {
        //super.onBackPressed()
    }

    override fun onDestroy() {
        super.onDestroy()
        destruct()
    }

    private fun initialize() {
        thread {
            enableFingerprintModuleUsbHost()
            Process.setThreadPriority(Process.THREAD_PRIORITY_AUDIO)
            NBDevices.initialize(applicationContext, nbChangeListener)
        }
    }

    private fun openDeviceSession(device: NBDevice) {
        if (!device.isSessionOpen) {
            val cakId = "DefaultCAKKey1\u0000".toByteArray()
            val cak = byteArrayOf(
                0x05.toByte(),
                0x4B.toByte(),
                0x38.toByte(),
                0x3A.toByte(),
                0xCF.toByte(),
                0x5B.toByte(),
                0xB8.toByte(),
                0x01.toByte(),
                0xDC.toByte(),
                0xBB.toByte(),
                0x85.toByte(),
                0xB4.toByte(),
                0x47.toByte(),
                0xFF.toByte(),
                0xF0.toByte(),
                0x79.toByte(),
                0x77.toByte(),
                0x90.toByte(),
                0x90.toByte(),
                0x81.toByte(),
                0x51.toByte(),
                0x42.toByte(),
                0xC1.toByte(),
                0xBF.toByte(),
                0xF6.toByte(),
                0xD1.toByte(),
                0x66.toByte(),
                0x65.toByte(),
                0x0A.toByte(),
                0x66.toByte(),
                0x34.toByte(),
                0x11.toByte()
            )
            val cdkId = "Application Lock\u0000".toByteArray()
            val cdk = byteArrayOf(
                0x6B.toByte(),
                0xC5.toByte(),
                0x51.toByte(),
                0xD1.toByte(),
                0x12.toByte(),
                0xF7.toByte(),
                0xE3.toByte(),
                0x42.toByte(),
                0xBD.toByte(),
                0xDC.toByte(),
                0xFB.toByte(),
                0x5D.toByte(),
                0x79.toByte(),
                0x4E.toByte(),
                0x5A.toByte(),
                0xD6.toByte(),
                0x54.toByte(),
                0xD1.toByte(),
                0xC9.toByte(),
                0x90.toByte(),
                0x28.toByte(),
                0x05.toByte(),
                0xCF.toByte(),
                0x5E.toByte(),
                0x4C.toByte(),
                0x83.toByte(),
                0x63.toByte(),
                0xFB.toByte(),
                0xC2.toByte(),
                0x3C.toByte(),
                0xF6.toByte(),
                0xAB.toByte()
            )
            val defaultAuthKey1Id = "AUTH1\u0000".toByteArray()
            val defaultAuthKey1 = byteArrayOf(
                0xDA.toByte(),
                0x2E.toByte(),
                0x35.toByte(),
                0xB6.toByte(),
                0xCB.toByte(),
                0x96.toByte(),
                0x2B.toByte(),
                0x5F.toByte(),
                0x9F.toByte(),
                0x34.toByte(),
                0x1F.toByte(),
                0xD1.toByte(),
                0x47.toByte(),
                0x41.toByte(),
                0xA0.toByte(),
                0x4D.toByte(),
                0xA4.toByte(),
                0x09.toByte(),
                0xCE.toByte(),
                0xE8.toByte(),
                0x35.toByte(),
                0x48.toByte(),
                0x3C.toByte(),
                0x60.toByte(),
                0xFB.toByte(),
                0x13.toByte(),
                0x91.toByte(),
                0xE0.toByte(),
                0x9E.toByte(),
                0x95.toByte(),
                0xB2.toByte(),
                0x7F.toByte()
            )
            val security = NBDeviceSecurityModel.get(device.capabilities.securityModel.toInt())

            if (security == NBDeviceSecurityModel.Model65200CakOnly) {
                device.openSession(cakId, cak)
            } else if (security == NBDeviceSecurityModel.Model65200CakCdk) {
                try {
                    device.openSession(cdkId, cdk)
                    device.SetBlobParameter(NBDevice.BLOB_PARAMETER_SET_CDK, null)
                    device.closeSession()
                } catch (ex: RuntimeException) {
                }
                device.openSession(cakId, cak)
                device.SetBlobParameter(NBDevice.BLOB_PARAMETER_SET_CDK, cdk)
                device.closeSession()
                device.openSession(cdkId, cdk)
            } else if (security == NBDeviceSecurityModel.Model65100) {
                device.openSession(defaultAuthKey1Id, defaultAuthKey1)
            }
        }
        // If the device requires external calibration data (NB-65210-S), load or create them
        if (device.capabilities.requiresExternalCalibrationData) {
            showToast("Device requires calibration")
            solveCalibrationData(device)
        }

    }

    private fun startUpDevice(device: NBDevice) {
        nbContext = NBBiometricsContext(device)

        showToast("SDK Initialized")
        isSdkInitialized = true

        runOnMain {
            menu?.findItem(R.id.actionCapture)?.isEnabled = true
            menu?.findItem(R.id.actionDeviceInfo)?.isEnabled = true
            menu?.findItem(R.id.actionCancelScan)?.isEnabled = true
            menu?.findItem(R.id.actionConvert)?.isEnabled = true
            menu?.findItem(R.id.actionSecurityLevel)?.isEnabled = true
            actionLayout.visibility = View.VISIBLE
        }
    }

    private fun scanFingerprint(
        device: NBDevice,
        scanFormatInfo: NBDeviceScanFormatInfo = device.supportedScanFormats.first(),
        timeout: Int = NBDevice.SCAN_TIMEOUT_INFINITE
    ) {
        thread {
            previewListener.reset()
            val result: NBDeviceScanResult =
                device.scanEx(scanFormatInfo, timeout, previewListener)

            if (result.status == NBDeviceScanStatus.OK) {

                val wsqImage =  device.ConvertImage(result.image, result.format.width, result.format.height, result.format.horizontalResolution,
                    NBDeviceEncodeFormat.WSQ, 3F, NBDeviceFingerPosition.Unknown, 0)


                runOnMain {
                    val dialog = AlertDialog.Builder(this)
                        .setTitle("Save Capture")
                        .setView( View.inflate(this, R.layout.layout_input_text, null))
                        .setCancelable(false)
                        .setNegativeButton(android.R.string.cancel, null)
                        .setPositiveButton(android.R.string.ok) { dialog, which ->
                            val fileName = (dialog as AlertDialog).findViewById<EditText>(R.id.inputText)?.text?.toString()

                            if (fileName.isNullOrBlank()) {
                                (dialog as AlertDialog).show()
                                return@setPositiveButton
                            }

                            val filePath = "${fileName}.wsq"
                            saveFingerFile(filePath, wsqImage)
                        }

                    dialog.show()
                }

                showSuccessStatus("Fingerprint captured")
                showFingerPrintImage(result.format, result.image)
            } else {
                showErrorStatus(result.status.descriptiveMessage())
            }
        }
    }

    private fun extractFingerprint(
        device: NBDevice,
        templateType: NBBiometricsTemplateType = NBBiometricsTemplateType.ISO,
        format: NBDeviceScanFormatInfo = device.supportedScanFormats.first(),
        fingerPosition: NBBiometricsFingerPosition = NBBiometricsFingerPosition.UNKNOWN
    ) {

        showPutFinger()

        thread {
            previewListener.reset()
            Process.setThreadPriority(Process.THREAD_PRIORITY_AUDIO)
            nbContext!!.runCatching {
                val result = extract(
                    templateType,
                    fingerPosition,
                    format,
                    previewListener
                )

                if (result.status != NBBiometricsStatus.OK) {
                    throw Exception(result.descriptiveMessage())
                }

                val outData =  saveTemplate(result.template)
                storeFingerprint(outData, format.format, templateType)
                showSuccessStatus(
                    "Fingerprint extracted.\nQuality: ${result.template.quality}." +
                            "\nPosition: ${result.template.position.descriptiveMessage()}" +
                            "\nTemplate: ${getNBTemplateName(result.template.type.value)}"
                )

            }.onFailure {
                showErrorStatus(it.localizedMessage)
            }
        }
    }

    private fun convertFingerprintTemplate(
        raw: ByteArray,
        sourceTemplateType: NBBiometricsTemplateType,
        destinationTemplate: NBBiometricsTemplateType
    ) {
        thread {
            previewListener.cancel()
            Process.setThreadPriority(Process.THREAD_PRIORITY_AUDIO)

            nbContext!!.runCatching {
                val sourceTemplate = loadTemplate(sourceTemplateType, raw)
                val template = convertTemplate(sourceTemplate, destinationTemplate)

                saveFingerFile(
                    "conversion_result.raw", template.data)

                showSuccessStatus(
                    "Fingerprint converted.\nQuality: ${template.quality}." +
                            "\nPosition: ${template.position.descriptiveMessage()}" +
                            "\nTemplate: ${getNBTemplateName(template.type.value)}"
                )

            }.onFailure {
                showErrorStatus(it.localizedMessage)
            }
        }
    }

    private fun convertTemplate(records: Array<FingerprintRecord>) {
        records.forEach {
            runOnBackground {
                kotlin.runCatching {
                    val data = storage.readFile(it.filePath)
                    if (data.isNotEmpty()) {
                        runOnBackground {
                            runCatching {
                                convertFingerprintTemplate( data, NBBiometricsTemplateType.get(it.templateIndex), NBBiometricsTemplateType.ISO_COMPACT_CARD)
                            }.onFailure {
                                showErrorStatus(it.localizedMessage)
                            }
                        }
                    }
                }.onFailure {
                    showErrorStatus(it.localizedMessage)
                }
            }
        }
    }

    private fun prepareIdentify(records: Array<FingerprintRecord>) = runOnBackground {
        var progressDialog: ProgressDialog? = null
        runOnMain {
            progressDialog = ProgressDialog.show(this, null, "Preparing saved templates", true, false)
        }

        kotlin.runCatching {
            val templateType = NBBiometricsTemplateType.get(records.first().templateIndex)
            val scanFormatInfo =  nbDevice!!.getScanFormatInfo(NBDeviceScanFormat.get(records.first().scanFormatIndex))

            val templates: List<AbstractMap.SimpleEntry<Any, NBBiometricsTemplate>> = records.map {
                val record = it
                showInfoStatus("Reading template for ${record.tag}")
                if (!storage.isFileExist(record.filePath)) throw Exception("File not found at ${record.filePath}")
                val data = storage.readFile(record.filePath)
                val template = nbContext!!.loadTemplate(templateType, data)

                AbstractMap.SimpleEntry(
                    record.tag as Any,
                    template
                )
            }

            runOnMain {
                progressDialog?.dismiss()
            }

            identifyFingerprint(
                scanFormatInfo,
                templates,
                templateType
            )

        }.onFailure {
            showErrorStatus(it.localizedMessage)
        }

    }

    private fun identifyFingerprint(
        scanFormatInfo: NBDeviceScanFormatInfo,
        templates: List<AbstractMap.SimpleEntry<Any, NBBiometricsTemplate>>,
        templateType: NBBiometricsTemplateType
    ) {
        thread {
            previewListener.reset()
            Process.setThreadPriority(Process.THREAD_PRIORITY_AUDIO)
            nbContext!!.runCatching {
                // add more templates

                // add more templates
                showPutFinger("Identifying fingerprint, please put your fingerprint on sensor!")
                previewListener.reset()

                val identifyResult: NBBiometricsIdentifyResult = identify(
                    templateType,
                    NBBiometricsFingerPosition.UNKNOWN,
                    scanFormatInfo,
                    previewListener,
                    templates.iterator(),
                    securityLevel
                )
                if (identifyResult.status != NBBiometricsStatus.OK) throw java.lang.Exception("Not identified, reason: " + identifyResult.status.descriptiveMessage())
                showSuccessStatus("Identified successfully with fingerprint: " + identifyResult.templateId)
            }.onFailure {
                showErrorStatus(it.localizedMessage)
            }
        }
    }

    private fun prepareVerify(record: FingerprintRecord) {
        kotlin.runCatching {
            showInfoStatus("Reading template for ${record.tag}")
            if (!storage.isFileExist(record.filePath)) throw Exception("File not found at ${record.filePath}")
            val data = storage.readFile(record.filePath)
            val templateType = NBBiometricsTemplateType.get(record.templateIndex)
            val scanFormatInfo =  nbDevice!!.getScanFormatInfo(NBDeviceScanFormat.get(record.scanFormatIndex))
            val template = nbContext!!.loadTemplate(templateType, data)
            verifyFingerprint(scanFormatInfo, template, templateType)

        }.onFailure {
            showErrorStatus(it.localizedMessage)
        }
    }

    private fun verifyFingerprint(
        scanFormatInfo: NBDeviceScanFormatInfo,
        template: NBBiometricsTemplate,
        templateType: NBBiometricsTemplateType = template.type
    ) {
        thread {
            previewListener.reset()
            Process.setThreadPriority(Process.THREAD_PRIORITY_AUDIO)
            nbContext!!.runCatching {

                showPutFinger("Verifying fingerprint, please put your fingerprint on sensor!")

                previewListener.reset()
                val verifyResult = verify(
                    templateType, NBBiometricsFingerPosition.UNKNOWN,
                    scanFormatInfo, previewListener, template, securityLevel
                )

                if (verifyResult.status != NBBiometricsStatus.OK) throw java.lang.Exception("Not verified, reason: " + verifyResult.status.descriptiveMessage())

                showSuccessStatus("Verified successfully with fingerprint. Score: " + verifyResult.score)
            }.onFailure {
                showErrorStatus(it.localizedMessage)
            }
        }
    }


    private fun showDeviceInfo() {
        val supportedFormatString =
            "\nSupported Scan Formats: \n" + nbDevice?.supportedScanFormats?.joinToString("\n") {
                it.toString()
            }

        val supportedTemplateStr =
            "\nSupported Templates: \n" + nbContext?.supportedTemplateTypes?.joinToString("\n") {
                getNBTemplateName(it.templateType.value)
            }

        val deviceInfo = "Type: ${nbDevice?.type}\n" +
                "Model: ${nbDevice?.model}\n" +
                "Serial Number: ${nbDevice?.serialNumber}\n" +
                "Module Serial Number: ${nbDevice?.moduleSerialNumber}"

        val info =
            "Device Info: $deviceInfo\nFirmware Version: ${nbDevice?.firmwareVersion}$supportedTemplateStr$supportedFormatString"

        runOnMain {
            AlertDialog.Builder(this)
                .setTitle("Device Info")
                .setMessage(info)
                .setPositiveButton(android.R.string.ok, null)
                .show()
        }
    }

    private fun destruct() {
        disableFingerprintModuleUsbHost()
        Process.killProcess(Process.myPid())
    }

    private fun showPutFinger(message: String = getString(R.string.place_finger)) = runOnMain {
        messageTextView.text = message
        imageView.setImageResource(R.drawable.put_finger)
    }

    private fun showNbStatus(
        status: NBBiometricsStatus,
        message: String = status.descriptiveMessage()
    ) = runOnMain {
        imageView.setImageResource(
            when (status) {
                NBBiometricsStatus.OK -> R.drawable.fingerprint_ok
                NBBiometricsStatus.CANCELED, NBBiometricsStatus.TIMEOUT -> R.drawable.ic_cancel_black_24dp
                NBBiometricsStatus.NEED_MORE_SAMPLES -> R.drawable.ic_info_outline_black_24dp
                else -> R.drawable.ic_error_black_24dp
            }
        )

        messageTextView.text = message
    }

    private fun showNbStatus(
        status: NBDeviceScanStatus,
        message: String = status.descriptiveMessage()
    ) = runOnMain {
        imageView.setImageResource(
            when (status) {
                NBDeviceScanStatus.OK, NBDeviceScanStatus.DONE -> R.drawable.fingerprint_ok
                NBDeviceScanStatus.CANCELED, NBDeviceScanStatus.TIMEOUT -> R.drawable.ic_cancel_black_24dp
                NBDeviceScanStatus.LIFT_FINGER -> R.drawable.raise_finger
                NBDeviceScanStatus.PUT_FINGER_ON_SENSOR -> R.drawable.put_finger
                NBDeviceScanStatus.KEEP_FINGER_ON_SENSOR -> R.drawable.press_finger
                NBDeviceScanStatus.WAIT_FOR_DATA_PROCESSING, NBDeviceScanStatus.WAIT_FOR_SENSOR_INITIALIZATION -> R.drawable.ic_info_outline_black_24dp
                else -> R.drawable.ic_error_black_24dp
            }
        )

        messageTextView.text = message
    }

    private fun showErrorStatus(message: String) = runOnMain {
        imageView.setImageResource(R.drawable.ic_error_black_24dp)
        messageTextView.text = message
    }

    private fun showInfoStatus(message: String) = runOnMain {
        imageView.setImageResource(R.drawable.ic_info_outline_black_24dp)
        messageTextView.text = message
    }

    private fun showSuccessStatus(message: String) = runOnMain {
        imageView.setImageResource(R.drawable.fingerprint_ok)
        messageTextView.text = message
    }

    private fun showToast(message: String) = runOnMain {
        Toast.makeText(this, message, Toast.LENGTH_SHORT).show()
    }


    private val nbChangeListener = object : NBDevicesDeviceChangedListener {
        override fun added(event: NBDevicesDeviceChangedEvent) {
//            showToast("Device added")
            nbDevice = event.device

            openDeviceSession(nbDevice!!)

            if (nbDevice!!.supportedScanFormats.isEmpty()) {
                showErrorStatus("No supported formats")
                return
            }

            startUpDevice(event.device)
        }

        override fun removed(event: NBDevicesDeviceChangedEvent) {
            showToast("Device removed")
            nbDevice?.run {
                if (isScanRunning) {
                    cancelScan()
                }

                if (!isDisposed) {
                    dispose()
                }
            }

            nbDevice = null

            if (NBDevices.isInitialized()) {
                nbContext?.run {
                    if (isOperationRunning) {
                        cancelOperation()
                    }

                    if (!isDisposed) {
                        dispose()
                    }
                }

                if (NBDevices.isInitialized()) {
                    NBDevices.terminate()
                }
            }
        }
    }

    private fun showFingerPrintImage(format: NBDeviceScanFormatInfo, image: ByteArray) {
        runOnMain {
            kotlin.runCatching {
                imageView.setImageBitmap(convertNBToBitmap(format, image))
            }.onFailure {
                messageTextView.text = it.localizedMessage ?: it.message ?: "Error displaying image"
            }
        }
    }

    private fun storeFingerprint(data: ByteArray, scanFormatInfo: NBDeviceScanFormat, templateType: NBBiometricsTemplateType, fileExt: String = ".dat") = runOnMain {
       val dialog = AlertDialog.Builder(this)
            .setTitle("Save Capture")
            .setView( View.inflate(this, R.layout.layout_input_text, null))
           .setCancelable(false)
            .setNegativeButton(android.R.string.cancel, null)
            .setPositiveButton(android.R.string.ok) { dialog, which ->
                val fileName = (dialog as AlertDialog).findViewById<EditText>(R.id.inputText)?.text?.toString()

                if (fileName.isNullOrBlank()) {
                    (dialog as AlertDialog).show()
                    return@setPositiveButton
                }

                val filePath = "${fileName}$fileExt"
                FingerprintRecordMgr.store(FingerprintRecord(fileName, "$basePath$filePath", scanFormatInfo.ordinal, templateType.ordinal))
                saveFingerFile(filePath, data)
                FingerprintRecordMgr.saveRecords(this)
            }

        dialog.show()
    }
    private fun saveFingerFile(
        filename: String, image: ByteArray
    )  = runOnBackground{
        val pathRaw = "$basePath$filename"

        if (!storage.isDirectoryExists(basePath)) {
            storage.createDirectory(basePath)
        }

        if (storage.createFile(pathRaw, image)) {
            showToast("$pathRaw saved")
        } else {
            showToast("File not saved")
        }
    }

    @Throws(java.lang.Exception::class)
    fun solveCalibrationData(device: NBDevice) {
        val paths =
            Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOWNLOADS).toString() + "/NBData/" + device.serialNumber + "_calblob.bin"
        File(Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOWNLOADS).toString() + "/NBData/")
            .mkdirs()
        val file = File(paths)
        if (!file.exists()) { // Ask the user whether he wants to calibrate the device
            runOnUiThread(Runnable {
                val dialogClickListener =
                    DialogInterface.OnClickListener { dialog, which ->
                        synchronized(lock) {
                            dialogResult =
                                which == DialogInterface.BUTTON_POSITIVE
                            lock.notifyAll()
                        }
                    }
                android.app.AlertDialog.Builder(this@FingerprintActivity).setMessage(
                    "This device is not calibrated yet. Do you want to calibrate it?\r\n\r\n" +
                            "If yes, at first perfectly clean the sensor, and only then select the YES button."
                ).setPositiveButton("Yes", dialogClickListener)
                    .setNegativeButton("No", dialogClickListener).show()
            })
            synchronized(lock) {
                lock.wait()
                if (!dialogResult) throw java.lang.Exception("The device is not calibrated")
            }
            showToast("Creating calibration data: $paths")
            showInfoStatus("This operation may take several minutes.")
            try {
                val data = device.GenerateCalibrationData()
                val fos = FileOutputStream(paths)
                fos.write(data)
                fos.close()
                showToast("Calibration data created")
            } catch (e: java.lang.Exception) {
                showErrorStatus(e.localizedMessage)
            }
        }
        if (file.exists()) {
            val size = file.length().toInt()
            val bytes = ByteArray(size)
            try {
                val buf =
                    BufferedInputStream(FileInputStream(file))
                buf.read(bytes, 0, bytes.size)
                buf.close()
            } catch (ex: IOException) {
            }
            device.SetBlobParameter(NBDevice.BLOB_PARAMETER_CALIBRATION_DATA, bytes)
        } else {
            throw java.lang.Exception("Missing compensation data - $paths")
        }
    }


    internal inner class PreviewListener :
        NBDeviceScanPreviewListener, NBBiometricsScanPreviewListener {
        private var counter = 0
        private var sequence = 0
        var isCanceled = false
            private set

        fun reset() {
            showInfoStatus("") // Placeholder for preview
            counter = 0
            sequence++
        }

        fun cancel() {
            isCanceled = true
        }

        override fun preview(event: NBDeviceScanPreviewEvent) {
            showNbStatus(
                event.status, String.format(
                    "PREVIEW #%d: Status: %s, Finger detect score: %d",
                    ++counter,
                    event.status.descriptiveMessage(),
                    event.fingerDetectValue
                )
            )

//            showToast("Status: ${event.status}")
//            if (event.status in arrayOf(NBDeviceScanStatus.OK, NBDeviceScanStatus.DONE)) {
//                saveImage(event.status, event.fingerDetectValue, event.image)
//                event.image?.run{
//                    showFingerPrintImage(event.format, this)
//                }
//            }

            if (isCanceled) {
                event.status = NBDeviceScanStatus.CANCELED
            }
        }

        override fun preview(event: NBBiometricsScanPreviewEvent) {
            showNbStatus(
                event.scanStatus, String.format(
                    "PREVIEW #%d: Status: %s, Finger detect score: %d",
                    ++counter,
                    event.scanStatus.descriptiveMessage(),
                    event.fingerDetectValue
                )
            )


//            if (event.scanStatus in arrayOf(NBDeviceScanStatus.OK, NBDeviceScanStatus.DONE)) {
//                saveImage(event.scanStatus, event.fingerDetectValue, event.image)
//                event.image?.run{
//                    showFingerPrintImage(event.format, this)
//                }
//            }


            if (isCanceled) {
                event.scanStatus = NBDeviceScanStatus.CANCELED
            }
        }

        private fun saveImage(
            status: NBDeviceScanStatus,
            fdetScore: Int,
            image: ByteArray?
        ) {
            if (!BuildConfig.DEBUG || image == null || fdetScore < 40) {
                return  // Save images only in Debug mode and only from reasonable finger detect score up
            }

            val dirPath =
                Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOWNLOADS).toString() + "/NBImages/"
            val filePath = dirPath + String.format(
                "NBBiometrics_seq%02d_%03d_fdet%03d_%s.raw",
                sequence,
                counter,
                fdetScore,
                status.toString()
            )
            File(dirPath).mkdirs()
            try {
                val fos = FileOutputStream(filePath)
                fos.write(image)
                fos.close()
            } catch (e: java.lang.Exception) {
            }
        }
    }

    private fun loadFingerprintFile(onSelect: (Array<String>) -> Unit) {
        val fileDialog = FilePickerDialog(this, DialogProperties().apply {
            selection_mode = DialogConfigs.MULTI_MODE;
            selection_type = DialogConfigs.FILE_AND_DIR_SELECT;
            root = File(DialogConfigs.DEFAULT_DIR);
            error_dir = File(DialogConfigs.DEFAULT_DIR);
            offset = File(DialogConfigs.DEFAULT_DIR);
            extensions = null;
            show_hidden_files = true;
        }).apply {
             this.setTitle("Select File(s)")
            setDialogSelectionListener {
                onSelect(it)
            }
        }

        fileDialog.show()
    }


    private fun toeConverter(device: NBDevice, array: Array<String>) {
        array.forEach {
            runOnBackground {
                kotlin.runCatching {
                    val data = storage.readFile(it)
                    if (data.isNotEmpty()) {
                        runOnBackground {
                            runCatching {
                                convertToe(device, data,
                                    NBDeviceEncodeFormat.Iso010,
                                    compressionRatio = 3.0F)
                            }.onFailure {
                                showErrorStatus(it.localizedMessage)
                            }
                        }
                    }
                }.onFailure {
                    showErrorStatus(it.localizedMessage)
                }
            }
        }
    }

    private fun convertToe(device: NBDevice, image: ByteArray,
                           encodeFormat: NBDeviceEncodeFormat = NBDeviceEncodeFormat.Iso010,
                           fingerPosition: NBDeviceFingerPosition = NBDeviceFingerPosition.Unknown,
                           width: Int = device.supportedScanFormats.first().width,
                           height: Int = device.supportedScanFormats.first().height,
                           horizontalResolution: Int = device.supportedScanFormats.first().horizontalResolution,
                           compressionRatio: Float = 1.0F) {

       val  newImage = device.ConvertImage(image, width, height, horizontalResolution,
            encodeFormat,
            compressionRatio, // compression ratio 3:1
        fingerPosition, 0)

        runOnBackground{
            val fileName = "${basePath}conversion${getFileSuffix()}.jpg"
           if  (storage.createFile( fileName, newImage)) {
               showToast("$fileName saved")
           }
        }
    }

    private fun setSecurityLevel() {
        val items: Array<String> = NBBiometricsSecurityLevel.values().map {
            it.descriptiveMessage()
        }.toTypedArray()

        AlertDialog.Builder(this)
            .setTitle("Security Levels")
            .setSingleChoiceItems(
                items,
                securityLevel.ordinal
            ) { dialog: DialogInterface, which: Int ->
                securityLevel = NBBiometricsSecurityLevel.values()[which]
                showToast("Security Level: ${securityLevel.descriptiveMessage()}")
                dialog.dismiss()
            }.show()
    }

    private fun listRecords(onclick: (Array<FingerprintRecord>) -> Unit) {
        listRecords(onclick, false)
    }

    private fun listRecords(onclick: (Array<FingerprintRecord>) -> Unit, isMultiSelection: Boolean) {
        val records = FingerprintRecordMgr.getAll()
        if (records.isEmpty()) {
            showToast("No records found")
            return
        }

        val items: Array<String> = records.map {
            "${it.tag}"//\n${it.filePath}"
        }.toTypedArray()

        val builder =  AlertDialog.Builder(this)
            .setTitle("Records")

        val selection = ArrayList<FingerprintRecord>()
        if (isMultiSelection) {
            builder
                .setMultiChoiceItems(items,null){_, which, isChecked ->
                if (isChecked) {
                    selection.add(records[which])
                } else {
                    if (selection.isNotEmpty()) {
                        selection.remove(records[which])
                    }
                }
            }
                .setOnDismissListener {
                if (selection.isNotEmpty()) {
                    onclick(selection.toTypedArray())
                }
            }.setPositiveButton(android.R.string.ok) {dialog, _ ->
                dialog.dismiss()
            }
        } else {
            builder.setSingleChoiceItems(
                items,
                -1
            ) { dialog: DialogInterface, which: Int ->
                val record = records[which]
                onclick(arrayOf(record))
                dialog.dismiss()
            }
        }

        builder.show()
    }


    private fun getFileSuffix() =
        SimpleDateFormat("yyyy_MM_dd_HH_mm_ss").format(System.currentTimeMillis())
}
