package com.xpresspayments.newland910.ui

import android.os.Bundle
import android.os.Handler
import android.os.Message
import android.util.Log
import android.view.*
import androidx.core.os.bundleOf
import androidx.fragment.app.DialogFragment
import com.newland.mtype.module.common.pin.*
import com.newland.mtype.util.ISOUtils
import com.xpresspayments.newland910.CryptoManager
import com.xpresspayments.newland910.extensions.hexString
import com.xpresspayments.newland910.extensions.pinModule
import com.xpresspayments.newland910.utils.AbstractDeviceEventListener
import com.xpresspos.epmslib.extensions.maskPan
import io.reactivex.*
import java.util.concurrent.TimeUnit
import com.xpresspayments.newland910.R
import com.xpresspayments.newland910.utils.SoundPoolImpl
import com.xpresspos.epmslib.extensions.formatCurrencyAmount
import kotlinx.android.synthetic.main.pin_entry_dialog_fragment.*


class PinEntryDialog : DialogFragment() {

    private val TAG = "KeyBoardNumber"
    private var inputLen = 0
    private val isDukpt = false

    private val pinMaxLen = 6
    private val pinLengthRange = getPinLengthRange(4, pinMaxLen)
    private val pinPadding = byteArrayOf(
        'F'.toByte(),
        'F'.toByte(),
        'F'.toByte(),
        'F'.toByte(),
        'F'.toByte(),
        'F'.toByte(),
        'F'.toByte(),
        'F'.toByte(),
        'F'.toByte(),
        'F'.toByte()
    )
    private val pinTimeOut =  59L
    private val timeUnit = TimeUnit.SECONDS
    private val pinConfirmType = PinConfirmType.ENABLE_ENTER_COMMANG

    companion object {
        const val CARD_PAN = "card_pan"
        const val MODULUS = "modulus"
        const val EXPONENT = "exponent"
        const val IS_OFFLINE = "is_offline"
        const val AMOUNT = "amount"

        fun newInstance(cardPan: String, modulus: ByteArray?, exponent: ByteArray?, isOffline: Boolean, amount: Long = 0) = PinEntryDialog()
            .apply {
                arguments = bundleOf(
                    Pair(CARD_PAN, cardPan),
                    Pair(MODULUS, modulus),
                    Pair(EXPONENT, exponent),
                    Pair(IS_OFFLINE, isOffline),
                    Pair(AMOUNT, amount)
                )
            }
    }

    private lateinit var pinInput: K21Pininput
    private lateinit var spi: SoundPoolImpl


    private var emitter: SingleEmitter<ByteArray>? = null

    val pinResult: Single<ByteArray> by lazy {
        Single.create<ByteArray> {
            emitter = it
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setStyle(STYLE_NO_FRAME, R.style.AppTheme_PopupOverlay)
        isCancelable = false
    }


    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return View.inflate(context, R.layout.pin_entry_dialog_fragment, container)
    }


    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        pinInput = context!!.pinModule
        spi = SoundPoolImpl.getInstance()
        spi.initLoad(activity)
        init()

    }

    override fun onDestroy() {
        super.onDestroy()
        spi.release()
    }

    private fun init() {
        val cardPan = arguments?.getString(CARD_PAN) ?: ""
        val modulus = arguments?.getByteArray(MODULUS)
        val exponent = arguments?.getByteArray(EXPONENT)
        val isOffline = arguments?.getBoolean(IS_OFFLINE, false)
        val amount = arguments?.getLong(AMOUNT, 0L)

        if (amount!! > 0L) {
            tv_input_pin_amount.text = (amount/100.0).formatCurrencyAmount()
        }

        Log.d(TAG, "Card PAN: ${cardPan.maskPan()}")
        Log.d(TAG, "Modulus: ${modulus?.hexString}")
        Log.d(TAG, "Exponent: ${exponent?.hexString}")

        pinKeyBoard.viewTreeObserver.addOnPreDrawListener(object : ViewTreeObserver.OnPreDrawListener {
            private var first: Boolean = false

            override fun onPreDraw(): Boolean {
                if (!first) {
                    first = true
                    getRandomKeyBoardNumber()
                    if (isOffline!!) { // Offline Pin verification
                        pinInput.startStandardOfflinePinInput(pinMaxLen,
                            pinLengthRange,
                            pinConfirmType,
                            pinTimeOut,
                            timeUnit,
                            modulus,
                            exponent,
                            pinInputListener)
                    } else {  //Online Pin verification
                        if (isDukpt) {
                            pinInput.startStandardPinInput(
                                null,
                                WorkingKey(CryptoManager.DEFAULT_DUKPT_KEY_INDEX),
                                KeyManageType.DUKPT,
                                AccountInputType.USE_ACCOUNT,
                                cardPan,
                                pinMaxLen,
                                pinLengthRange,
                                pinPadding,
                                pinConfirmType,
                                pinTimeOut,
                                timeUnit,
                                null,
                                null,
                                pinInputListener
                            )
                        } else {
                            pinInput.startStandardPinInput(
                                getString(R.string.enter_pin),
                                WorkingKey(CryptoManager.DEFAULT_PIN_KEY_INDEX),
                                KeyManageType.MKSK,
                                AccountInputType.USE_ACCOUNT,
                                cardPan,
                                pinMaxLen,
                                pinLengthRange,
                                pinPadding,
                                pinConfirmType,
                                pinTimeOut,
                                timeUnit,
                                null,
                                null,
                                pinInputListener
                            )
                        }
                    }

                }
                return first
            }
        })

    }


    private val mHandler = object : Handler() {
        override fun handleMessage(msg: Message) {
            when (msg.what) {
                2 -> {
                    val len = msg.obj as Int
                    val buffer = StringBuffer()
                    for (i in 0 until len) {
                        buffer.append(" * ")
                    }

                    txtPassword.text = buffer
                }

                else -> {
                }
            }
        }
    }

    private val pinInputListener = object : AbstractDeviceEventListener<K21PininutEvent>() {

        override fun onEvent(event: K21PininutEvent, h: Handler?) {
            spi.play()
            if (event.isProcessing) {// inputing
                Log.i(TAG, getString(R.string.is_processing))
                val notifyStep = event.notifyStep
                if (notifyStep == PinInputEvent.NotifyStep.ENTER) {
                    inputLen += 1
                } else if (notifyStep == PinInputEvent.NotifyStep.BACKSPACE) {
                    inputLen = if (inputLen <= 0) 0 else inputLen - 1
                }
                val msg = mHandler.obtainMessage(2)
                msg.obj = inputLen
                msg.sendToTarget()

            } else if (event.isUserCanceled) {// cancel
                Log.i(TAG, getString(R.string.log_is_user_canceled))
                dismiss()

                emitter?.tryOnError(RuntimeException("Pin entry cancelled"))
            } else if (event.isSuccess) {// confirm
                Log.i(TAG, getString(R.string.log_is_success))
                if (event.inputLen == 0) {
                    Log.i(TAG, getString(R.string.log_enter_null))
                    dismiss()

                    val pin = byteArrayOf() // bypass

                    emitter?.onSuccess(pin)
                } else {
                    //  enter success
                    val pin = event.encrypPin
                    dismiss()

                    Log.i(TAG, getString(R.string.log_enter_success) + ISOUtils.hexString(pin)) //0000000 means offline pin entered
                    emitter?.onSuccess(pin)
                }
            } else {
                Log.i(TAG, getString(R.string.log_entery_pwd_exception), event.exception)
                dismiss()
                emitter?.tryOnError(event.exception)
            }
        }
    }

    /**
     * get Random KeyBoard Number
     *
     * @return
     */
    private fun getRandomKeyBoardNumber() {
        try {
            val initCoordinate = pinKeyBoard?.coordinate
            Log.i(TAG, "Initial coordinate: ${ISOUtils.hexString(initCoordinate)}")

            //  Get the value from random password keyboard.
            val keySeq = pinKeyBoard?.getPinKeySeq(com.xpresspayments.newland910.ui.N900PinKeyBoard.PinKeySeq.RANDOM_NUM)

            //If the number of random and function keys fixed , do not need the keySeq
            val keyboardRandom = if (keySeq != null) {
                KeyboardRandom(initCoordinate, keySeq)
            } else {
                KeyboardRandom(initCoordinate)
            }

            val randomCoordinate = pinInput.loadRandomKeyboard(keyboardRandom)
            pinKeyBoard?.loadRandomKeyboardfinished(randomCoordinate)
        } catch (e: Exception) {
            e.printStackTrace()
        }

    }

    /**
     * Get Pin Length Range
     * @param pinMinLen
     * @param pinMaxLen
     * @return
     */
    private fun getPinLengthRange(pinMinLen: Int, pinMaxLen: Int): ByteArray {
        val sumPinLen = byteArrayOf(0x00, 0x00, 0x00, 0x00, 0x04, 0x05, 0x06, 0x07, 0x08, 0x09, 0x0A, 0x0B, 0x0C)
        val pinLen = ByteArray(pinMaxLen - pinMinLen + 1)
        System.arraycopy(sumPinLen, pinMinLen, pinLen, 0, pinLen.size)
        return pinLen
    }

}
