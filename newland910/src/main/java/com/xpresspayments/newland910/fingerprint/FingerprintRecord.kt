package com.xpresspayments.newland910.fingerprint

import android.content.Context
import android.os.Environment
import com.google.gson.Gson
import com.snatik.storage.Storage
import com.xpresspayments.commons.runOnBackground
import java.util.ArrayList

data class FingerprintRecord (val tag: String,
                              val filePath: String,
                              val scanFormatIndex: Int,
                              val templateIndex: Int)



object FingerprintRecordMgr {
    private val store = Store()
    private val gson = Gson()
    private val  path = "/records.data"
    fun loadRecords(context: Context) = runOnBackground{
        val storage =  Storage(context)
        val fullPath =  storage.internalCacheDirectory + path

        if (storage.isFileExist(fullPath)) {
            val content = storage.readTextFile(fullPath)
            println(content)
            val data:Store? = gson.fromJson<Store>(content, Store::class.java)
            data?.run {
                store.records.clear()
                store.records.addAll(data.records)
            }

        } else {
            println("No record found")
        }
    }

    fun saveRecords(context: Context) = runOnBackground {
        if (store.records.isNotEmpty()) {
            val storage =  Storage(context)
            val fullPath =  storage.internalCacheDirectory + path

            val recString = gson.toJson(store)
            if (storage.createFile(fullPath, recString)) {
                println("Records saved")
            } else {
                println("Records could not be saved")
            }
        } else {
            println("Store is empty")
        }

    }

    fun store(fingerprintRecord: FingerprintRecord) = store.records.add(fingerprintRecord)

    fun getAll(): List<FingerprintRecord> = store.records

    private class Store {
        val records = ArrayList<FingerprintRecord>()
    }
}