package com.xpresspayments.newland910.fingerprint;

import com.nextbiometrics.devices.NBDevice;
import com.nextbiometrics.devices.NBDevices;

import java.io.Serializable;

public class ScanDeviceInfo implements Serializable {
    private String id;

    private boolean isSpi;
    private String spiName;
    private int awakePin;
    private int resetPin;
    private int chipSelectPin;

    public ScanDeviceInfo(String id) {
        this(id, false, null, -1, -1, -1);
    }

    public ScanDeviceInfo(String spiName, int awakePin, int resetPin, int chipSelectPin) {
        this(null, true, spiName, awakePin, resetPin, chipSelectPin);
    }

    public ScanDeviceInfo(String id, boolean isSpi, String spiName, int awakePin, int resetPin, int chipSelectPin) {
        this.id = id;
        this.isSpi = isSpi;
        this.spiName = spiName;
        this.awakePin = awakePin;
        this.resetPin = resetPin;
        this.chipSelectPin = chipSelectPin;
    }

    public String getId() {
        return id;
    }

    public boolean isSpi() {
        return isSpi;
    }

    public String getSpiName() {
        return spiName;
    }

    public int getAwakePin() {
        return awakePin;
    }

    public int getResetPin() {
        return resetPin;
    }

    public int getChipSelectPin() {
        return chipSelectPin;
    }

    public static NBDevice getDevice(ScanDeviceInfo deviceInfo) {
        if (deviceInfo == null || !deviceInfo.isSpi()) {
            NBDevice[] devices = NBDevices.getDevices();
            for (NBDevice device : devices) {
                if (deviceInfo != null && deviceInfo.getId() != null && deviceInfo.getId().equals(device.getId())) {
                    return device;
                }
            }
            if (devices.length > 0)
                return devices[0];
            return null;
        } else {
            NBDevice device = NBDevice.connectToSpi(deviceInfo.getSpiName(), deviceInfo.getAwakePin(), deviceInfo.getResetPin(), deviceInfo.getChipSelectPin(), NBDevice.DEVICE_CONNECT_TO_SPI_SKIP_GPIO_INIT_FLAG);
            deviceInfo.id = device.getId();
            return device;
        }
    }
}
