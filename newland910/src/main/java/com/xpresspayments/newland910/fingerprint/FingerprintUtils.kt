package com.xpresspayments.newland910.fingerprint

import android.app.Activity
import android.graphics.Bitmap
import android.graphics.Color
import android.newland.printer.FileUtil
import com.nextbiometrics.biometrics.NBBiometricsExtractResult
import com.nextbiometrics.biometrics.NBBiometricsVerifyResult
import com.nextbiometrics.devices.NBDeviceScanFormatInfo
import java.io.File
import java.nio.IntBuffer
import kotlin.experimental.and


internal fun getNBTemplateName(type: Int) = when (type) {
    1 -> "Proprietary algorithm vendor template format"
    2 -> "ISO/IEC 19794-2 template format"
    3 -> "ANSI/INCITS 378 template format"
    4 -> "ISO/IEC 19794-2 Compact Card template format"
    else -> "Unknown template"
}


internal fun getNBScanTypeString(type: Int) = when (type) {
    0 -> "12 mm x 17 mm format, width 180 pixels, height 256 pixels, 385 dpi"
    1 -> "12 mm x 16 mm format, width 180 pixels, height 244 pixels, 385 dpi"
    2 -> "Partial format, width 90 pixels, height 128 pixels, 385 dpi"
    3 -> "12 mm x 12 mm format, width 180 pixels, height 180 pixels, 385 dp"
    4 -> "12 mm x 17 mm format, width 234 pixels, height 332 pixels, upscaled to 500 dpi"
    5 -> "12 mm x 16 mm format, width 234 pixels, height 317 pixels, upscaled to 500 dpi"
    6 -> "Partial format, width 117 pixels, height 166 pixels, upscaled to 500 dpi"
    7 -> "12 mm x 12 mm format, width 234 pixels, height 234 pixels, upscaled to 500 dpi"
    8 -> "Quarter partial format, width 22 pixels, height 128 pixels, 385 dpi"
    9 -> "Quarter partial format, width 29 pixels, height 166 pixels, upscaled to 500 dpi"
    else -> "Invalid scan type"
}

fun Any.descriptiveMessage() = this.toString().replace("_", " ").toLowerCase().capitalize()

fun NBBiometricsExtractResult.descriptiveMessage() = status.descriptiveMessage()
fun NBBiometricsVerifyResult.descriptiveMessage() = status.descriptiveMessage()

 fun convertNBToBitmap(
    formatInfo: NBDeviceScanFormatInfo,
    image: ByteArray
): Bitmap? {
    val buf = IntBuffer.allocate(image.size)
    for (pixel in image) {
        val grey = (pixel and 0x0ff.toByte()).toInt()
        buf.put(Color.argb(255, grey, grey, grey))
    }
    return Bitmap.createBitmap(
        buf.array(),
        formatInfo.width,
        formatInfo.height,
        Bitmap.Config.ARGB_8888
    )
}

val fingerprintUsbFile by lazy  {
    File("/sys/class/fingerprint_ctrl", "fingerprint_en")
}

fun Activity.enableFingerprintModuleUsbHost() {
    FileUtil.writeFileByString(fingerprintUsbFile, "1", false)
}

fun Activity.disableFingerprintModuleUsbHost() {
    FileUtil.writeFileByString(fingerprintUsbFile, "0", false)
}